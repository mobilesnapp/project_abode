'use strict';

//jshint ignore:start
angular.module('app.application')
    .controller('TimelineCtrl', TimelineCtrl);

TimelineCtrl.inject = ['$scope', '$rootScope', '$sce', '$window', 'apiUrl', '$ionicModal', 'EventsService', 'DevicesService', 'AssociatesService'];
function TimelineCtrl($scope, $rootScope, $sce, $window, apiUrl, $ionicModal, EventsService, DevicesService, AssociatesService) {
    $scope.url = url;
    $scope.$parent.vm.click_tab = 'timeline';
    function url(path) {
        return $sce.trustAsResourceUrl(apiUrl + '/' + path);
    }

    var timeline_init_busy = false;

    $scope.click_index = 0;
    $scope.timelines = EventsService.list;
    $scope.user = {};
    AssociatesService.associatesGET().then(function (data) {
        angular.forEach(data, function (val) {
            $scope.user[val.id] = angular.copy(val);
        });
    });
    angular.forEach($scope.timelines, function (val) {
        if (val.file_count > 1) { //jshint ignore:line
            var ext = val.file_path.substring(val.file_path.length - 4); //jshint ignore:line
            var file = val.file_path.substring(0, val.file_path.length - 5); //jshint ignore:line
            val.files = [];
            for (var i = 0; i < val.file_count; i++) { //jshint ignore:line
                val.files.push({
                    file_path: apiUrl + '/' + file + (i + 1) + ext  //jshint ignore:line
                });
            }
        }
    });
    $scope.devices = DevicesService.list;

    $rootScope.$on('devices:loaded', function () {
        // Reload icons
        for (var i = 0; i < EventsService.list.length; i++) {
            EventsService.list[i].init_icon();
        }
    });
    $rootScope.$on('updateTimeLine', function () {
        $scope.doRefresh();
    });
    $scope.doRefresh = function () {
        $scope.timelines = angular.copy($scope.timelines);
        EventsService.search().then(function () {
            $scope.timelines = EventsService.list;
            $scope.$broadcast('scroll.refreshComplete');
            timeline_init_busy = false;
        }, function () {
            $scope.timelines = EventsService.list;
            $scope.$broadcast('scroll.refreshComplete');
            timeline_init_busy = false;
        });
    };
    //$scope.doRefresh();

    $scope.timeline_init = function () {
        if ($scope.timelines.length) {
            timeline_init_busy = true;
            EventsService.search().then(function () {
                timeline_init_busy = false;
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        }
        //if ($scope.timelines.length === 0 && timeline_init_busy === false) {
        //    timeline_init_busy = true;
        //    EventsService.search().then(function () {
        //        timeline_init_busy = false;
        //    });
        //}

        if ($scope.devices.length === 0) {
            DevicesService.load();
        }
    };

    $scope.nextPageLoad = function () {
        console.log('next page');
        if ($scope.timelines.length === 0 && timeline_init_busy === false) {
            timeline_init_busy = true;
            EventsService.search().then(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                timeline_init_busy = false;
            });
        } else if (timeline_init_busy === false) {
            EventsService.search('yes', 'next').then(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                timeline_init_busy = false;
            });
        }
    };

    // -------- Portrait/Landscape Streaming Camera View fullscreen/normal change -----------//

    $scope.window_res = {
        w: $window.innerWidth,
        h: $window.innerHeight
    };
    $scope.image_dialog_res = {width: 0, height: 0, left: 0, top: 0};

    $ionicModal.fromTemplateUrl('components/_global/templates/popups/popup_modal_image.html', {
        scope: $scope,
        animation: 'fade-in'
    }).then(function (modal) {
        $scope.modal = modal;
    });

    $scope.openModal = function (imgPath) {
        $scope.camera = {
            sel_event: {
                image_path: imgPath
            }
        };
        if ($scope.modal) {
            $scope.modal.show();
        }
    };

    $scope.closeModal = function () {
        if ($scope.modal) {
            $scope.modal.hide();
        }
    };

    //$scope.getUserInfo = function (id) {
    //    angular.forEach(vm.users, function (val) {
    //        if (id === val.id) {
    //            this.userName = ' by ' + val.name;
    //            this.profilePic = val.profile_pic; //jshint ignore:line
    //        }
    //    });
    //};
    // modal dialog rotate left, top and width calculation

    function calc_image_modal_positions() {
        $scope.image_dialog_res.width = $scope.window_res.h + 'px';
        $scope.image_dialog_res.height = $scope.window_res.w;
        $scope.image_dialog_res.left = ($scope.window_res.w - $scope.window_res.h) / 2 + 'px';
        $scope.image_dialog_res.top = ($scope.window_res.h - $scope.window_res.w) / 2 + 'px';
    }

    calc_image_modal_positions();
}