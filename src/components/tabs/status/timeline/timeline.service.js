'use strict';

//jshint ignore:start
angular.module('app.application')
    .service('AssociatesService', AssociatesService);

AssociatesService.inject = ['Restangular', 'apiVersion', 'MessageService'];
function AssociatesService(Restangular, apiVersion, MessageService) {
    return {
        associatesGET: function () {
            return Restangular.all(apiVersion + 'associates').getList().then(function (data) {
                return data;
            }, function (error) {
                MessageService.hideLoading();
                MessageService.showErrorMessage('Error', error.data.message);
            });
        }
    };
}