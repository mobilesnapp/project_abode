'use strict';

//jshint ignore:start
angular.module('app.application')
    .controller('StatusCtrl', StatusCtrl)
    .controller('AddMotionCamCtrl', AddMotionCamCtrl);

StatusCtrl.inject = ['$scope', '$rootScope', '$state', '$ionicPopover', '$ionicModal', 'GatewayService', 'EventsService', '$ionicViewSwitcher', '$ionicScrollDelegate', 'localStorageService', '$cordovaInAppBrowser', '$ionicSlideBoxDelegate'];
function StatusCtrl($scope, $rootScope, $state, $ionicPopover, $ionicModal, GatewayService, EventsService, $ionicViewSwitcher, $ionicScrollDelegate, localStorageService, $cordovaInAppBrowser, $ionicSlideBoxDelegate) {
    var vm = this,
        tutorial_show = localStorageService.get("tutorial_show");

    vm.alerts = EventsService.alertsList;
    vm.click_tab = $rootScope.status_tab;
    vm.gateway = GatewayService;

    vm.onAlertsClick = onAlertsClick;
    vm.tabClick = tabClick;
    vm.openOfflineSupportWebsite = openOfflineSupportWebsite;
    vm.openModal = openModal;
    vm.closeModal = closeModal;

    console.log($rootScope.status_tab);
    if ($rootScope.status_tab == undefined || $rootScope.status_tab == '') {
        vm.click_tab = $rootScope.status_tab || 'timeline';
        vm.tabClick('timeline');
    }
    if (tutorial_show != '1') {
        $ionicModal.fromTemplateUrl('components/_global/templates/popups/popup_modal_tutorial.html', {
            scope: $scope,
            animation: 'fade-in'
        }).then(function (modal) {
            vm.modal = modal;
            vm.modal.show();
        });
    }
    $scope.nextSlide = function () {
        $ionicSlideBoxDelegate.next();
    };
    $scope.$on('$ionicView.beforeEnter', function (scopes, states) {

        if ($rootScope.status_tab) {
            if (states.stateName !== "tab.status." + $rootScope.status_tab) {
                $state.go("tab.status." + $rootScope.status_tab);
            }
        }

        if (EventsService.groupList.length == 0) {
            EventsService.loadAlertEvents('no');
        }
        if (EventsService.alertsList.length == 0) {
            EventsService.loadGroupAlarm('no');
        }
    });

    function onAlertsClick() {
        $ionicViewSwitcher.nextDirection('forward');
        $state.go('alert');
    }

    function tabClick(tab) {
        vm.click_tab = tab;
        $rootScope.status_tab = tab;
        $state.go("tab.status." + tab);
        $ionicScrollDelegate.scrollTop();
    }

    function openOfflineSupportWebsite() {
        window.open('https://support.goabode.com/success/s/article/Gateway-Offline', '_blank');
    }

    function openModal() {
        if (vm.modal) {
            vm.modal.show();
        }
    }

    function closeModal() {
        if (vm.modal) {
            vm.modal.hide();
            localStorageService.set("tutorial_show", "1");
        }
    }
}

AddMotionCamCtrl.inject = ['$sce'];
function AddMotionCamCtrl($sce) {
    this.config = {
        sources: [
            {
                src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.mp4"),
                type: "video/mp4"
            },
            {
                src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"),
                type: "video/webm"
            },
            {
                src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"),
                type: "video/ogg"
            }
        ],
        tracks: [
            {
                src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                kind: "subtitles",
                srclang: "en",
                label: "English",
                default: ""
            }
        ],
        theme: "lib/videogular-themes-default/videogular.css",
        plugins: {
            poster: "http://www.videogular.com/assets/images/videogular.png"
        }
    };
}