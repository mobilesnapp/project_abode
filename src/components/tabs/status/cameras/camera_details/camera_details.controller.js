'use strict';

//jshint ignore:start
angular.module('app.application')
    .controller('CameraDetailsCtrl', CameraDetailsCtrl);

CameraDetailsCtrl.inject = ['$scope', '$ionicModal', '$interval', '$state', 'CameraService', 'DevicesService', 'MessageService', 'EventsService', '$window', 'ERRORS'];
function CameraDetailsCtrl($scope, $ionicModal, $interval, $state, CameraService, DevicesService, MessageService, EventsService, $window, ERRORS) {

    $scope.camera = CameraService.selectedCam;
    $scope.camera.sel_event = $scope.camera.last_event;
    $scope.history_visible = 0;
    $scope.image_loaded = 0;

    $scope.onClickCameraEvent = function (event) {
        if (event.typeId === '5000') {
            $state.go('tab.video', {video_id: event.id});
        } else {
            $scope.showImage = event.image_path;
            $scope.openModal();
        }
    };

    // -------- Portrait/Landscape Streaming Camera View fullscreen/normal change -----------//
    $scope.window_res = {w: $window.innerWidth, h: $window.innerHeight};
    $scope.image_dialog_res = {width: 0, height: 0, left: 0, top: 0};

    calc_image_modal_positions();
    $scope.openModal = openModal;
    $scope.closeModal = closeModal;

    $ionicModal.fromTemplateUrl('components/_global/templates/popups/popup_modal_image.html', {
        scope: $scope,
        animation: 'fade-in'
    }).then(function (modal) {
        $scope.modal = modal;
    });

    function calc_image_modal_positions() {
        $scope.image_dialog_res.width = $scope.window_res.h + 'px';
        $scope.image_dialog_res.height = $scope.window_res.w;
        $scope.image_dialog_res.left = ($scope.window_res.w - $scope.window_res.h) / 2 + 'px';
        $scope.image_dialog_res.top = ($scope.window_res.h - $scope.window_res.w) / 2 + 'px';
    }

    function openModal() {
        if ($scope.modal)
            $scope.modal.show();
    }

    function closeModal() {
        if ($scope.modal) {
            $scope.showImage = null;
        }
        $scope.modal.hide();
    }

    // ------------- >> Camera Request << --------------- //
    $scope.stop_new_interval = function () {
        if (angular.isDefined($scope.check_new_interval)) {
            $interval.cancel($scope.check_new_interval);
            $scope.check_new_interval = undefined;
        }
    };

    $scope.onCameraRequestClick = function () {
        //if ($scope.camera.stream == 0) {
        //    $scope.showSpinner = true;
        //}
        var cam_device = searchCamAsDevice($scope.camera);
        if (cam_device != null && cam_device != undefined) {
            cam_device.recIPCAM(1).then(function () {
                //$scope.showSpinner = true;
                $scope.check_new_interval = $interval(function () {
                    EventsService.get_camera_events($scope.camera).then(function (response) {
                        // changed
                        if (response) {
                            if (response[0].id != $scope.camera.event_list[0].id) {
                                $scope.camera.event_list = response;
                                $scope.camera.sel_event = $scope.camera.event_list[0];
                                $scope.stop_new_interval();
                                //$scope.showSpinner = false;
                            }
                        }
                    });
                }, 5000);
            }, function () {
                console.log("failed");
            });
        }
    };

    // ************** Streaming Camera *************** //
    var camera = null,
        timer = null;
    $scope.streaming = false;
    $scope.name = '';
    $scope.errorCounter = 0;
    $scope.url1 = '';

    $scope.onImageLoad = function (id) {
        $scope.image_loaded = 1;
        if (timer) {
            $interval.cancel(timer);
            timer = $interval(function () {
                if (camera) {
                    $scope.url1 = camera.getImageUrl();
                }
            }, 1000);
        }
        if (camera) {
            $scope.url1 = camera.getImageUrl();
        }
    };

    $scope.onImageError = function (id) {
        if (camera) {
            startStream(camera);
        }
    };

    function stop() {
        if (camera) {
            camera.stopStream();
        }
        $scope.streaming = false;
        $scope.url1 = '';
        camera = null;
        if (timer) {
            $interval.cancel(timer);
            timer = null;
        }
        $scope.name = '';
    }

    function searchCam() {
        if (camera == null) return -1;
        var cams = DevicesService.cams;
        for (var i = 0; i < cams.length; i++)
            if (cams[i].mac == camera.mac)
                return i;
        return -1;
    }

    function searchCamAsDevice(cam_search) {
        if (cam_search == null) return null;
        var cams = DevicesService.list;
        for (var i = 0; i < cams.length; i++)
            if (cams[i].id == cam_search.id)
                return cams[i];
        return null;
    }

    $scope.next = function () {
        var cams = DevicesService.cams;
        var index = searchCam();
        if (index != -1) {
            $scope.streaming = false;
            var newIndex = (index == cams.length - 1) ? 0 : index + 1;
            DevicesService.setCamera(cams[newIndex]);
        }
    };

    $scope.prev = function () {
        var cams = DevicesService.cams;
        var index = searchCam();
        if (index != -1) {
            $scope.streaming = false;
            var newIndex = (index == 0) ? cams.length - 1 : index - 1;
            DevicesService.setCamera(cams[newIndex]);
        }
    };

    $scope.showHistory = function () {
        if (DevicesService.selCam) {
            filterForm.device = DevicesService.selCam;
            filterForm.event = eventsArray.getEventById(5000);
            filterForm.submit();
            $state.go('app.events');
        }
    };

    $scope.$on('$destroy', stop);

    function startStream(cam) {
        stop();
        var mac = cam.mac;
        $scope.name = cam.name;
        camera = cam;
        camera.startStream().then(function (result) {
            if (result == 'cancelled') {
                $scope.stream_notification = 'Streaming cancelled.';
            } else {
                $scope.url1 = camera.getImageUrl();
                $scope.streaming = true;
            }
        }, function (reason) {
            camera = null;
            MessageService.showRegisteredErrorMessage(ERRORS.STREAMING_ERROR, "Streaming Error");
            $scope.stream_notification = 'Unable to start streaming.';
        }, function (msg) {
            $scope.stream_notification = msg;
        });
    }

    $scope.$on('cam:selected', function (event, cam) {
        if (cam != null && typeof cam === 'object') {
            startStream(cam);
        }
        else
            stop();
    });

    function selectDevice() {
        if ($scope.camera.stream != '1') return;
        startStream($scope.camera)
    }

    $scope.$on('devices:loaded', selectDevice);

    selectDevice();
}