'use strict';

//jshint ignore:start
angular.module('app.application')
    .controller('VideoCtrl', VideoCtrl);

VideoCtrl.inject = ['$scope', '$stateParams', 'CameraService', 'apiUrl', '$sce'];
function VideoCtrl($scope, $stateParams, CameraService, apiUrl, $sce) {
    $scope.camera = CameraService.selectedCam;
    $scope.config = {
        //    preload: 'auto',
        //    theme: {
        //        url: '../bower_components/videogular-themes-default/videogular.css'
        //    }
    };

    $scope.imgUrl = function (path) {
        var res = path.replace('mp4', 'jpg');
        return $sce.trustAsResourceUrl(apiUrl + '/' + res);
    };

    function playVideo(event) {
        $scope.config.sources = {
            src: $sce.trustAsResourceUrl(event.files[0].thumbnailPath.replace('jpg', 'mp4')),
            poster: $sce.trustAsResourceUrl(event.files[0].thumbnailPath),
            type: 'video/mp4'
        };
    }

    $scope.onPlayerReady = function () {
        if ($scope.camera) {
            for (var i = 0; i < $scope.camera.event_list.length; i++) {
                if ($stateParams.video_id === $scope.camera.event_list[i].id) {
                    playVideo($scope.camera.event_list[i]);
                }
            }
        }
    };
    $scope.onPlayerReady();
}
