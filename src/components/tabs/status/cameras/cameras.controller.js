'use strict';

angular.module('app.application')
    .controller('CameraCtrl', CameraCtrl);

CameraCtrl.inject = ['$scope', '$state', 'GatewayService', 'CameraService', 'MessageService'];
function CameraCtrl($scope, $state, GatewayService, CameraService, MessageService) {
    var vm = this;
    vm.gatewayOnline = GatewayService.online;
    vm.cameraService = CameraService;
    vm.cameras = vm.cameraService.list;

    vm.cameraInit = cameraInit();
    vm.doRefresh = doRefresh;
    vm.onCameraClick = onCameraClick;

    $scope.$parent.vm.click_tab = 'cameras'; //jshint ignore:line

    function cameraInit() {
        if (CameraService.list.length === 0) {
            MessageService.showLoading('Loading...');
            CameraService.load().then(function (data) { //jshint ignore:line
                MessageService.hideLoading();
            }, function () {
                MessageService.hideLoading();
            });
        }
    }

    function doRefresh() {
        vm.cameras = angular.copy(vm.cameras);
        CameraService.load().then(function () {
            vm.cameras = vm.cameraService.list;
            $scope.$broadcast('scroll.refreshComplete');
        }, function () {
            vm.cameras = vm.cameraService.list;
            $scope.$broadcast('scroll.refreshComplete');
        });
    }

    function onCameraClick(index) {
        CameraService.selectCam(index);
        $state.go('tab.camera', {camera_id: index}); //jshint ignore:line
    }
}