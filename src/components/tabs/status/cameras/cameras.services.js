'use strict';

//jshint ignore:start
angular.module('app.application')
    .service('CameraService', function ($q, $rootScope, $http, $ionicLoading, DevicesService, EventsService, MessageService, Camera) {

        this.list = [];
        this.selectedCam = '';
        this.loaded = false;

        this.load = function () {
            var self = this;
            var deferred = $q.defer();
            if (this.loaded || this.busy) deferred.reject();
            this.busy = true;

            $http.get($rootScope.urlBackend + "/cams").then(function (response) {
                if (response.data) {
                    // first clear the list
                    while (self.list.length > 0) {
                        self.list.pop();
                    }

                    for (var i = 0; i < response.data.length; i++) {
                        var c = new Camera(response.data[i]);
                        self.list.push(c);
                    }
                    self.loaded = true;
                    $rootScope.$broadcast('cameras:loaded', this);
                    self.init_cams();
                    self.busy = false;
                    deferred.resolve();
                } else {
                    deferred.reject();
                }

            }, function (err) {
                console.log('Error in loading cameras', err);
                self.busy = false;
                deferred.reject();
            });
            return deferred.promise;
        };

        this.selectCam = function (cam_indx) {
            var self = this;
            self.selectedCam = this.list[cam_indx];
            EventsService.get_camera_events(self.selectedCam).then(function (response) {
                self.selectedCam.event_list = response;
            });
        };

        this.init_cams = function () {
            var self = this;
            for (var i = 0; i < self.list.length; i++) {
                var cam = self.list[i];
                self.list[i].status = DevicesService.searchCamById(cam).status;
                EventsService.get_last_event(cam, i).then(function (response) {
                    if (response) {
                        self.list[response.cam_index].last_event = response.data;
                    }
                }, function (err) {
                });
            }
        };

        this.clear = function () {
            while (this.list.length > 0) this.list.pop();
            this.selectedCam = '';
            this.loaded = false;
        };

    })
    .factory('Camera', function ($q, $http, $rootScope, MessageService, $interval) {

        var Camera = function (item) {
            this.id = '';
            this.name = '';
            this.area = '';
            this.zone = '';
            this.stream = '';
            this.type = '';
            this.icon = '';

            this.mac = '';
            this.external_port = '';
            this.external_ip = '';
            this.is_port_forwarding = '';
            this.host_by_mac = '';
            this.motion_detection = '';
            this.motion_detection_upload = '';

            this.SESSION_URL = '/stream/session';
            this.START_URL = '/stream/start';

            this.url = '';
            this.server = '';
            this.session = '';
            this.state = 'NOT_STREAMING';
            this.timer = null;
            this.deferred = null;
            this.counter = 0;

            if (item) {
                this.update(item);
            }
        };

        Camera.prototype.update = function (item) {

            this.id = item.id;
            if (item.mac)
                this.stream_id = item.mac.replace(/:/g, '');
            else
                this.stream_id = item.id;

            this.name = item.name;
            this.area = item.area;
            this.zone = item.zone;
            this.stream = item.stream;
            this.type = item.type;
            this.icon = item.icon;

            this.mac = item.mac;
            this.external_port = item.external_port;
            this.external_ip = item.external_ip;
            this.is_port_forwarding = item.is_port_forwarding;
            this.host_by_mac = item.host_by_mac;
            this.motion_detection = item.motion_detection;
            this.motion_detection_upload = item.motion_detection_upload;

        };

        Camera.prototype.toString = function () {
            if (this.state == 'STARTING_STREAM')
                return '[' + this.mac + '] - Starting Streaming';
            if (this.state == 'STREAMING')
                return '[' + this.mac + '] - ' + this.url;

            return '[' + this.mac + '] - Not Streaming';
        };

        Camera.prototype.isStreaming = function () {
            return this.state == 'STREAMING';
        };

        Camera.prototype.startStream = function () {
            if (this.state == 'STARTING_STREAM' ||
                this.state == 'STREAMING') {
                return;
            }

            this.state = 'STARTING_STREAM';
            var deferred = $q.defer();
            var self = this;
            self.deferred = deferred;

            var keepAlive = function () {
                $http.post($rootScope.urlBackend + self.START_URL, {mac: self.stream_id, timeout: deferred.promise})
                    .then(function (response) {
                        if (response.data['server'] != '') {
                            //console.log('Streaming Keep Alive timer response received.');
                        }
                        else {
                            throw 'Invalid response received for keep alive timer.'
                        }
                    }, function (error) {

                    })
                    ['catch'](function (error) {
                    console.log('Error in streaming keep alive, stoping stream.', error);
                    self.stopStream();
                });
            };

            $http.post($rootScope.urlBackend + self.SESSION_URL, {mac: self.stream_id})
                .then(function (response) {
                    deferred.notify('Trying to Load Camera');
                    //console.log("session result=", response);
                    var result = response.data;
                    if (result['session'] != '') {
                        self.session = result['session'];
                        deferred.notify('Loading');
                        return $http.post($rootScope.urlBackend + self.START_URL, {mac: self.stream_id});
                    }
                    else
                    //throw 'system.error.stream.session';
                        throw 'Loading';
                })
                .then(function (response) {
                    var result = response.data;
                    if (result['server'] != '') {
                        self.server = result['server'];
                        //console.log("session get image...");
                        self.url = self.server + '/get_image?mac=' + self.stream_id + "&session=" + self.session;
                        self.state = 'STREAMING';
                        deferred.notify('Streaming');
                        self.timer = $interval(keepAlive, 45000);
                        deferred.resolve(self);
                    }
                    else
                        throw 'system.error.stream.start';
                })
                ['catch'](function error() {
                //console.log(msg);
                self.deferred = null;
                self.stopStream();
                deferred.notify('Error in streaming camera');
                deferred.reject('system.error.stream');
            })
                ['finally'](function () {
                self.deferred = null;
            });

            return deferred.promise;
        };

        Camera.prototype.getImageUrl = function () {
            if (this.state == 'STREAMING') {
                this.counter++;
                return this.url + '&id=' + this.counter;
            }
            return '';
        };

        Camera.prototype.stopStream = function () {
            if (this.state == 'NOT_STREAMING') return;
            if (this.deferred)
                this.deferred.resolve('cancelled');
            this.deferred = null;
            this.url = '';
            this.server = '';
            this.session = '';
            this.counter = 0;
            if (this.timer)
                $interval.cancel(this.timer);
            this.timer = null;
            this.state = 'NOT_STREAMING';
            //console.log('Streaming Stoped', this);
        };

        return Camera;
    });