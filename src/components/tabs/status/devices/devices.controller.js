'use strict';

angular.module('app.application')
    .controller('DeviceCtrl', DeviceCtrl);

DeviceCtrl.inject = ['$scope', '$sce', 'apiUrl', 'DevicesService', 'GatewayService'];
function DeviceCtrl($scope, $sce, apiUrl, DevicesService, GatewayService) {
    var vm = this;
    vm.devices = DevicesService.list;
    vm.gatewayOnline = GatewayService.online;

    vm.devicesInit = devicesInit;
    vm.doRefresh = doRefresh;
    vm.url = url;
    vm.onCameraRequestClick = onCameraRequestClick;
    vm.changeStatus = changeStatus;

    $scope.$parent.vm.click_tab = 'devices'; //jshint ignore:line

    $scope.$watch(function () {
        return vm.devices;
    }, function (newVaule, oldVaule) { //jshint ignore:line
        reviseDevices();
    });
    function devicesInit() {
        if (vm.devices.length === 0) {
            DevicesService.load();
        }
    }

    function reviseDevices() {
        for (var i = 0; i < vm.devices.length; i++) {
            var device = vm.devices[i];
            if (device.status === 'Online' || device.status === 'Open') {
                device.status_bool = '1'; //jshint ignore:line
            } else {
                device.status_bool = '0'; //jshint ignore:line
            }

            if (vm.gatewayOnline !== '1') {
                device.offline_status = 'Offline'; //jshint ignore:line
                if (device.status === 'Open' || device.status === 'Closed') {
                    device.offline_status = 'Closed'; //jshint ignore:line
                }
            }
        }
    }

    function doRefresh() {
        console.log(vm.devices);
        var devices;
        devices = angular.copy(vm.devices);
        vm.devices = devices;
        DevicesService.load().then(function (result) { //jshint ignore:line
            vm.devices = DevicesService.list;
            reviseDevices();
            $scope.$broadcast('scroll.refreshComplete');
        }, function (err) {
            vm.devices = DevicesService.list;
            reviseDevices();
            console.log('search failed=', err);
            $scope.$broadcast('scroll.refreshComplete');
        });
    }

    function url(path) {
        return $sce.trustAsResourceUrl(apiUrl + '/' + path);
    }

    function onCameraRequestClick(device) {
        var camDevice = device;
        if (camDevice.status === 'Online') {
            camDevice.recIPCAM(1).then(function () {
                console.log('success');
            }, function () {
                console.log('failed');
            });
        }
    }

    function changeStatus(device) {
        device.toggle();
    }
}
