'use strict';

//jshint ignore:start
angular.module('app.application')
    .service('DevicesService', function ($q, $http, Device, $rootScope, ERRORS) {

        this.list = [];
        this.cams = [];
        this.ircams = [];
        this.camcoders = [];
        this.selCam = null;
        this.busy = false;
        this.loaded = false;
        this.promise = null;

        this.reload = function () {
            this.loaded = false;
            this.load();
        };

        this.load = function (monitoringServiceToken) {
            var self = this;
            var deferred = $q.defer();

            if (this.busy) {
                deferred.reject();
            }

            this.busy = true;
            this.clear();
            this.changeFlag = false;

            if (monitoringServiceToken) {
                var requestUrl = "/monitor/devices?session_id=" + monitoringServiceToken;
            } else {
                var requestUrl = "/devices";
            }

            $http.get($rootScope.urlBackend + requestUrl)
                .then(function (response) {
                    var gateway_array = {
                        actions: [],
                        area: "0",
                        'control-url': "",
                        'dashboard-order': "0",
                        faults: {
                            'low-battery': 0,
                            'tempered': 0,
                            'supervision': 0,
                            'out-of-order': 0,
                            'no-response': 0
                        },
                        icon: "assets/images/svg/gateway-a1.svg",
                        id: "", //XF:001d94037ec3
                        name: "Gateway",
                        status: "Online",
                        statusEx: "",
                        statusIcons: "",
                        type: "",
                        typeTag: "",
                        zone: "4"
                    };
                    response.data.push(gateway_array);
                    if (response.data) {
                        // first clear the list
                        while (self.list.length > 0) self.list.pop();
                        for (var i = 0; i < response.data.length; i++) {
                            var d = new Device(response.data[i]);
                            self.list.push(d);
                            if (d.mac != "")
                                self.cams.push(d);
                            if (d.typeTag == 'device_type.ir_camera')
                                self.ircams.push(d);
                            if (d.typeTag == 'device_type.ir_camcoder')
                                self.camcoders.push(d);
                        }
                        self.loaded = true;
                        self.busy = false;
                        deferred.resolve();
                        $rootScope.$broadcast('devices:loaded', this);
                    }
                    else
                        deferred.reject();
                    // throw response.data;
                }, function (err) {
                    self.busy = false;
                    console.log('Error in loading devices', err);
                    deferred.reject();
                });

            return deferred.promise;
        };


        this.rtDeviceUpdate = function (device_id) {
            var self = this;

            $http.get($rootScope.urlBackend + "/devices/" + device_id)
                .then(function (response) {
                    if (response.data) {
                        for (var i = 0; i < self.list.length; i++) {
                            if (self.list[i].id == device_id) {
                                self.list[i].update(response.data[0]);
                            }
                        }
                    }
                });
        };

        // ? add -> post /devices
        this.add = function (id, area, zone, name, icon) {
            var self = this;
            var url = "/devices/add";
            $http.put($rootScope.urlBackend + url).success(function (response) {
                // set post data
                // parse response
                // add in devices list
            });
        };

        this.del = function (id) {
            var self = this;
            var url = "/devices/" + id;
            // $http.del(url).success(function(response) {
            $http['delete']($rootScope.urlBackend + url)
                .then(function (response) {
                    for (var i = 0; i < self.list.length; i++) {
                        if (self.list[i].id == id) {
                            self.list.splice(i, 1);
                            break;
                        }
                    }
                    self.changeFlag = true;
                    console.log(ERRORS.DEVICE_DELETED_SUCCESSFULLY);
                }.bind(this))['catch'](function (data, status, headers, config) {
                // TODO: Handle Error;
                this.busy = false;
            }.bind(this))['finally'](function () {
            }.bind(this));
        };

        this.clear = function () {
            while (this.list.length > 0) this.list.pop();
            while (this.cams.length > 0) this.cams.pop();
            while (this.ircams.length > 0) this.ircams.pop();
            while (this.camcoders.length > 0) this.camcoders.pop();
            this.selCam = null;
            this.loaded = false;
        };

        this.setCamera = function (cam) {
            this.selCam = cam;
            $rootScope.$broadcast('cam:selected', this.selCam);
        };

        this.searchCamById = function (cam) {
            for (var i = 0; i < this.list.length; i++) {
                var device = this.list[i];
                if (device.id == cam.id) {
                    return device;
                }
            }
            console.log("Camera not found, ID=", cam.id);
            return null;
        };

        this.getDeviceByAction = function (action) {
            for (var i = 0; i < this.list.length; i++) {
                var device = this.list[i];

                if (action != '') {
                    for (var j = 0; j < device.actions.length; j++) {
                        if (action == device.actions[j].value) {
                            return device;
                        }
                    }
                }

            }
            return null;
        }

    });
