'use strict';

angular.module('app.application')
    .controller('SubscriptionController', SubscriptionController);

SubscriptionController.$inject = ['BillingService', '$ionicHistory', '$state', 'localStorageService', 'Restangular', 'apiVersion', 'MessageService'];
function SubscriptionController(BillingService, $ionicHistory, $state, localStorageService, Restangular, apiVersion, MessageService) {
    var vm = this;

    vm.steps = [];
    vm.steps.push(vm.currentStep = 1);
    vm.tab = 0;
    //vm.user = localStorageService.get('user');
    vm.user = Restangular.one(apiVersion + 'user').get();
    vm.user.then(function (data) {
        if (!data.address || !data.city || !data.state || !data.zip || !data.country) {
            MessageService.showErrorMessage('Error', 'Address not found!');
        }
        vm.selectedPlan = data.plan || 'Basic'; //jshint ignore:line
    });
    vm.nextWindow = nextWindow;
    vm.previousWindow = previousWindow;
    vm.selectTab = selectTab;
    vm.selectPlan = selectPlan;
    vm.submitPlan = submitPlan;
    vm.onBillingUpgradeClick = onBillingUpgradeClick;

    BillingService.billingPlansGET().then(function (data) {
        console.log(data);
        vm.plans = data;
    });

    function nextWindow(step) {
        vm.steps.push(step);
        vm.currentStep = step;
    }

    function previousWindow() {
        vm.steps.pop(-1);
        if (!!vm.steps.length) {
            vm.currentStep = vm.steps[vm.steps.length - 1];
        } else {
            $ionicHistory.goBack();
        }
    }

    function selectTab(index) {
        vm.tab = index;
    }

    function selectPlan(name, index) {
        vm.selectedPlan = name;
        vm.selectedPlanId = index;
    }

    function submitPlan() {
        if (vm.selectedPlan === 'Basic') {
            BillingService.billingPlansPUT({id: vm.plans[vm.selectedPlanId].id}).then(function (data) {
                if (data) {
                    nextWindow(3);
                }
            });
        } else {
            nextWindow(2);
        }

    }

    function onBillingUpgradeClick() { //jshint ignore:line
        var user = localStorageService.get('user');
        var req = {
            id: vm.plans[vm.selectedPlanId].id,
            name_on_the_card: vm.name_on_the_card, //jshint ignore:line
            card_no: vm.card_no, //jshint ignore:line
            expiration_month: vm.expiration_month, //jshint ignore:line
            expiration_year: vm.expiration_year, //jshint ignore:line
            cvc: vm.cvc,
            address: user.address,
            city: user.city,
            state: user.state,
            zip: user.zip,
            country: user.country
        };
        BillingService.billingPlansPUT(req).then(function (data) {
            if (data) {
                nextWindow(3);
            }
        });
    }
}