'use strict';

//jshint ignore:start
angular.module('app.application')
    .controller('SettingsController', SettingsController)

    .service('LoginService', function ($http, $rootScope, $state, md5,
                                       DevicesService, EventsService, GatewayService, User, RegisterService, CameraService, socket,
                                       $q, $location, ERRORS, MessageService, localStorageService, $cordovaDevice) {

        this.emailsent = false;
        this.email = '';
        this.authenticated = false;
        this.busy = false;

        this.setAuthCodeParam = function (key, userObj) {
            this.key = key;
            this.userObj = userObj;
            // socket.connect();
        };

        this.checkLogin = function () {
            // should be changed

            if (this.authenticated) return;
            // if($cookies.key){
            //   $http.defaults.headers.common['ABODE-API-KEY'] = $cookies.key;
            //   this.authenticated = true;
            //   socket.connect();
            //   $rootScope.$broadcast('com.goabode.my.login', true);
            // }
            // else
            //   $state.go('login');


            var authdata = localStorageService.get("authData");
            // check last login information
            if (authdata && authdata.token != undefined && authdata.token != '' && authdata.expire != '' && authdata.expire != undefined) {
                var expire_date = new Date(authdata.expire.replace(/-/g, "/"));
                var now_date = new Date();
                if (expire_date > now_date) {
                    var token = authdata.token;
                    $http.defaults.headers.common['ABODE-API-KEY'] = token;
                    this.authenticated = true;
                    socket.connect();
                    User.update(localStorageService.get("CurrentUser"));
                    // console.log(localStorageService.get("CurrentUser"));
                } else {

                    this.authenticated = false;
                    if (authdata.expire != undefined && authdata.expire != '')
                        MessageService.showErrorMessage("Token Expired", "Login token expired. Please login again.");
                }
            }

            if (this.authenticated == false) {

                if (localStorageService.get("welcome_screen") == undefined) {
                    $state.go("welcome");
                    localStorageService.set("welcome_screen", true);
                } else {
                    $rootScope.sign_back_button_av = false;
                    $rootScope.$broadcast('change_login_status', false);
                    $state.go("signin");
                }
            } else {
                if (localStorageService.get("authData").token != '' && localStorageService.get("authData").token != undefined) {
                    if (User.step == '-1') {
                        $state.go('tab.status.timeline');
                    }
                    else if (User.step == '2') {
                        $state.go("gatewaysetup");
                    } else if (User.step == '3') {
                        $state.go("devicesetup");
                    } else if (User.step == '4') {
                        $state.go("tab.status.timeline");
                    } else {
                        $state.go("profilestart");
                    }


                }
            }

        };

        this.login = function (user2, pass) {
            this.emailsent = false;
            $http.defaults.headers.common['ABODE-API-KEY'] = '';
            if (this.busy) return;
            this.busy = true;
            this.step = '';
            var hash = md5.createHash(pass);
            // blockUI.start();
            var deferred = $q.defer();
            MessageService.showLoading("Signing...");

            // var apns_token = localStorageService.get("apns_token");
            var login_data = {
                id: user2,
                password: hash
                // apns_token: localStorageService.get("deviceToken"),
                // device_name:$cordovaDevice.getName(),
                // device_model:$cordovaDevice.getModel()   // getName is deprecated as of version 2.3.0
            };

            var self = this;


            $http.post($rootScope.urlBackendReg + "/auth2/login", login_data).then(function (result) {

                var response = result.data;
                MessageService.hideLoading();
                console.log(result);


                var token = response.token;
                // if (token == undefined) {
                //   MessageService.showErrorMessage("Login Failed", response);
                //   deferred.reject("failed");
                //   return deferred.promise;
                // }
                self.authenticated = true;
                $http.defaults.headers.common['ABODE-API-KEY'] = token;

                localStorageService.set('authData', {
                    expire: response.expired_at,
                    token: token
                });


                User.update(response.user);
                // RegisterService.update_step('2');

                if (response.user.step == "-1") {
                    $state.go('tab.status.timeline');
                    // $cookies.key = token;
                    socket.connect();
                    $rootScope.$broadcast('com.goabode.my.login', true);
                    // $rootScope.screenLocked = false;
                }
                else {

                    if (response.user.step == '2') {
                        $state.go("gatewaysetup");
                    } else if (response.user.step == '3') {
                        $state.go("devicesetup");
                    } else if (response.user.step == '4') {
                        $state.go("tab.status.timeline");
                    } else {
                        $state.go("profilestart");
                    }

                }

                self.busy = false;
                deferred.resolve("success");


            }, function (err) {
                MessageService.hideLoading();
                self.busy = false;
                console.log("Error=", err.data.message);
                if (err.data != undefined && err.data.message == "You need to verify your email address before you can continue.") {
                    // Show Registration Message
                    deferred.resolve("register");
                }
                else if (err.data.message == undefined || err == undefined) {
                    MessageService.showErrorMessage("Login Failed", "Please try again.");
                } else if (err.status == 400) {
                    MessageService.showErrorMessage("Login Failed", err.data.message);
                } else {
                    MessageService.showErrorMessage(err.data.message, err.errors[0].message);
                }
                deferred.reject("failed");

            });

            return deferred.promise;
        };

        this.verify = function (authcode) {
            var self = this;
            var deferred = $q.defer();
            // var bui_verify = blockUI.instances.get('bui_verify');
            // bui_verify.start();

            $http.get($rootScope.urlBackendReg + "/reg/verify?authcode=" + authcode)
                .then(function (response) {

                    //Error Handling
                    if (response.data.result == 0) {
                        // toastr.error(ERRORS.AUTH_CODE_INVALID, null, $rootScope.toastrOpts);
                        MessageService.showError("Authentication Error", ERRORS.AUTH_CODE_INVALID);
                        deferred.reject(ERRORS.AUTH_CODE_INVALID);
                    }//bui_verify
                    else {
                        self.setAuthCodeParam(response.data.message.token, response.data.message.user);
                        //toastr.success('You have successfully verified your email address!', null, $rootScope.toastrOpts);
                        deferred.resolve('You have successfully verified your email address!');
                    }

                })
                ['catch'](function (msg) {
                // toastr.error(ERRORS.VERIFY_EMAIL_INVALID, null, $rootScope.toastrOpts);
                MessageService.showError("Authentication Error", ERRORS.VERIFY_EMAIL_INVALID);
                deferred.reject(ERRORS.VERIFY_EMAIL_INVALID);
            })
                ['finally'](function () {
                // bui_verify.stop();
            });
            return deferred.promise;
        };

        this.loginWithToken = function (token, u) {
            this.authenticated = true;
            $http.defaults.headers.common['ABODE-API-KEY'] = token;
            User.update(u);
            console.log(u);
            console.log('login with token');
            if (u.step == '-1') {
                $rootScope.$broadcast('com.goabode.my.login', true);
                // $state.go('app.dashboard');
                $state.go('tab.status');
            }
            else if (u.step == '1') {
                // $state.go('registrationsteps');

            }
        };

        this.resend_notification = function (email) {
            this.emailsent = false;
            // blockUI.start();
            var deferred = $q.defer();

            $http.post($rootScope.urlBackendReg + "reg/resend", {'email': email})
                .then(function (response) {
                    console.log(response);
                    if (response.data.status != "404") {
                        this.emailsent = true;
                        // $state.go('resendverification');
                        MessageService.showSuccessMessage("Mail Send", "email sent successfully!");
                    }
                    else {
                        this.emailsent = false;
                        // toastr.error(response.data.error, null, $rootScope.toastrOpts);
                        MessageService.showError("Email Sent", response.data.error);
                    }

                    deferred.resolve(response);

                    // blockUI.stop();
                }.bind(this))
                ['catch'](function (msg) {
                // toastr.error(response.data, null, $rootScope.toastrOpts);

                // blockUI.stop();
                deferred.reject(msg);
            }.bind(this))
                ['finally'](function () {
                // blockUI.stop();
            }.bind(this));
            //  return deferred.promise;

        };

        this.clearAll = function () {
            // $modalStack.dismissAll();
            // $cookies.key = '';
            DevicesService.clear();
            GatewayService.clear();
            User.clear();
            EventsService.clear();
            EventsService.clearAlerts();
            EventsService.clearAlertGroups();
            CameraService.clear();
            // associates.clear();
            // userLog.clear();
            // automation.clear();
            // notifications.clear();
            // socket.disconnect();
            // while (socket.newEvents.length > 0)
            //   socket.newEvents.pop();
            // for(var key in $rootScope.modals)
            //   $rootScope.modals[key].close();
            // $rootScope.modals = {};
        };

        this.logout = function (toState, authError) {
            console.log('is authenticates');
            if (!authError) {
                this.busy = true;
                $http.post($rootScope.urlBackendReg + "/logout").then(function (response) {

                })["finally"](function () {
                    this.busy = false;
                }.bind(this));
            }

            this.clearAll();
            this.authenticated = false;
            localStorageService.set("authData", {token: "", expire: ""});
            $http.defaults.headers.common['ABODE-API-KEY'] = '';
            // $cookies.key = '';
            $rootScope.sign_back_button_av = false;
            $rootScope.$broadcast('com.goabode.my.login', false);
            $rootScope.$broadcast('change_login_status', false);

            $state.go(toState ? toState : 'signin');
        };

        this.forgot = function (user) {

            if (this.busy) return;
            this.busy = true;
            // blockUI.start();
            MessageService.showLoading("Sending...");
            $http.post($rootScope.urlBackendReg + "/reg/forget_password", {username: user})
                .success(function (response) {
                    console.log("success=", response);
                    if (!response.message) {
                        // toastr.error(response.error, null, $rootScope.toastrOpts);
                        MessageService.showErrorMessage("Forget Password", response.error);
                    } else {
                        // toastr.success(response.response, null, $rootScope.toastrOpts);
                        MessageService.showSuccessMessage("Reset Password", response.message);
                    }
                    // blockUI.stop();
                    MessageService.hideLoading();
                    this.busy = false;
                }.bind(this))
                .error(function (data, status, headers, config) {
                    // toastr.success(ERRORS.LOGIN_FAIL, null, $rootScope.toastrOpts);
                    console.log("error=", data);
                    MessageService.showErrorMessage("Login", ERRORS.LOGIN_FAIL);
                    this.busy = false;
                    MessageService.hideLoading();
                    // blockUI.stop();
                }.bind(this));

        };

        // this.forgotpasword=function(forgot_password,forogt_authcode,forgot_id){

        //   var deferred = $q.defer();
        //   blockUI.start();
        //   $http.post("/api/registration/forgetpassword_change", {"id":forgot_id,"auth-code":forogt_authcode,"password":forgot_password})
        //     .success(function(response) {
        //       if(response){
        //         console.log(response);
        //         blockUI.stop();
        //         toastr.success(ERRORS.PASSWORD_RESET, null, $rootScope.toastrOpts);
        //         deferred.resolve(ERRORS.PASSWORD_RESET);
        //       }
        //     }.bind(this))
        //     .error(function(data, status, headers, config) {
        //       if(status==404){
        //         toastr.error(ERRORS.EMAIL_CODE_INVALID, null, $rootScope.toastrOpts);
        //       }
        //       blockUI.stop();
        //     }.bind(this));
        //   return deferred.promise;
        // }
    })
    .controller("StreamCtrl", function ($scope, $rootScope, $element, $interval, Camera, DevicesService, MessageService, $cookies, $state, /*filterForm, eventsArray,*/ ERRORS) {

        var camera = null;
        $scope.streaming = false;
        $scope.name = '';
        $scope.errorCounter = 0;
        $scope.url1 = '';
        var timer = null;

        $scope.onImageLoad = function (id) {
            if (timer) {
                $interval.cancel(timer);
                timer = null;
            }
            if (camera)
                $scope.url1 = camera.getImageUrl();
        };

        $scope.onImageError = function (id) {
            if (camera)
                startStream(camera);
        };

        function loadNewUrl() {
            if (camera)
                $scope.url1 = camera.getImageUrl();
        }

        function stop() {
            if (camera)
                camera.stopStream();
            $scope.streaming = false;
            $scope.url1 = '';
            //$scope.errorCounter = 0;
            camera = null;
            if (timer) {
                $interval.cancel(timer);
                timer = null;
            }
            $scope.name = '';
        }

        function searchCam() {
            if (camera == null) return -1;
            var cams = DevicesService.cams;
            console.log('search cam');
            for (var i = 0; i < cams.length; i++)
                if (cams[i].mac == camera.mac)
                    return i;
            return -1;
        }

        $scope.next = function () {
            var cams = DevicesService.cams;
            var index = searchCam();
            if (index != -1) {
                $scope.streaming = false;
                console.log('cam next');
                var newIndex = (index == cams.length - 1) ? 0 : index + 1;
                //console.log('next', index, newIndex);
                DevicesService.setCamera(cams[newIndex]);
            }
        };

        $scope.prev = function () {
            var cams = DevicesService.cams;
            var index = searchCam();
            if (index != -1) {
                $scope.streaming = false;
                console.log('cam prev');
                var newIndex = (index == 0) ? cams.length - 1 : index - 1;
                //console.log('prev', index, newIndex);
                DevicesService.setCamera(cams[newIndex]);
            }
        };

        $scope.showHistory = function () {
            if (DevicesService.selCam) {
                filterForm.device = DevicesService.selCam;
                filterForm.event = eventsArray.getEventById(5000);
                filterForm.submit();
                $state.go('app.events');
            }
        };

        $scope.$on('$destroy', stop);

        function startStream(cam) {
            stop();
            var mac = cam.mac;
            $scope.name = cam.name;
            camera = new Camera(mac);
            camera.startStream()
                .then(function (result) {
                    if (result == 'cancelled') {
                        $scope.stream_notification = 'Streaming cancelled.';
                    }
                    else {
                        $scope.url1 = camera.getImageUrl();
                        $scope.streaming = true;
                    }
                    // console.log('Stream: ', result);
                }, function (reason) {
                    camera = null;
                    // toastr.error(ERRORS.STREAMING_ERROR, null, $rootScope.toastrOpts);
                    MessageService.showRegisteredErrorMessage(ERRORS.STREAMING_ERROR, "Streaming Error");
                    //  console.log('StreamFailed: ' + reason);
                    $scope.stream_notification = 'Unable to start streaming.';
                }, function (msg) {
                    // console.log('StreamState: ' + msg);
                    $scope.stream_notification = msg;
                });
        }

        $scope.$on('cam:selected', function (event, cam) {
            console.log('cam:selected');
            if (cam != null && typeof cam === 'object') {
                //  console.log("Watch::" + cam.mac);
                startStream(cam);
            }
            else
                stop();
        });

        function selectDevice() {
            // check last selected camera mac from cookie
            // find that camera in cams
            console.log('selectDevice');
            if (DevicesService.cams.length > 0) {
                if (DevicesService.selCam == null)
                    DevicesService.setCamera(DevicesService.cams[DevicesService.cams.length - 1]);
                else
                    startStream(DevicesService.selCam);
            }
        }

        $scope.$on('devices:loaded', selectDevice);

        selectDevice();

    });

SettingsController.$inject = ['localStorageService', '$state', '$scope', '$ionicModal', '$ionicPopup', 'EventsService', 'GatewayService', 'AreasService', 'apiVersion', 'Restangular'];
function SettingsController(localStorageService, $state, $scope, $ionicModal, $ionicPopup, EventsService, GatewayService, AreasService, apiVersion, Restangular) {
    var vm = this;

    //TODO: REMOVE
    $scope.alerts = EventsService.alertsList;
    $scope.gateway = GatewayService;
    vm.openOfflineSupportWebsite = function () {
        window.open('https://support.goabode.com/success/s/article/Gateway-Offline', '_blank');
    };
    $scope.onAlertsClick = function () {
        //$ionicViewSwitcher.nextDirection('forward');
        $state.go('alert');
    };
    $scope.onClickVolume = function () {
        $scope.volume = {};
        AreasService.AreasGET().then(function (data) {
            $scope.volume.value = data.away_entry_beep;
            $ionicPopup.show({
                templateUrl: 'components/_global/templates/popups/popup_volume.html',
                // title: 'Select',
                scope: $scope,
                buttons: [{
                    text: 'OK',
                    type: 'abode-button volume-popup',
                    onTap: function () {
                        AreasService.AreasPUT($scope.volume.value);
                    }
                }]
            });
        })
    };
    $scope.onClickNotification = function (title) {
        $ionicPopup.show({
            templateUrl: 'components/_global/templates/popups/popup-quickaction.html',
            title: title,
            scope: $scope,
            buttons: [{
                text: 'Open in Browser',
                type: 'abode-button notif-popup',
                onTap: function () {
                    window.open('https://my.goabode.com', '_blank');
                }
            },
                {
                    text: 'OK',
                    type: 'abode-button notif-popup',
                    onTap: function () {
                        return true;
                    }
                }]
        });

    };

    if (window.cordova && cordova.platformId === 'ios') { //jshint ignore:line
        window.touchid.checkSupport(function () {
            vm.iosDevice = true;
        }, function () {
            vm.iosDevice = false;
        });
    } else {
        vm.iosDevice = false;
    }
    vm.user = localStorageService.get('user');
    vm.settings = localStorageService.get('settings');

    vm.settingsUpdate = function (name, value) {
        if (name === 'location' && value === false) {
            window.geofence.removeAll();
        }
        vm.settings[name] = value;
        localStorageService.set('settings', vm.settings);
    };
    vm.tutorialShow = function () {
        localStorageService.set('tutorial_show', 0);
        $state.go('tab.status.timeline');
    };
    vm.logOut = function () {
        //localStorageService.set('token', null);
        Restangular.one(apiVersion + 'logout').post().then(function (data) {
            localStorageService.remove('token');
            $state.go('signin');
        });
    };
}