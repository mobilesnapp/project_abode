'use strict';

angular.module('app.application')
    .config(SettingsConfig);

SettingsConfig.$inject = ['$stateProvider'];
function SettingsConfig($stateProvider) {
    $stateProvider
        .state('tab.settings', {
            url: '/settings',
            views: {
                'tab-settings': {
                    templateUrl: 'components/tabs/settings/settings.template.html',
                    controller: 'SettingsController as vm'
                }
            }
        })
        .state('settingsSubscription', {
            url: '/settingsSubscription',
            controller: 'SubscriptionController as vm',
            templateUrl: 'components/tabs/settings/subscription/settings.subscription.template.html'
        })
        .state('settingsAddDevices', {
            url: '/settingsAddDevices',
            templateUrl: 'components/tabs/settings/add-devices/settings.add-devices.template.html'
        })
        .state('settingsAddDevicesOther', {
            url: '/settingsAddDevicesOther',
            controller: 'SettingsController as vm',
            templateUrl: 'components/tabs/settings/otherdevices.html'
        });
}