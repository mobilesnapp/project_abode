'use strict';

// TODO REFACTORING
//jshint ignore:start
angular.module('app.application', [])
    .config(panelConfig)
    .run(['Sockets', function () {

    }]);

panelConfig.$inject = ['$stateProvider'];
function panelConfig($stateProvider) {
    $stateProvider
        .state('tab', {
            url: '/tab',
            abstract: true,
            templateUrl: 'components/tabs/tabs.template.html',
            controller: function ($scope, $rootScope, $state, GatewayService) {
                $scope.$on('$ionicView.enter', function () {
                    //if ($rootScope.status_tab == undefined || $rootScope.status_tab == '') {
                    //    $scope.click_tab = 'timeline';
                    //    $rootScope.status_tab = 'timeline';
                    //    $state.go('tab.status.timeline');
                    //}
                    GatewayService.init_connect();
                })
            }
        })
        .state('tab.quickaction', {
            url: '/quickaction',
            views: {
                'tab-quickaction': {
                    templateUrl: 'components/tabs/quickaction/quickaction.template.html',
                    controller: 'QuickactionController'
                }
            }
        })
        .state('tab.status', {
            url: '/status',
            views: {
                'tab-status': {
                    templateUrl: 'components/tabs/status/status.template.html',
                    controller: 'StatusCtrl as vm'
                }
            }
        })
        .state('tab.status.timeline', {
            url: '/timeline',
            views: {
                'status-view': {
                    templateUrl: 'components/tabs/status/timeline/timeline.template.html',
                    controller: 'TimelineCtrl'
                }
            }
        })
        .state('tab.status.cameras', {
            url: '/cameras',
            views: {
                'status-view': {
                    templateUrl: 'components/tabs/status/cameras/cameras.template.html',
                    controller: 'CameraCtrl as vm'
                }
            }
        })
        .state('tab.camera', {
            url: "/camera/:camera_id",
            views: {
                'tab-status': {
                    templateUrl: "components/tabs/status/cameras/camera_details/camera_details.template.html",
                    controller: 'CameraDetailsCtrl'
                }
            }
        })
        .state('tab.video', {
            url: "/video/:video_id",
            views: {
                'tab-status': {
                    templateUrl: "components/tabs/status/cameras/camera_details/video/video.template.html",
                    controller: 'VideoCtrl'
                }
            }
        })
        .state('tab.status.devices', {
            url: '/devices',
            views: {
                'status-view': {
                    templateUrl: 'components/tabs/status/devices/devices.template.html',
                    controller: 'DeviceCtrl as vm'
                }
            }
        })
        //.state('tab.settings', {
        //    url: '/settings',
        //    views: {
        //        'tab-settings': {
        //            templateUrl: 'components/tabs/settings/settings.html',
        //            controller: 'SettingsCtrl'
        //        }
        //    }
        //})

        //.state('addnewdevice_settings', {
        //    url: "/addnewdevice_settings",
        //    templateUrl: 'components/tabs/settings/addnewdevice/addnewdevice.html',
        //    //controller: 'NewDeviceCtrl'
        //})
        //.state('subscription', {
        //    url: "/subscription",
        //    templateUrl: 'components/tabs/settings/subscription.html',
        //    controller: function ($scope, $state, BillingService, $ionicHistory, $ionicViewSwitcher) {
        //
        //        $scope.methods = [];
        //
        //        $scope.onBackClick = function () {
        //            $ionicViewSwitcher.nextDirection('back');
        //            // $ionicGoBack();
        //            $ionicHistory.goBack();
        //        };
        //
        //        $scope.onClickItem = function (sel_item) {
        //            for (var i = 0; i < $scope.methods.length; i++) {
        //                $scope.methods[i].selected = false;
        //            }
        //            sel_item.selected = true;
        //            $scope.selected_method = sel_item;
        //
        //        };
        //
        //        $scope.init_billing = function () {
        //            BillingService.getBillingContent().then(function () {
        //                $scope.methods[0] = BillingService.starter;
        //                $scope.methods[1] = BillingService.premium;
        //                $scope.methods[2] = BillingService.standard;
        //                $scope.methods[3] = BillingService.basic;
        //
        //                $scope.methods[1].selected = true;
        //                $scope.methods[0].selected = false;
        //                $scope.methods[2].selected = false;
        //                $scope.methods[3].selected = false;
        //
        //                $scope.selected_method = $scope.methods[1];
        //            });
        //        };
        //
        //        $scope.onSubscriptionContinueClick = function () {
        //            BillingService.checkPackage($scope.selected_method.name);
        //            $state.go("subscription2");
        //        }
        //    }
        //})
        //.state('subscription2', {
        //    url: "/subscription2",
        //    templateUrl: 'components/tabs/settings/subscription2.html',
        //    // controller: 'SubscriptionCtrl'
        //    controller: function ($scope, $state, BillingService) {
        //        switch (BillingService.update_package) {
        //            case 'free':
        //                $scope.selected_method = BillingService.free;
        //                break;
        //            case 'starter':
        //                $scope.selected_method = BillingService.starter;
        //                break;
        //            case 'premium':
        //                $scope.selected_method = BillingService.premium;
        //                break;
        //            case 'standard':
        //                $scope.selected_method = BillingService.standard;
        //                break;
        //
        //        }
        //
        //        $scope.onBillingUpgradeClick = function () {
        //            BillingService.PackagesWithCard();
        //        }
        //
        //    }
        //})
        //.state('subscription3', {
        //    url: "/subscription3",
        //    templateUrl: 'components/tabs/settings/subscription3.html',
        //    controller: function ($scope, $state, BillingService) {
        //        switch (BillingService.update_package) {
        //            case 'free':
        //                $scope.selected_method = BillingService.free;
        //                break;
        //            case 'starter':
        //                $scope.selected_method = BillingService.starter;
        //                break;
        //            case 'premium':
        //                $scope.selected_method = BillingService.premium;
        //                break;
        //            case 'standard':
        //                $scope.selected_method = BillingService.standard;
        //                break;
        //        }
        //    }
        //})

        .state('otherdevices', {
            url: "/otherdevices",
            // views: {
            //   'addnewdevice_settings' : {
            //     templateUrl: 'templates/settings/addnewdevice.html',
            //     controller: 'NewDeviceCtrl'
            //   }
            // }
            templateUrl: 'components/tabs/settings/otherdevices.html',
            //controller: 'NewDeviceCtrl'
        })

        .state('addother_search', {
            url: "/addother_search",
            // views: {
            //   'addnewdevice_settings' : {
            //     templateUrl: 'templates/settings/addnewdevice.html',
            //     controller: 'NewDeviceCtrl'
            //   }
            // }
            templateUrl: 'components/tabs/settings/addother_search.html',
            //controller: 'NewDeviceCtrl'
        })

        .state('addother', {
            url: "/addother",
            // views: {
            //   'addnewdevice_settings' : {
            //     templateUrl: 'templates/settings/addnewdevice.html',
            //     controller: 'NewDeviceCtrl'
            //   }
            // }
            templateUrl: 'components/tabs/settings/addother.html',
            //controller: 'NewDeviceCtrl'
        })

        .state('addstreamcam', {
            url: "/addstreamcam",
            templateUrl: "components/tabs/settings/templates/addstreamcam/addstreamcam.html",
            //controller: "NewDeviceCtrl"
        })

        .state('addstreamcam2', {
            url: "/addstreamcam2",
            templateUrl: "components/tabs/settings/templates/addstreamcam/addstreamcam2.html",
            //controller: "NewDeviceCtrl"
        })

        .state('addstreamcam3', {
            url: "/addstreamcam3",
            templateUrl: "components/tabs/settings/templates/addstreamcam/addstreamcam3.html",
            //controller: "NewDeviceCtrl"
        })

        .state('addstreamcam4', {
            url: "/addstreamcam4",
            templateUrl: "components/tabs/settings/templates/addstreamcam/addstreamcam4.html",
            //controller: "NewDeviceCtrl"
        })

        .state('addstreamcam5', {
            url: "/addstreamcam5",
            templateUrl: "components/tabs/settings/templates/addstreamcam/addstreamcam5.html"
        })

        .state('addstreamcam6', {
            url: "/addstreamcam6",
            templateUrl: "components/tabs/settings/templates/addstreamcam/addstreamcam6.html",
            //controller: "NewDeviceCtrl"
        })

        .state('addstreamcam2_1', {
            url: "/addstreamcam2_1",
            templateUrl: "components/tabs/settings/templates/addstreamcam/addstreamcam2_1.html",
            //controller: "NewDeviceCtrl"
        })

        .state('addstreamcam2_2', {
            url: "/addstreamcam2_2",
            templateUrl: "components/tabs/settings/templates/addstreamcam/addstreamcam2_2.html",
            //controller: "NewDeviceCtrl"
        })

        .state('addwindow_search', {
            url: "/addwindow_search",
            templateUrl: "components/tabs/settings/templates/addwindow/addwindow_search.html",
            //controller: "NewDeviceCtrl"
        })

        .state('addmotioncam_search', {
            url: "/addmotioncam_search",
            templateUrl: "components/tabs/settings/templates/addmotioncamera/addmotioncam_search.html",
            //controller: "NewDeviceCtrl"
        })

        .state('addstreamcam_search', {
            url: "/addstreamcam_search",
            templateUrl: "components/tabs/settings/templates/addstreamcam/addstreamcam_search.html",
            //controller: "NewDeviceCtrl"
        })

        // Device Setup
        .state('clouddvr', {
            url: "/clouddvr",
            templateUrl: "components/tabs/settings/templates/devicesetup/clouddvr.html",
            //controller: "NewDeviceCtrl"
        })

}