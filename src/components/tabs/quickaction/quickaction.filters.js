'use strict';

//jshint ignore:start
angular.module('app.application')
    .filter('lock_unlock', function () {
        return function (input, is_selected) {
            var key_img = '';
            if (is_selected == true) {
                key_img = './img/Quick Actions/lock-white.png';
            } else {
                key_img = './img/Quick Actions/lock-black.png';
            }

            if (!input.status)
                return key_img;
            if (input.status == 'Offline' || input.status == 'Closed') {
                return key_img;
            } else {
                if (is_selected == 1) {
                    key_img = './img/Quick Actions/unlock-white.png';
                } else {
                    key_img = './img/Quick Actions/unlock-black.png';
                }
            }

            return key_img;
        }
    })
    .filter('ifEmpty', function () {
        return function (input, defaultValue) {
            if (angular.isUndefined(input) || input === null || input === '') {
                return defaultValue;
            }
            return input;
        }
    })
    .filter('capitalize', function () {
        return function (input) {
            return input.charAt(0).toUpperCase() + input.substr(1).toLowerCase();
        }
    })
    .filter('timersecond', function () {
        return function (input) {
            if (input < 10) {
                return input + " sec";
            } else if (input < 50) {
                return Math.ceil(input / 10) * 10 + " sec";
            } else {
                return Math.ceil(input / 60) + " min";
            }
        }
    });