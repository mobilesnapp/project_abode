'use strict';

//jshint ignore:start
angular.module('app.application')
    .controller('QuickactionController', QuickactionController)
    .controller('GatewayStatusCtrl', function ($scope, $state, $ionicPopup, $ionicPopover, GatewayService, $timeout, $interval) {
        $scope.area_type = '';
        $scope.gateway_error_type = '';

        $scope.gateway = GatewayService;
        $scope.home_timer = $scope.gateway.homeExitTimer;
        $scope.away_timer = $scope.gateway.awayExitTimer;

        $scope.selectedStatus = '';
        $scope.inProgressStatus = '';
        $scope.change_enable = true;

        var disable_timer = undefined;

        $scope.alarm_status = {
            motion: 0,
            sensor: 0
        };

        var change_alarm_status = function (gateway_status) {
            if (gateway_status == 'away') {
                $scope.alarm_status = {motion: 1, sensor: 1};
                $scope.change_enable = true;
            }

            if (gateway_status == 'home') {
                $scope.alarm_status = {motion: 0, sensor: 1};
                $scope.change_enable = true;
            }

            if (gateway_status == 'standby') {
                $scope.alarm_status = {motion: 0, sensor: 0};
                $scope.change_enable = true;
            }
        };
        GatewayService.init_connect().then(function () {
            $scope.selectedStatus = GatewayService.area1;
            change_alarm_status($scope.selectedStatus);
        });

        $scope.$watch(function () {
            return GatewayService.homeExitTimer
        }, function (newVaule, oldValue) {
            $scope.home_timer = newVaule;
            if (GatewayService.showhomeExitTimer == false) {
                $scope.inProgressStatus = '';
                $scope.area_type = '';
            }
            console.log("HOme Timer Changed = ", newVaule, "  old=", oldValue);
        });

        $scope.$watch(function () {
            return GatewayService.awayExitTimer
        }, function (newVaule, oldValue) {
            $scope.away_timer = newVaule;
            if (GatewayService.showAwayExitTimer == false) {
                $scope.inProgressStatus = '';
                $scope.area_type = '';
            }
            console.log("Away Timer Changed = ", newVaule, "  old=", oldValue);
        });

        $scope.onClickStatus = function (status) {
            if (status != $scope.selectedStatus && $scope.change_enable == true) {
                $scope.selectedStatus = status;
                $scope.area_type = '';
                $scope.inProgressStatus = status;
                $scope.change_enable = false;

                disable_timer = $timeout(function () {
                    disable_timer = undefined;
                }, 2000);  // 2 mins

                GatewayService.changeMode(status).then(function () {
                    if (disable_timer != undefined) {
                        $timeout.cancel(disable_timer);
                        disable_timer = undefined;
                    }

                    if (GatewayService.area1 == 'home') {
                        $scope.area_type = 'home';
                        //geolocationService.getCurrentPosition().then(function (position) {
                        //    geofenceService.createdGeofenceDraft = {
                        //        id: UUIDjs.create().toString(),
                        //        latitude: position.coords.latitude,
                        //        longitude: position.coords.longitude,
                        //        radius: 50,
                        //        transitionType: TransitionType.ENTER,
                        //        notification: {
                        //            id: geofenceService.getNextNotificationId(),
                        //            title: 'Home position',
                        //            text: '',
                        //            icon: '',
                        //            openAppOnClick: true
                        //        }
                        //    };
                        //    $scope.geofence = geofenceService.findById(geofenceService.createdGeofenceDraft.id);
                        //    geofenceService.addOrUpdate($scope.geofence);
                        //}, function (reason) {
                        //    console.log('Cannot obtain current location', reason);
                        //});

                        // $scope.timer = $scope.gateway.homeExitTimer;
                    } else if (GatewayService.area1 == 'away') {
                        $scope.area_type = 'away';
                        // $scope.timer = $scope.gateway.awayExitTimer;
                    } else {
                        $scope.inProgressStatus = '';
                    }

                    // change alarm status
                    change_alarm_status($scope.selectedStatus);

                }, function (err) {
                    $scope.gatewayError = err.error;
                    console.log(err);
                    if (GatewayService.area1 == 'home') {
                        $scope.gateway_error_type = 'Home';
                    } else if (GatewayService.area1 == 'away') {
                        $scope.gateway_error_type = 'Away';
                    } else {
                        $scope.gateway_error_type = 'Standby';
                    }

                    $scope.alertView = $ionicPopup.show({
                        templateUrl: 'components/_global/templates/popups/popup_gateway_error.html',
                        // title: 'Select',
                        scope: $scope,
                        buttons: [{
                            text: 'OK',
                            type: 'abode-button register-popup',
                            onTap: function () {
                            }
                        }]
                    });

                    // change alarm status
                    change_alarm_status($scope.selectedStatus);
                    $scope.inProgressStatus = '';
                });
            }

        };
        console.log("GatewayStatusCtrl...");
    });

QuickactionController.$inject = [
    '$scope',
    '$sce',
    '$ionicPopover',
    '$rootScope',
    '$cordovaInAppBrowser',
    '$ionicPopup',
    'apiUrl',
    'Automation',
    'AutomationRule',
    'GatewayService',
    'DevicesService'
];
function QuickactionController($scope,
                               $sce,
                               $ionicPopover,
                               $rootScope,
                               $cordovaInAppBrowser,
                               $ionicPopup,
                               apiUrl,
                               Automation,
                               AutomationRule,
                               GatewayService,
                               DevicesService) {
    var sub_click = false;
    $scope.gateway = GatewayService;
    $scope.devices = [];
    $scope.devices_selected_index = 0;
    $scope.openOfflineSupportWebsite = openOfflineSupportWebsite;
    $scope.selectRule = selectRule;
    $scope.isGroupShown = isGroupShown;
    $scope.showSubRulesClick = showSubRulesClick;
    $scope.onClickNewAction = onClickNewAction;
    $scope.onCameraRequestClick = onCameraRequestClick;
    $scope.url = url;
    $scope.toggleGroup = toggleGroup;
    $scope.changeStatus = function (device) {
        device.toggle();
    }

    $scope.popover = $ionicPopover.fromTemplateUrl('components/_global/templates/popups/popover-standby.html', {}).then(function (popover) {
        $scope.popover = popover;
    });
    $scope.$on('$ionicView.enter', function () {
        Automation.load();
        $scope.rules = Automation.rules;
        if ($scope.devices.length == 0) {
            DevicesService.load();
            $rootScope.$on("devices:loaded", function () {
                $scope.devices = DevicesService.list;
            });
        }
    });
    $scope.$on('popover.hidden', function (data) {
        // Execute action
        var result = document.getElementsByClassName("view-container");
        var wrappedResult = angular.element(result);
        wrappedResult.removeClass('blur-filter');
    });

    function openOfflineSupportWebsite() {
        window.open('https://support.goabode.com/success/s/article/Gateway-Offline', '_blank');
    }

    // UI Changes

    function clearAllSubItems(rule) {
        for (var i = 0; i < $scope.rules.length; i++) {
            if (rule != $scope.rules[i])
                $scope.rules[i].show_subitems = false;
        }
    }

    function clearAllSelectedItems() {
        for (var i = 0; i < $scope.rules.length; i++) {
            $scope.rules[i].select_item = false;
        }
    }

    function showSubRulesClick(rule, event) {
        sub_click = true;
        if (rule)
            Automation.selAutomation = angular.copy(rule);
        else
            Automation.selAutomation = new AutomationRule();

        $scope.ar = Automation.selAutomation;
        Automation.summary();

        clearAllSubItems(rule);

        $scope.toggleGroup(rule);
        if (!rule.show_subitems) {
            $scope.ar = [];
        }
    }

    function selectRule(rule) {
        if (sub_click) {
            sub_click = false;
            return;
        }
        clearAllSelectedItems();
        rule.select_item = true;

        rule.apply().then(function () {
            rule.apply_inprogress = false;
            console.log('apply success');
            rule.select_item = false;
            // rule.apply_toggle = true;
        }, function () {
            rule.apply_inprogress = false;
            rule.select_item = false;
            console.log('apply failed');
            // rule.apply_toggle = false;
        });

    }


    /*
     * if given group is the selected group, deselect it
     * else, select the given group
     */

    function toggleGroup(group) {
        group.show_subitems = !group.show_subitems;
    }

    function isGroupShown(group) {
        return group.show_subitems;
    }

    /*
     * New Quick Action Button Click
     */

    function onClickNewAction() {
        // An alert dialog

        $ionicPopup.show({
            templateUrl: 'components/_global/templates/popups/popup-quickaction.html',
            scope: $scope,
            title: 'Quick Action',
            buttons: [{
                text: 'OK',
                type: 'abode-button abode-popup',
                onTap: function () {
                    return true;
                }
            }]
        });
    }

    function onCameraRequestClick(device) {
        if (device.typeTag == 'device_type.ir_camera' || device.typeTag == 'device_type.ipcam') {
            var cam_device = device;
            if (cam_device.status === 'Online') {
                cam_device.recIPCAM(1).then(function () {
                    console.log('success');
                }, function () {
                    console.log('failed');
                });
            }
        }
    }

    function url(path) {
        return $sce.trustAsResourceUrl(apiUrl + '/' + path);
    }
}