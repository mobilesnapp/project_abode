'use strict';

//jshint ignore:start
angular.module('app.application')
    .directive('imageonload', function ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element[0].onerror = function () {
                    attrs.$set('ngHide', true);
                    attrs.$set('hide', null);
                    $compile(element)(scope);
                };
            }
        };
    })
    .directive('whenTimelineScrolled', function () {
        return function (scope, element, attr, $scope) {
            var raw = element[0];

            // binding on element doesn't work so this is a temp fix
            $(raw).bind('scroll', function () {
                //var scrollPercentage = (($(window).scrollTop() + $(window).height()) / $(document).height()) * 100;
                var scrollPercentage = ($(raw).scrollTop() + $(raw).height()) / (scope.events.alertsList.length * $('#alerts li:first-child').height()) * 100;
                console.log(scrollPercentage);
                if (scrollPercentage > 99 && !scope.in_progress && !scope.is_reached_end) {
//                    console.log('test');
                    // eventsService.load();
                    scope.$apply(attr.whenAlertEventsScrolled);
                }
            });
        };
    })
    .directive('imgLoad', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('load', function () {
                    scope.$apply(attrs.imgLoad);
                    //element.css('border',' 1px solid rgb(216, 210, 210)');
                });
            }
        };
    })
    .directive('resize', function ($window) {
        return function (scope, element, attrs) {
            var w = angular.element($window);
            w.bind("resize", function () {
                scope.$apply(attrs.resize);
            });

        }
    })
    .directive('imgError', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('error', function (arg1, arg2, arg3) {
                    console.log('image load error', arg1, arg2, arg3);
                    //call the function that was passed
                    scope.$apply(attrs.imgError);
                });
            }
        };
    });