'use strict';

//jshint ignore:start
angular.module('app.application')
    .constant("ERRORS", {
        "GENERAL_ERROR": 'Oops, something went wrong.  Please contact abode support.',
        "LOGIN_INVALID": "Incorrect Username or Password.",
        "LOGIN_FAIL": "Unable to call login service.",
        "LOGIN_ERROR": "Failed to login, some error occurred.",
        "New_EVENT_RECEIVED": "New event received!",
        "CAPTURE_FILE_DELETED": "Captured file(s) deleted successfully!",
        "MARK_ALL_AS_READ": "Error in marking all events as read!",
        "QUICK_ACTION_APPLIED_SUCCESSFULLY": "Quick Action applied successfully!",
        "QUICK_ACTION_APPLIED_FAILED": "Error in applying Quick Action.",
        "QUICK_ACTION_ADDED_SUCCESSFULLY": "Quick Action added successfully.",
        "AUTOMATION_ADDED_SUCCESSFULLY": "Automation Rule added successfully.",
        "AUTOMATION_SLOT_NOT_AVAILABLE": "Automation slot is not available!",
        "ERROR_ADDING_AUTOMATION_RULE": "Error in adding Automation Rule.",
        "QUICK_ACTION_EDITED_SUCCESSFULLY": "Quick Action edited successfully!",
        "AUTOMATION_RULE_EDITED_SUCCESSFULLY": "Automation Rule edited successfully!",
        "ERROR_EDITING_AUTOMATION_RULE": "Error in editing automation rule.",
        "QUICK_ACTION_DELETED_SUCCESSFULLY": "Quick Action deleted successfully.",
        "AUTOMATION_RULE_DELETED_SUCCESSFULLY": "Automation Rule deleted successfully.",
        "ERROR_DELETING_AUTOMATION": "Error in deleting Automation Rule.",
        "LOCATION_DELETED_SUCCESSFULLY": "Location deleted successfully!",
        "ERROR_DELETING_LOCATION": "Error in deleting Location.",
        "LOCATION_ADDED_SUCCESS": "Location added successfully!",
        "ERROR_ADDING_LOCATION": "Error in adding Location.",
        "RECORD_REQ_SENT_SUCCESSFULLY": "Record Request sent successfully!",
        "RECORD_REQ_FAILED": "Record Request failed!",
        "ERROR_CHANGING_STATUS": "Error in changing status.",
        "DEVICE_DELETED_SUCCESSFULLY": "Device deleted successfully!",
        "ADD_DEVICE_GATEWAY_NOT_STANDBY_MODE_ERROR": "Gateway should be in Standby mode to add new devices!",
        "ERROR_LEARNING_DEVICES": "Error in learning devices.",
        "EMPTY_DEVICE_NAME": "Device name field is empty.",
        "DEVICE_ADDED_SUCCESSFULLY": "Device added successfully!",
        "DEVICE_EDITED_SUCCESSFULLY": "Device edited successfully!",
        "STREAMING_ERROR": "Unable to start streaming.",
        "EMERGENCY_CONTACT_ADDED_SUCCESSFULLY": "Emergency contact added successfully!",
        "EMERGENCY_CONTACT_DELETED_SUCCESSFULLY": "Emergency contact deleted successfully!",
        "EMERGENCY_CONTACT_UPDATED_SUCCESSFULLY": "Emergency contact updated successfully!",
        "USER_DELETED_SUCCESS": "User deleted successfully!",
        "PROFILE_PIC_UPDATE_SUCCESS": "Profile Pic updated successfully!",
        "USER_NAME_REQ": "User name is required.",
        "FIRST_NAME_REQ": "First Name is required.",
        "LAST_NAME_REQ": "Last Name is required.",
        "PHONE_REQ": "Phone is required.",
        "PASSWORD_REQ": "Password is required.",
        "USER_PROFILE_UPDATED": "User profile updated successfully!",
        "PASSWORD_VALIDATION_MSG": "Password should contain at least 1 uppercase, 1 lowercase and a numeric value. It should have 6 characters or more.",
        "PASS_REPASS_MATCH_ERROR": "Password and Confirm Password should be same.",
        "PASS_UPDATED_SUCCESS": "Password updated successfully!",
        "FILL_THE_FORM": "Please fill in the form.",
        "ASSOCIATED_USER_ADDED": "Email has been sent on the associated user's account.!",
        "ASSOCIATED_USER_ADD_FAILED": "Error in adding associated user!",
        "ASSOCIATED_USER_UPDATE_FAILED": "Error in updating associated user!",
        "ASSOCIATED_USER_UPDATED": "Associated user updated successfully!",
        "ASSOCIATED_USER_DELETE_SUCCESS": "Associated user deleted successfully!",
        "ASSOCIATED_USER_DELETE_FAILED": "Error in deleting associated users.",
        "PACKAGE_UPDATED_SUCCESSFULLY": "Package updated successfully!",
        "EMAIL_CODE_INVALID": "Email code is expired or incorrect.",
        "PASSWORD_RESET": "Password has been changed successfully.",
        "AUTH_CODE_INVALID": "Authentication code is not valid.",
        "VERIFY_EMAIL_INVALID": "Error in verifying email address.",
        "ERROR_IN_EDITING_NOTIFICATION_CONTROL": "Error in changing notification control."
    })
    .service('AddDevicesService', function (User, MessageService, DevicesService, $q, $rootScope, $http, $ionicLoading, ERRORS) {

        var devicetimer = null;
        var selectedDevice = null;

        this.newdevicesLoad = [];
        this.ico = [];
        this.selectedDevice = null;
        this.wifi_list = [];
        this.selectedWifi = null;
        this.selectedWifiIndex = 0;
        this.new_device_input_flag = [-1, -1, -1, -1];

        $scope.deviceDelete = function (deviceId) {
            DevicesService.del(deviceId);
        };

        $scope.closeAddingDevice = function () {
            $http.put($rootScope.urlBackend + "/panel/learn/stop")
                .then(function (response) {
                    devicetimer = false;
                }.bind(this))
                ["catch"](function (data, status, headers, config) {
            }.bind(this))
                ["finally"](function () {
                $scope.busy = false;
            }.bind(this));
        };

        $scope.init_devices_setup = function () {
            GatewayService.init_connect()
                .then(function (data) {
                    $scope.openNewDevice();
                }, function (err) {
                    console.log("initGateway failed...", err);
                });
        };

        $scope.openNewDevice = function (device, index) {
            if (GatewayService.area1 != 'standby') {
                MessageService.showRegisteredErrorMessage(ERRORS.ADD_DEVICE_GATEWAY_NOT_STANDBY_MODE_ERROR, "Gateway Mode Error");
                return;
            }

            devicetimer = false;
            while (newdevicesLoad.length > 0)
                newdevicesLoad.pop();

            $scope.busy = true;
            $scope.editMode = false;
            $http.put($rootScope.urlBackend + "/panel/learn/start")
                .then(function (response) {
                    NewDeviceTimer();
                    devicetimer = true;
                }.bind(this))
                ["catch"](function (data, status, headers, config) {
                $rootScope.$broadcast('closeAddDeviceDialouge');
            }
                .bind(this))
                ["finally"](function () {
                $scope.busy = false;
            }.bind(this));
        };

        var NewDeviceTimer = function () {
            var counter = 0;
            $http.get($rootScope.urlBackend + "/devices/new")
                .then(function (response) {
                    if (response.data) {
                        for (var i = 0; i < response.data.length; i++) {
                            counter = 0;
                            for (var j = 0; j < newdevicesLoad.length; j++) {
                                if (newdevicesLoad[j].id == response.data[i].id) {
                                    counter = 1;
                                    break;
                                }
                            }
                            if (counter == 0) {
                                newdevicesLoad.push(response.data[i]);
                            }
                        }
                    }
                }.bind(this))
                ["catch"](function (data, status, headers, config) {
            }.bind(this))["finally"](function () {
                if (devicetimer) {
                    NewDeviceTimer();
                }
            }.bind(this));
        };

        var addNewDevice = function (device) {
            if (!$scope.icons[device.id]) {
                $scope.icons[device.id] = "";
            }
            $scope.busy = 1;

            $http.post("/api/v1/devices/" + device.id, {"name": device.name, "icon_id": icon_id})
                .then(function (response) {
                    if (response.data) {
                        for (var k = 0; k < $scope.newdevicesLoad.length; k++) {
                            if (response.data.id == $scope.newdevicesLoad[k].id) {
                                DevicesService.list.push(response.data);
                                $scope.newdevicesLoad.splice(k, 1);
                            }
                        }
                        var selectedDevice = null;
                        DevicesService.changeFlag = true;
                    }
                }.bind(this))
                ["catch"](function (data, status, headers, config) {
                MessageService.showErrorMessage("Add New Device Error", data.data.message);
            }.bind(this))["finally"](function () {
                this.busy = false;
            }.bind(this));
            return;
        };

        // Timer for searching windows
        var checkTimer, finalCheck;
        $scope.stopSearching = function () {
            if (angular.isDefined(checkTimer)) {
                $interval.cancel(checkTimer);
                checkTimer = undefined;
            }
        };

        $scope.checkSensorDevice = function (type) {
            if (angular.isDefined(checkTimer)) return;
            checkTimer = $interval(function () {
                for (var i = 0; i < newdevicesLoad.length; i++) {
                    var new_device = newdevicesLoad[i];
                    if (new_device.type == type) {  // Find Device
                        selectedDevice = new_device;
                        $scope.stopSearching();
                        $timeout.cancel(finalCheck);
                        if (type == "Door Contact") {
                            $state.go("addwindow");
                        }
                        else if (type == "Motion Camera") {
                            $state.go("addmotioncam");
                        }
                        else if (type == "IP Cam") {
                            $state.go("addstreamcam");
                        }
                        break;
                    }
                }

            }, 1000);

            finalCheck = $timeout(function () {
                // Cancel interval
                MessageService.showErrorMessage("Searching New Device", "No New Door/Window Sensor Detected");
                $scope.stopSearching();
                $state.go("devicesetup")
            }, 10000);  // 2 mins
        };

        // Streaming Camera

        var activateStreamingCam = function (act_code) {
            $http.post($rootScope.urlBackend + "/cams/activate", {"code": act_code})
                .then(function (response) {
                }, function (err) {
                    console.log("Streaming cam added failed, err=", err);
                });
        };

        var getWifiStreamList = function (device) {
            var deferred = $q.defer();
            var mac_addr = device.mac;
            $http.get($rootScope.urlBackend + "/cams/wifi", {"mac": mac_addr})
                .then(function (response) {
                    deferred.resolve(response);
                }, function (err) {
                    deferred.reject(err);
                });
            return deferred.promise;
        }
    })
    .factory('AutomationRule', function ($rootScope, $q, $http, ERRORS, MessageService) {

        var AutomationRule = function (item) {
            this.name = '';
            this.id = '';
            this.actions = '';
            this.actionsArr = [];
            this.condition = '';
            this.type = '';
            //this.locations = '';
            this.device = '';
            this.device_id = '';
            this.zone = '';
            this.device_condition = '';
            this.summaryArr = [];
            this.scheduleType = [];
            this.mobileDevicesArr = [];
            this.locationsArr = [];
            this.editMode = false;
            if (item) {
                this.update(item);
            }
        };

        AutomationRule.prototype.update = function (item) {
            this.name = item.name;
            this.id = item.id;
            this.actions = item.actions;
            this.condition = item.condition;
            this.type = item.type;
            //this.locations = item.locations;
            this.device_id = item.device_id;
            this.device_condition = item.device_condition;
            var str = item.actions;
            this.actionsArr = str.split(";");
            for (var i = 0; i < this.actionsArr.length; ++i)
                this.actionsArr[i] += ';';
            if (this.actionsArr.length > 0 && this.actionsArr[this.actionsArr.length - 1] == ';')
                this.actionsArr.pop();
        };

        AutomationRule.prototype.apply = function () {
            var self = this;
            // var bui = blockUI.instances.get('bui_' + self.id);
            // if(bui)
            //   bui.start();
            var deferred = $q.defer();

            // $http.put($rootScope.urlBackend + '/automation/apply', {no : self.id})
            $http.put($rootScope.urlBackend + '/automation/' + self.id + '/apply')
                .then(function (response) {
                    if (response.status == 200) {
                        MessageService.showRegisteredErrorMessage(ERRORS.QUICK_ACTION_APPLIED_SUCCESSFULLY, "Automation");
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }
                })
                ['catch'](function () {
                deferred.reject();
                MessageService.showRegisteredErrorMessage(ERRORS.QUICK_ACTION_APPLIED_FAILED, "Automation");
            })
                ['finally'](function () {
            });

            return deferred.promise;
        };

        return AutomationRule;
    })
    .service('Automation', function ($q, $http, AutomationRule, MessageService, $rootScope, Device, DevicesService, ERRORS) {

        this.mobileDevicesList = [];
        this.locationsList = [];
        this.loaded = false;
        this.mobileDevicesLoaded = false;
        this.locationsLoaded = false;
        this.selAutomation = new AutomationRule();
        this.busy = false;
        this.addBusy = false;
        this.mobileDevicesBusy = false;
        this.locationsBusy = false;
        this.quickActionsNum = 0;
        this.automationRulesNum = 0;

        this.reloadAutomation = function () {
            this.loaded = false;
            this.load();
        };

        this.load = function () {

            DevicesService.load();  // for test

            this.busy = true;
            var self = this;
            self.rules = [];
            MessageService.showLoading("Loading...");
            $http.get($rootScope.urlBackend + "/automation")
                .then(function (response) {
                    MessageService.hideLoading();
                    if (response.data) {
                        self.quickActionsNum = 0;
                        self.automationRulesNum = 0;
                        for (var i = 0; i < response.data.length; i++) {
                            var d = new AutomationRule(response.data[i]);
                            self.rules.push(d);
                            //Device Based Automation parsing
                            if (self.rules[i].type == 'device') {
                                var result = '';
                                var deviceRegx = /a=(\d+)\&z=(\d+)\&trigger=(\d+)/;
                                result = self.rules[i].condition.match(deviceRegx);

                                for (var objProp in result) {
                                    if (objProp == 2)
                                        self.rules[i].zone = result[objProp];
                                    else if (objProp == 3)
                                        self.rules[i].device_condition = result[objProp];
                                }

                                //Now Handled in APP.JS

//            for (var j = 0; devicesService.list.length > j; j++){
//              if (devicesService.list[j].zone == self.rules[i].zone){
//                self.rules[i].device_id = devicesService.list[j].device_id;
//                self.rules[i].device = devicesService.list[j];
//              }
//            }
                            }

                            //Calculating Number of Quick Actions && Automation Rules
                            if (self.rules[i].type == 'manual')
                                self.quickActionsNum++;
                            else
                                self.automationRulesNum++;
                        }
                        self.loaded = true;
                    }
                })['catch'](function (msg) {
                console.log('Error in loading automation rules', msg);
                MessageService.hideLoading();
            })['finally'](function () {
                self.busy = false;
                MessageService.hideLoading();
            });
        };

        this.loadMobileDevices = function () {
            if (this.mobileDevicesLoaded) return;
            var self = this;
            self.mobileDevicesBusy = true;
            $http.get($rootScope.urlBackend + "/automation/devices").then(function (response) {
                if (response.data.devices) {
                    for (var i = 0; i < response.data.devices.length; i++) {
                        self.mobileDevicesList.push(response.data.devices[i]);
                    }
                    self.mobileDevicesLoaded = true;
                }
            })['catch'](function (msg) {
                console.log('Error in loading mobile devices', msg)
            })['finally'](function () {
                self.mobileDevicesBusy = false;
            });
        };

        this.getMobileDeviceName = function (mobileDeviceId) {
            var self = this;
            for (var i = 0; self.mobileDevicesList.length > i; i++) {
                if (self.mobileDevicesList[i]['notify_id'] == mobileDeviceId)
                    return self.mobileDevicesList[i]['device_name'];
            }
        };

        this.loadLocations = function () {
            if (this.locationsLoaded) return;
            var self = this;
            self.locationsBusy = true;

            $http.get($rootScope.urlBackend + "/locations")
                .then(function (response) {
                    if (response.data.locations) {
                        for (var i = 0; i < response.data.locations.length; i++) {
                            self.locationsList.push(response.data.locations[i]);
                        }
                        self.locationsLoaded = true;
                    }
                })['catch'](function (msg) {
                console.log('Error in loading locations.', msg)
            })['finally'](function () {
                self.locationsBusy = false;
            });
        };

        this.getLocationName = function (locationId) {
            var self = this;
            for (var i = 0; self.locationsList.length > i; i++) {
                if (self.locationsList[i].id == locationId) {
                    return self.locationsList[i].name;
                }
            }
        };

        this.check = function () {
        };

        this.dayPostFix = function (day) {
            var newDayStr = "";
            var intDay = parseInt(day);

            if (intDay > 10 && intDay < 20)
                return newDayStr = day + "th";

            var dayMod = intDay % 10;

            switch (dayMod) {
                case 1:
                    newDayStr = day + "st";
                    break;
                case 2:
                    newDayStr = day + "nd";
                    break;
                case 3:
                    newDayStr = day + "rd";
                    break;
                default:
                    newDayStr = day + "th";
                    break;
            }
            return newDayStr;
        };

        this.summary = function () {
            var self = this;
            var dName = '';
            var dActionLabel = '';
            var dActionString = '';
            var actionArray = [];
            var summaryString = '';


            while (self.selAutomation.summaryArr.length > 0)
                self.selAutomation.summaryArr.pop();

            for (var i = 0; i < self.selAutomation.actionsArr.length; i++) {
                //console.log(self.selAutomation.actionsArr[i]);
                dName = self.selAutomation.actionsArr[i].deviceName;
                dActionLabel = self.selAutomation.actionsArr[i].deviceAction;
                dActionString = self.selAutomation.actionsArr[i].actionString;

                if (self.selAutomation.actionsArr[i].actionString)
                    dActionString = self.selAutomation.actionsArr[i].actionString;
                else
                    dActionString = self.selAutomation.actionsArr[i];

                actionArray.push(dActionString);

                var dDetail = DevicesService.getDeviceByAction(dActionString);
                if (!dName) {
                    // Have to Handle ModeChange/VideoAll/ImageAll Case
                    if (!dDetail) {
                        switch (dActionString) {
                            case 'a=1&m=0;':
                                // dName = "Change System Mode";
                                dName = "Mode";
                                dActionLabel = "Standby";
                                break;
                            case 'a=1&m=2;':
                                // dName = "Change System Mode";
                                dName = "Mode";
                                dActionLabel = "Home";
                                break;
                            case 'a=1&m=1;':
                                // dName = "Change System Mode";
                                dName = "Mode";
                                dActionLabel = "Away";
                                break;
                            case 'a=1&req=vid_all;':
                                // dName = "Capture Video From all Motion & IP Cameras";
                                dName = "All Cam Video";
                                dActionLabel = "All Video";
                                break;
                            case 'a=1&req=img_all;':
                                // dName = "Capture Images From all Motion Cameras";
                                dName = "All Cam Image";
                                dActionLabel = "Flash";
                                break;
                            case 'a=1&req=img_all_nf;':
                                // dName = "Capture Images From all Motion Cameras";
                                dName = "All Cam Image";
                                dActionLabel = "No Flash";
                                break;
                            default:
                                break;
                        }
                    }
                    else {
                        dName = dDetail.name;

                        for (var j = 0; j < dDetail.actions.length; j++) {
                            if (dActionString == dDetail.actions[j].value) {
                                dActionLabel = dDetail.actions[j].label;
                            }
                        }
                    }
                }

                //Makinng Summary String
                if (!dDetail) {
                    switch (dName) {
                        // case 'Change System Mode':
                        case 'Mode':
                            summaryString = dName + " to " + dActionLabel;
                            break;
                        // case 'Capture Video From all Motion & IP Cameras':
                        case 'All Cam Video':
                            summaryString = dName;
                            break;
                        // case 'Capture Images From all Motion Cameras':
                        case 'All Cam Image':
                            if (dActionLabel == 'Flash')
                            // summaryString = dName + " with Flash.";
                                summaryString = dName + " Flash";
                            else
                            // summaryString = dName + " without Flash.";
                                summaryString = dName + " No Flash";
                            break;
                        default:
                            break;
                    }
                }
                else {
                    switch (dDetail.typeTag) {
                        case 'device_type.ir_camera':
                            if (dActionLabel == 'Flash')
                                summaryString = dName + " Capture Flash";
                            else
                                summaryString = dName + " Capture No Flash";
                            break;
                        case 'device_type.ir_camcoder':
                        case 'device_type.ipcam':
                            summaryString = dName + "Video";
                            break;
                        case 'device_type.dimmer':
                            if (dActionLabel.indexOf('for') > 0)
                            // summaryString = "Switch on '" + dName + "' for " + dActionLabel.substring(14) + ".";
                                summaryString = dName + " on";
                            else if (dActionLabel == "Switch on" || dActionLabel == "Switch off" || dActionLabel == "Toggle")
                                summaryString = dName + dActionLabel;
                            else
                                summaryString = dName + ' to ' + dActionLabel;
                            break;
                        case 'device_type.power_switch_sensor':
                        case 'device_type.power_switch_meter':
                            if (dActionLabel.indexOf('for') == -1)
                                summaryString = dActionLabel + " " + dName;
                            else
                                summaryString = dName + " on";
                            break;
                        case 'device_type.door_lock':
                            summaryString = dActionLabel + " " + dName;
                            break;
                        default:
                            break;
                    }
                }

                self.selAutomation.summaryArr.push({
                    deviceName: dName,
                    deviceActionLabel: dActionLabel,
                    deviceActionString: dActionString,
                    summaryString: summaryString
                });
            }

            self.selAutomation.actions = actionArray.toString().replace(/\,/g, '');
        };

        this.add = function () {
            if (this.addBusy) return;

            var deferred = $q.defer();
            var self = this;
            self.addBusy = true;
            var sceneName = self.selAutomation.name;
            var actionString = self.selAutomation.actions;
            var device_id = self.selAutomation.device_id;
            var device_condition = self.selAutomation.device_condition;

            var condition = '';
            var type = '';
            switch (self.selAutomation.type) {
                case 'status':
                    type = 'status';
                    condition = self.selAutomation.condition;
                    break;
                case 'device':
                    type = 'device';
                    condition = self.selAutomation.condition;
                    condition = 'a=1&z=' + self.selAutomation.zone + '&trigger=' + self.selAutomation.device_condition;
                    break;
                case 'location':
                    type = 'location';
                    condition = self.selAutomation.condition;
                    //console.log(condition);
                    break;
                case 'schedule':
                    type = 'schedule';
                    condition = self.selAutomation.condition;
                    break;
                case 'manual':
                    type = 'manual';
                    condition = '';
            }


            // var bui = blockUI.instances.get('AutomationModelBlockUI');
            // bui.start();

            $http.post($rootScope.urlBackend + '/automation', {
                type: type,
                name: sceneName,
                actions: actionString,
                condition: condition
            }).then(function (response) {
                if (response.data) {
                    var rule = new AutomationRule(response.data);
                    self.rules.push(rule);

                    if (rule.type == 'manual')
                        self.quickActionsNum++;
                    else
                        self.automationRulesNum++;
                    MessageService.showSuccessMessage("Automation", (rule.type == 'manual' ? ERRORS.QUICK_ACTION_ADDED_SUCCESSFULLY : ERRORS.AUTOMATION_ADDED_SUCCESSFULLY));
                    deferred.resolve(rule);
                }
                else if (response.data == "Automation Slot Not Available") {
                    MessageService.showAlertMessage("Automation", ERRORS.AUTOMATION_SLOT_NOT_AVAILABLE);
                    deferred.reject(ERRORS.AUTOMATION_SLOT_NOT_AVAILABLE);
                }
                else
                    throw 'Error in adding automation rule.';
            })['catch'](function (msg) {
                console.log('Error in adding automation rule', msg);
                deferred.reject(msg);
                MessageService.showRegisteredErrorMessage(ERRORS.ERROR_ADDING_AUTOMATION_RULE, "Automation");
            })['finally'](function () {
                self.addBusy = false;
            });
            return deferred.promise;
        };

        this.edit = function () {
            var deferred = $q.defer();
            var self = this;
            self.addBusy = true;
            //var locationsString = '';
            var ar = self.selAutomation;

            var condition = '';
            var device_id = null;
            var zone = null;
            var device_condition = null;

            switch (ar.type) {
                case 'status':
                    condition = ar.condition;
                    device_id = ar.device_id;
                    zone = ar.zone;
                    device_condition = ar.device_condition;
                    break;
                case 'device':
                    zone = ar.zone;
                    device_condition = ar.device_condition;
                    condition = 'a=1&z=' + zone + '&trigger=' + device_condition;
                    break;
                case 'location':
                    for (var i = 0; i < ar.mobileDevicesArr.length; i++) {
                        for (var k = 0; k < ar.locationsArr.length; k++) {
                            condition = condition + "l=" + ar.locationsArr[k].locationId + "&d=" +
                                ar.mobileDevicesArr[i] + "&r=" + ar.locationsArr[k].cond.toLowerCase() + ";";
                        }
                    }
                    break;
                case 'schedule':
                    condition = ar.condition;
                    break;
                case 'manual':
                    condition = '';
                    break;
            }

            $http.put($rootScope.urlBackend + '/automation/' + ar.id + '/edit', {
                type: ar.type,
                name: ar.name,
                actions: ar.actions,
                condition: condition
            }).then(function (response) {
                if (response.data) {
                    for (var i = 0; i < self.rules.length; i++) {
                        if (self.rules[i].id == response.data.id) {
                            self.rules[i].update(response.data);
                            //response.data.condition =

                            if (self.rules[i].type == 'device') {
                                var deviceRegx = /a=(\d+)\&z=(\d+)\&trigger=(\d+)/;
                                var result = '';
                                result = self.rules[i].condition.match(deviceRegx);

                                for (var objProp in result) {
                                    if (objProp == 2)
                                        self.rules[i].zone = result[objProp];
                                    else if (objProp == 3)
                                        self.rules[i].device_condition = result[objProp];
                                }

                                $rootScope.$broadcast('devices:loaded', this);
                            }
                            break;
                        }
                    }
                    MessageService.showSuccessMessage("Automation", (ar.type == 'manual' ? ERRORS.QUICK_ACTION_EDITED_SUCCESSFULLY : ERRORS.AUTOMATION_RULE_EDITED_SUCCESSFULLY));
                    deferred.resolve(ar);
                }
                else
                    throw 'Error in editing automation rule.';
            })['catch'](function (msg) {
                console.log('Error in editing automation rule', msg);
                MessageService.showSuccessMessage("Automation", ERRORS.ERROR_EDITING_AUTOMATION_RULE);
                deferred.reject(msg);
            })['finally'](function () {
                self.addBusy = false;
            });

            return deferred.promise;
        };

        this.del = function (rule) {
            var result = confirm("Do you want to delete this " + (rule.type == 'manual' ? 'Quick Action' : 'Automation Rule') + "?");
            if (result == true) {
                // var bui = blockUI.instances.get('bui_' + rule.id);
                // bui.start();

                var self = this;

                $http['delete']($rootScope.urlBackend + '/automation', {data: {"id": rule.id}})
                    .then(function (response) {
                        if (response.data.response.cmd_ret == 1) {

                            if (rule.type == 'manual')
                                self.quickActionsNum--;
                            else
                                self.automationRulesNum--;

                            // toastr.success((rule.type == 'manual' ? ERRORS.QUICK_ACTION_DELETED_SUCCESSFULLY : ERRORS.AUTOMATION_RULE_DELETED_SUCCESSFULLY), null, $rootScope.toastrOpts);
                            MessageService.showSuccessMessage("Automation", (rule.type == 'manual' ? ERRORS.QUICK_ACTION_DELETED_SUCCESSFULLY : ERRORS.AUTOMATION_RULE_DELETED_SUCCESSFULLY));
                            var idx = self.rules.indexOf(rule);
                            self.rules.splice(idx, 1);
                        }
                        else {
                            throw 'Error in deleting automation rule.';
                        }
                    })
                    ['catch'](function () {
                    //console.log("Error in automation delete", msg)
                    // toastr.error(ERRORS.ERROR_DELETING_AUTOMATION, null, $rootScope.toastrOpts);
                    MessageService.showRegisteredErrorMessage(ERRORS.ERROR_DELETING_AUTOMATION, "Automation");
                })
                    ['finally'](function () {
                    bui.stop();
                });
            }
        };

        this.delLocation = function (location) {
            var result = confirm("Do you want to delete this location?");
            if (result == true) {
                var self = this;
                // var bui = blockUI.instances.get('bui_' + location.id);
                // bui.start();
                $http['delete']($rootScope.urlBackend + '/locations', {data: {"id": location.id}})
                    .then(function (response) {
                        if (response.data.response == 'Location deleted') {
                            var idx = self.locationsList.indexOf(location);
                            self.locationsList.splice(idx, 1);
                            // toastr.success(ERRORS.LOCATION_DELETED_SUCCESSFULLY, null, $rootScope.toastrOpts);
                            MessageService.showSuccessMessage("Automation", ERRORS.LOCATION_DELETED_SUCCESSFULLY);
                        }
                        else {
                            throw 'Error in deleting location.';
                        }
                    })
                    ['catch'](function () {
                    // toastr.error(ERRORS.ERROR_DELETING_LOCATION, null, $rootScope.toastrOpts);
                    MessageService.showRegisteredErrorMessage(ERRORS.ERROR_DELETING_LOCATION, "Automation");
                })
                    ['finally'](function () {
                    // bui.stop();
                });
            }
        };

        this.selAutomation = new AutomationRule();

        this.addLocation = function (location) {
            var self = this;
            var deferred = $q.defer();
            //var bui = blockUI.instances.get('bui_' + id);
            // var bui = blockUI.instances.get('LocationsBlockUI');
            // bui.start();
            //bui.start();
            $http.put($rootScope.urlBackend + '/locations/add', {
                name: location.name,
                lat: location.lat,
                lng: location.lng,
                radius: location.radius
            })
                .then(function (response) {
                    if (response.data.response == 'Location added') {
                        MessageService.showSuccessMessage("Automation", ERRORS.LOCATION_ADDED_SUCCESS);
                        self.locationsList.push(response.data.location);
                        $rootScope.$broadcast('NewLocation', response.data.location.id);
                        deferred.resolve();
                    }
                    else {
                        throw 'Error in adding location.';
                    }
                })
                ['catch'](function () {
                MessageService.showRegisteredErrorMessage(ERRORS.ERROR_ADDING_LOCATION, "Automation");
                deferred.reject('Error in adding location.');
            })
                ['finally'](function () {
                // bui.stop();
            });
            return deferred.promise;
        };

        this.clear = function () {
            while (this.rules.length > 0) this.rules.pop();
            while (this.mobileDevicesList.length > 0) this.mobileDevicesList.pop();
            while (this.locationsList.length > 0) this.locationsList.pop();
            this.loaded = false;
            this.mobileDevicesLoaded = false;
            this.locationsLoaded = false;
            this.busy = false;
            this.quickActionsNum = 0;
        };

        this.reload = function () {
            this.clear();
            this.load();
        };
    })
    .factory('Billing', function ($rootScope, $q, $http, ERRORS) {

        var Billing = function (item, state_array, packg) {

            this.billingaddress = '';
            this.billingcity = '';
            this.billingstate = '';
            this.billingzip = '';
            this.expiration_date = '';
            this.cvc = '';
            this.card_no = '';
            this.billingpackage = '';
            if (item)
                this.update(item, state_array, packg);

        };

        Billing.prototype.getStateByCode = function (code, state) {
            var profile_state;
            for (var i = 0; i < state.length; i++) {
                if (state[i].code == code) {
                    profile_state = state[i];
                }
            }
            return profile_state;
        };

        Billing.prototype.update = function (item, state_array, packg) {
            if (item.line != undefined)
                this.billingaddress = item.line;
            if (item.city != undefined)
                this.billingcity = item.city;
            if (item.state != undefined) {
                this.billingstate = this.getStateByCode(item.state, state_array);
            }
            if (item.zip != undefined)
                this.billingzip = item.zip;
            if (item.date != undefined)
                this.expiration_date = item.date;
            if (item.cvc_no != undefined)
                this.cvc = item.cvc;
            if (item.card_number != undefined)
                this.card_no = item.card_number;
            if (packg != undefined)
                this.package = packg;

        };

        return Billing;
    })
    //.service('BillingService', function ($q, $http, $rootScope, User, ERRORS, Billing, RegisterService, MessageService) { // states ?
    //    this.invoices = [];
    //    this.update_package = '';
    //    this.busy = false;
    //    this.updatecase = false;
    //    var billObj = {};
    //    var cardinfoObj = '';
    //    this.bill = [];
    //    this.basic = [];
    //    this.starter = [];
    //    this.premium = [];
    //    this.standard = [];
    //    this.url_state = '';
    //
    //    this.showInvoices = function () {
    //        var deferred = $q.defer();
    //        $http.get($rootScope.urlBackend + "/billing/invoice")
    //            .then(function (response) {
    //                this.invoices.push(response.data);
    //                deferred.resolve('Invoices Successfully Loaded');
    //
    //            }.bind(this))
    //            ['catch'](function (data, status, headers, config) {
    //            // toastr.error(data.data.error, null, $rootScope.toastrOpts);
    //            MessageService.showRegisteredErrorMessage(data.data.error, "Invoice Error");
    //
    //        }.bind(this))
    //            ['finally'](function () {
    //        }.bind(this));
    //
    //        return deferred.promise;
    //
    //    };
    //
    //
    //    this.update_url_state = function () {
    //        //console.log(this.url_state);
    //        if (this.url_state == 'register') {
    //            User.update_step('4');
    //        }
    //    };
    //
    //
    //    this.checkPackage = function (pack, url_state) {
    //        this.bill = [];
    //        //this.url_state=url_state;
    //        this.update_package = pack;
    //        if (User.stripeaccount == '1' || pack == 'free') {
    //
    //            this.busy = true;
    //            // var bui = blockUI.instances.get('billing_div');
    //            // bui.start();
    //            $http.put($rootScope.urlBackend + "/billing/", {'plan': pack})
    //                .then(function (response) {
    //                    //console.log(response);
    //                    if (response.data) {
    //                        User.plan = response.data.plan;
    //                        this.update_url_state();
    //                        // toastr.success(ERRORS.PACKAGE_UPDATED_SUCCESSFULLY, null, $rootScope.toastrOpts);
    //                        console.log(ERRORS.PACKAGE_UPDATED_SUCCESSFULL);
    //                    }
    //                    // bui.stop();
    //                    this.busy = false;
    //                }.bind(this))
    //                ['catch'](function (data, status, headers, config) {
    //                // toastr.error(data.data.message, null, $rootScope.toastrOpts);
    //                MessageService.showRegisteredErrorMessage(data.data.message, "Billing Error");
    //            }.bind(this))
    //                ['finally'](function () {
    //                this.busy = false;
    //                // bui.stop();
    //            }.bind(this));
    //        } else {
    //            // billObj=new Billing(User.address,states.state,this.update_package);
    //            billObj = new Billing(User.address, User.address.state, this.update_package);
    //            //console.log(billObj);
    //            this.bill.push(billObj);
    //            // this.openCreditCardForm();
    //        }
    //    };
    //
    //
    //    this.getPackageDetail = function () {
    //        cardinfoObj = '';
    //        // var bui = blockUI.instances.get('card_popup');
    //        // bui.start();
    //        //blockUI.start();
    //        $http.get($rootScope.urlBackend + "/billing/card")
    //            .then(function (response) {
    //                //cardinfoObj=response.data;
    //                cardinfoObj = angular.copy(response.data);
    //            }.bind(this))
    //            ['catch'](function (data, status, headers, config) {
    //        }.bind(this))
    //            ['finally'](function () {
    //            // bui.stop();
    //        }.bind(this));
    //
    //
    //    };
    //
    //    // this.updatebilling_info = function ($scope) {
    //    //   this.updatecase=true;
    //    //   billObj=new Billing(user.address,states.state,this.update_package);
    //    //   while(this.bill.length>0) this.bill.pop();
    //    //   this.bill.push(billObj);
    //    //   this.openCreditCardForm();
    //    // }
    //
    //    // this.openCreditCardForm = function () {
    //    //       //$scope.card=card;
    //
    //    //       $rootScope.modals.openCreditCardModal = $modal.open({
    //    //           templateUrl: '../app/tpls/account/creditCardForm.html',
    //    //           controller: function($scope,billingService,states,user){
    //    //         $scope.billingService=billingService;
    //    //       $scope.user=user;
    //    //       console.log(billingService.url_state);
    //    //     if(billingService.url_state=='billing' &&  user.stripeaccount == '1'){
    //    //         billingService.getPackageDetail();
    //    //        }
    //
    //    //       var promise = states.load();
    //    //               promise.then(function () {
    //    //            $scope.states=states;
    //    //          });
    //    //     },
    //    //           resolve: {
    //    //               parameters: function () {
    //    //                   return{'card': 'dddd'};
    //    //               },
    //
    //
    //    //           }
    //    //       });
    //    // };
    //
    //
    //    // this.currentaddress=function(checkedvalue){
    //    //   console.log(checkedvalue);
    //    //   while(this.bill.length>0) this.bill.pop();
    //    //   if(!checkedvalue){
    //    //       billObj.billingaddress='';
    //    //       billObj.billingcity='';
    //    //       billObj.billingstate='';
    //    //       billObj.billingzip='';
    //    //       if(this.url_state=='register')
    //    //         {
    //    //          this.bill.push(billObj);
    //    //       }
    //    //         if(this.url_state=='billing')
    //    //         {
    //    //          billObj.billingaddress= cardinfoObj.address;
    //    //          billObj.update(cardinfoObj,states.state,this.update_package);
    //    //            this.bill.push(billObj);
    //    //          //this.bill.push(cardinfoObj);
    //    //          console.log('billing state');
    //
    //    //        }
    //    //     console.log(this.bill);
    //    //   }
    //    //   else{
    //    //     billObj.update(user.address,states.state,this.update_package);
    //    //     this.bill.push(billObj);
    //    //   }
    //    //   }
    //
    //    this.PackagesWithCard = function () {
    //        var api_url = '';
    //        this.busy = true;
    //        if (this.bill[0] == undefined) {
    //            return;
    //        }
    //        var packagedate = (this.bill[0].expiration) ? this.bill[0].expiration.split('-') : '';
    //        // var bui = blockUI.instances.get('card_popup');
    //        // bui.start();
    //
    //        if (User.stripeaccount == '1')
    //            api_url = $rootScope.urlBackend + "/billing/card";
    //        else
    //            api_url = $rootScope.urlBackend + "/billing";
    //        //console.log("Billing info=", this.bill[0]);
    //
    //        var b_state = this.bill[0].billingstate;
    //        if (b_state != undefined) {
    //            b_state = b_state.code;
    //        } else {
    //            b_state = '';
    //        }
    //
    //        $http.put(api_url,
    //            {
    //                'plan': this.bill[0].package,
    //                'card_no': this.bill[0].card_number,
    //                'cvc': this.bill[0].cvc,
    //                'expiration_month': packagedate[0],
    //                'expiration_year': packagedate[1],
    //                'address': this.bill[0].billingaddress,
    //                'city': this.bill[0].billingcity,
    //                'state': b_state,
    //                'zip': this.bill[0].billingzip,
    //                'country': 'United States'
    //            })
    //
    //            .then(function (response) {
    //                //console.log("billing=", response);
    //                if (response.data) {
    //                    //console.log(response);
    //                    this.update_url_state();
    //                    User.plan = this.bill[0].package;
    //                    User.stripeaccount = '1';
    //                    //billObj.update('',states.state,this.bill[0].package);
    //                    this.bill = [];
    //                    //this.bill.push(billObj);
    //                    // toastr.success(ERRORS.PACKAGE_UPDATED_SUCCESSFULLY, null, $rootScope.toastrOpts);
    //                    MessageService.showRegisteredSuccessMessage(ERRORS.PACKAGE_UPDATED_SUCCESSFULLY, "Billed Successfully");
    //
    //                    $state.go("subscription3");
    //                }
    //                this.busy = false;
    //                // $rootScope.modals.openCreditCardModal.close();
    //                // bui.stop();
    //            }.bind(this))
    //            ['catch'](function (data, status, headers, config) {
    //            // toastr.error(data.data.message, null, $rootScope.toastrOpts);
    //            MessageService.showRegisteredErrorMessage(data.data.message, "Billing Error");
    //        }.bind(this))
    //            ['finally'](function () {
    //            this.busy = false;
    //            // bui.stop();
    //        }.bind(this));
    //
    //    };
    //
    //
    //    this.clear = function () {
    //        while (this.invoices.length > 0) this.invoices.pop();
    //        while (this.bill.length > 0) this.bill.pop();
    //        cardinfoObj = '';
    //    };
    //
    //    this.getBillingContent = function () {
    //        //blockUI.start();
    //        var deferred = $q.defer();
    //        MessageService.showLoading("Loading...");
    //        $http.get($rootScope.urlBackend + "/plans")
    //            .then(function (response) {
    //                if (response.data) {
    //                    this.basic = response.data[0];
    //                    this.starter = response.data[3];
    //                    this.premium = response.data[1];
    //                    this.standard = response.data[2];
    //                    MessageService.hideLoading();
    //                    deferred.resolve();
    //                }
    //            }.bind(this))
    //            ['catch'](function (data, status, headers, config) {
    //
    //        }.bind(this))
    //            ['finally'](function () {
    //            deferred.reject();
    //            MessageService.hideLoading();
    //        }.bind(this));
    //        return deferred.promise;
    //    }
    //
    //
    //})

    .service('Device', function (User, MessageService, $q, $rootScope, $http, $ionicLoading, ERRORS) {

        var Device = function (item) {
            this.id = '';
            this.name = '';
            this.icon = '';
            this.area = '';
            this.zone = '';
            this.type = '';
            this.typeTag = '';
            this.fault = {battery: false, tampered: false, supervision: false, outOfOrder: false, control: false};
            this.status = '';
            this.statusEx = '';
            this.statusIcons = [];
            this.favorite = 0;
            this.error = '';
            this.controlUrl = '';
            this.toggleModel = false;
            this.hasToggle = false;
            this.hasSlider = false;
            this.hasRec = false;
            this.hasTemp = false;
            this.mac = '';
            this.alreadyPopulated = false;
            this.summaryString = '';
            this.actions = [];
            if (item) {
                this.update(item);
            }
            if (item) {
                this.deviceFault = this.hasFault();
            }
        };

        Device.prototype.hasFault = function () {
            return this.fault.supervision ||
                this.fault.outOfOrder;
        };

        Device.prototype.update = function (item) {
            this.id = item.id;
            this.name = item.name;
            this.icon = item.icon;
            this.area = item.area;
            this.zone = item.zone;
            this.type = item.type;
            this.typeTag = item.type_tag;
            this.status = item.status;
            this.actions = item.actions;
            //d.statusEx = items[i].;
            this.statusEx = item.statusEx;
            this.statusIcons = item.status_icons;
            this.favorite = item.sort_order;
            this.controlUrl = item.control_url;
            this.fault.battery = item.faults.low_battery == '1';
            this.fault.tampered = item.faults.tempered == '1';
            this.fault.supervision = item.faults.supervision == '1';
            this.fault.outOfOrder = item.faults.out_of_order == '1';
            this.fault.control = item.faults.no_response == '1';
            this.toggleModel = (this.status == 'LockClosed' || this.status == 'On');
            switch (this.typeTag) {
                case 'device_type.door_lock':
                    if (!this.hasFault())
                        this.hasToggle = true;
                    break;
                case 'device_type.power_switch_meter':
                    if (!this.hasFault())
                        this.hasToggle = true;
                    if (!this.alreadyPopulated)
                        this.alreadyPopulated = true;
                    break;
                case 'device_type.power_switch_sensor':
                    if (!this.hasFault())
                        this.hasToggle = true;
                    if (!this.alreadyPopulated)
                        this.alreadyPopulated = true;
                    break;
                case 'device_type.dimmer':
                    if (!this.hasFault()) {
                        this.hasSlider = true;
                    }
                    break;
                case 'device_type.thermostat':
                case 'device_type.room_sensor':
                case 'device_type.temperature_sensor':
                    if (!this.hasFault())
                        this.hasTemp = true;
                    break;
                case 'device_type.ipcam':
                    var s = this.id.substring(3);
                    this.mac = '';
                    for (var i = 0; i < s.length - 1; i += 2) {
                        if (this.mac != '')
                            this.mac += ':';
                        this.mac += s.charAt(i) + s.charAt(i + 1);
                    }
                    if (!this.hasFault())
                        this.hasRec = true;
                    break;
                case 'device_type.ir_camera':
                case 'device_type.ir_camcoder':
                    if (!this.hasFault())
                        this.hasRec = true;
                    break;
                default:
                    break;
            }
        };

        Device.prototype.edit = function (name, icon) {
        };

        //====================
        //Record IP cam

        Device.prototype.recIPCAM = function (cameraStream) {
            var deferred = $q.defer();
            console.log(cameraStream);
            if (cameraStream == 1) {
                MessageService.showLoading("Recording...");
            }
            $http.put($rootScope.urlBase + "/" + this.controlUrl)
                .then(function (response) {
                    MessageService.hideLoading();
                    MessageService.showErrorMessage('Success', 'Please wait... Capturing media');
                    //MessageService.showLoading("Recording...");
                    deferred.resolve();
                }, function (err) {
                    console.log(err);
                    MessageService.showErrorMessage('Error', err.data.message + '. Please try again after some time.');
                    MessageService.hideLoading();
                    deferred.reject();
                });
            return deferred.promise;
        };

        //========Dimmer Level=============
        Device.prototype.changeDimmerLevel = function (dimmerValue) {
            var newStatus = '';
            newStatus = dimmerValue;

            var self = this;

            $http.put($rootScope.urlBackend + this.controlUrl, {level: dimmerValue})
                .then(function (response) {
                    dimmerValue = newStatus;
                    self.statusEx = newStatus;
//        toastr.success('Device status changed.', $rootScope.toastrOpts);
                    // bui.stop;
                    //console.log("Device Status Changed");
                })
                ["catch"](function (msg) {
                // toastr.error(ERRORS.ERROR_CHANGING_STATUS, null, $rootScope.toastrOpts);
                console.log(ERRORS.ERROR_CHANGING_STATUS);

            })
                ["finally"](function () {
                // bui.stop();
            });
        };
        //Dimmer Level Code Block Ends here=============
        //===================
        Device.prototype.toggle = function () {
            console.log('update device', this.toggleModel);
            var newStatus = '';
            switch (this.type) {
                case 'Door Lock':
                    newStatus = (this.status == 'LockOpen') ? 'LockClosed' : 'LockOpen';
                    break;
                case 'Power Switch Meter':
                case 'Power Switch Sensor':
                case 'Dimmer':
                    newStatus = (this.status == 'On') ? 'Off' : 'On';
                    break;
                default:
                    return;
            }

            var self = this;
            // var bui = blockUI.instances.get('bui_' + this.id);
            // bui.start();

            $http.put($rootScope.urlBase + '/' + this.controlUrl, {status: this.toggleModel ? 1 : 0})
                .then(function (response) {
                    self.status = newStatus;
                })["catch"](function (msg) {
                self.toggleModel = !self.toggleModel;
                console.log(ERRORS.ERROR_CHANGING_STATUS);
            })["finally"](function () {
                // bui.stop();
            });
        };

        Device.prototype.toString = function () {
            return "[Device '" + this.type + "']";
        };

        return Device;
    })
    .service('GatewayService', function (User, $q, $rootScope, $http, $ionicLoading, MessageService, $interval) {

        this.id = '';
        this.online = '1';
        this.showBanner = '';
        this.initialized = '';
        this.version = {all: '', net: '', zigbee: '', zwave: '', rf: ''};
        this.area1 = '';
        this.area2 = '';
        this.error = '';
        this.temp = '';
        this.timezone = '';
        this.timeZoneDiff = '';
        this.timeFormat = 'h:mm a';
        this.homeExitTimer = 0;
        this.homeExitTimerBusy = true;
        this.showhomeExitTimer = false;
        this.homeExitTimerTemp = 0;
        this.awayExitTimer = 0;
        this.awayExitTimerBusy = true;
        this.showAwayExitTimer = false;
        this.awayExitTimerTemp = 0;
        this.initGatewayBusy = false;

        this.init = function () {
            var self = this;
            if (self.initGatewayBusy) return;
            $http.put($rootScope.urlBackend + "/panel/init").success(function (panelInitResponse) {
                self.initGatewayBusy = true;
                if (panelInitResponse.response == "Panel Initialized") {
                    automation.clear();
                    $rootScope.$broadcast('gatewayInitDone');
                    self.initialized = '1';
                    $rootScope.panelInitModal.close();
                    self.load();
                    eventsService.search("no", "next");
                    devicesService.reload();
                    automation.reloadAutomation();
                }
                else {
                    $rootScope.$broadcast('gatewayInitFailed');
                }
            })['catch'](function () {
                $rootScope.$broadcast('gatewayInitFailed');
            })['finally'](function () {
                self.initGatewayBusy = false;
            });
        };

        this.init_connect = function () {
            var deferred = $q.defer();
            $http.get($rootScope.urlBackend + "/panel").success(function (response) {
                if (response) {
                    var p = response;
                    this.id = p['report_account'];
                    this.online = p.online;
                    this.showBanner = 1;
                    this.initialized = p['initialized'];
                    this.version = {
                        all: p.version,
                        net: p['net_version'],
                        zigbee: p['zigbee_version'],
                        zwave: p['z_wave_version'],
                        rf: p['rf_version']
                    };
                    this.area1 = p.mode['area_1'];
                    this.area2 = p.mode['area_2'];
                    this.time = p.time;
                    this.timezone = p.timezone;
                    this.timeZoneDiff = p['tz_diff'];

                    $rootScope.$broadcast('gatewayLoadDone');

                    if (this.initialized == '0') {
                        this.init();
                    }

                    this.getHomeExitTimer();
                    deferred.resolve();
                }
            }.bind(this))['catch'](function (msg) {
            }.bind(this));
            return deferred.promise;
        };

        this.closeTimers = function () {
            if (this.homeTimerInterval) {
                $interval.cancel(this.homeTimerInterval);
                this.homeTimerInterval = undefined;
                this.homeExitTimer = this.homeExitTimerTemp;
                this.showhomeExitTimer = false;
            }

            if (this.awayTimerInterval) {
                $interval.cancel(this.awayTimerInterval);
                this.awayTimerInterval = undefined;
                this.awayExitTimer = this.awayExitTimer;
                this.showAwayExitTimer = false;
            }
        };

        this.changeMode = function (mode) {
            var deferred = $q.defer();
            var area = 1;
            var oldState = this.area1;
            this.area1 = mode;
            var self = this;

            // if (self.homeExitTimer > 0)
            // var exitTimer = self.homeExitTimer;

            // var panelModeChangeBlockUI = blockUI.instances.get('panelModeChangeBlockUI');
            // panelModeChangeBlockUI.start();
            self.closeTimers();
            $http.put($rootScope.urlBackend + "/panel/mode/" + area + "/" + mode).then(function (response) {
                if (response.data.area == 1) {
                    self.area1 = mode;
                    if (self.area1 == 'home' && self.homeExitTimer > 0) {
                        self.showhomeExitTimer = true;
                        self.showAwayExitTimer = false;
                        self.homeTimerInterval = $interval(function () {
                            self.homeExitTimer--;
                            if (self.homeExitTimer == 0) {
                                self.showhomeExitTimer = false;
                                self.homeExitTimer = self.homeExitTimerTemp;
                            }
                        }, 1000, self.homeExitTimer);
                    }
                    else {
                        if (self.showhomeExitTimer) {
                            self.homeExitTimer = self.homeExitTimerTemp;
                            self.showhomeExitTimer = false;
                        }
                    }

                    if (self.area1 == 'away' && self.awayExitTimer > 0) {
                        self.showAwayExitTimer = true;
                        self.showhomeExitTimer = false;
                        self.awayTimerInterval = $interval(function () {
                            self.awayExitTimer--;
                            if (self.awayExitTimer == 0) {
                                self.showAwayExitTimer = false;
                                self.awayExitTimer = self.awayExitTimerTemp;
                            }
                        }, 1000, self.awayExitTimer);
                    }
                    else {
                        if (self.showAwayExitTimer) {
                            self.awayExitTimer = self.awayExitTimerTemp;
                            self.showAwayExitTimer = false;
                        }

                    }

                } else {
                    self.area1 = oldState;
                }

                // panelModeChangeBlockUI.stop();
                // deferred.resolve(response.response.cmd_ack);
                deferred.resolve(response);

            }, function (msg) {
                //if (self.showhomeExitTimer == true)
                // panelModeChangeBlockUI.stop();
                console.log("change mode error=", msg);
                if (msg && msg.status == 610) {
                    // MessageService.showErrorMessage("Gateway Error", msg.data.message);
                }
                deferred.reject(msg.data);
            });

            return deferred.promise;
        };

        this.getWeather = function () {
            var self = this;

            $http.get($rootScope.urlBackend + "/weather")
                .then(function (response) {
                    if (response.data) {
                        self.temp = response.data.temperature + ' \u00B0 F';
                        $rootScope.$broadcast('com.goabode.my.weather:update', self.temp);
                    } else
                        throw 'Invalid response received from weather api' + response.data;
                })['catch'](function (msg) {
                console.log('Unable to get gateway weather', msg)
            })['finally'](function () {
            });
        };

        this.getHomeExitTimer = function () {
            var self = this;
            self.homeExitTimerBusy = true;
            self.awayExitTimerBusy = true;
            $http.get($rootScope.urlBackend + "/areas")
                .then(function (response) {
                    if (response.data.area_1) {
                        self.homeExitTimer = response.data.area_1.home_exit_delay;
                        self.awayExitTimer = response.data.area_1.away_exit_delay;

                        self.homeExitTimerTemp = angular.copy(self.homeExitTimer);
                        self.awayExitTimerTemp = angular.copy(self.awayExitTimer);
                        //this.homeExitTimerBusy
                    }
                    else
                        throw 'Failed to get Home Exit Timer';
                })['catch'](function (msg) {
                console.log('Unable to get Home Exit Timer')
            })['finally'](function () {
                self.homeExitTimerBusy = false;
                self.awayExitTimerBusy = false;
            });
        };


        this.clear = function () {
            this.id = '';
            this.online = '';
            this.showBanner = '';
            this.initialized = '';
            this.version = {all: '', net: '', zigbee: '', zwave: '', rf: ''};
            this.area1 = '';
            this.area2 = '';
            this.error = '';
            this.temp = '';
            this.timeZoneDiff = '';
            this.timeFormat = 'h:mm a';
        }

    })
    .service('EventsArray', function ($rootScope, $http) {

        this.loaded = false;
        this.isFilterApplied = false;

        this.deviceNamesByType = [
            {"id": "0", "Name": "Unknown"},
            {"id": "1", "Name": "Panel"},
            {"id": "2", "Name": "Remote Controller"},
            {"id": "3", "Name": "Night Switch"},
            {"id": "4", "Name": "Door Contact"},
            {"id": "5", "Name": "Water Sensor"},
            {"id": "7", "Name": "Fix Panic"},
            {"id": "9", "Name": "PIR"},
            {"id": "10", "Name": "EIR"},
            {"id": "11", "Name": "Smoke Detector"},
            {"id": "12", "Name": "Gas"},
            {"id": "13", "Name": "CO Detector"},
            {"id": "14", "Name": "Heater"},
            {"id": "15", "Name": "Keypad"},
            {"id": "16", "Name": "Tag Reader"},
            {"id": "17", "Name": "Keypad"},
            {"id": "18", "Name": "Keypad"},
            {"id": "19", "Name": "Glass Sensor"},
            {"id": "20", "Name": "Temperature Sensor"},
            {"id": "21", "Name": "WTR"},
            {"id": "22", "Name": "BX"},
            {"id": "23", "Name": "Siren"},
            {"id": "24", "Name": "Power switch sensor"},
            {"id": "25", "Name": "Power switch sensor"},
            {"id": "26", "Name": "Repeater"},
            {"id": "27", "Name": "IR Camera"},
            {"id": "28", "Name": "IR Camcoder"},
            {"id": "29", "Name": "Out View"},
            {"id": "30", "Name": "Dect"},
            {"id": "31", "Name": "Remote Controller"},
            {"id": "32", "Name": "PCT R"},
            {"id": "33", "Name": "Door Contact"},
            {"id": "34", "Name": "Fall Sensor"},
            {"id": "35", "Name": "NT"},
            {"id": "36", "Name": "UT"},
            {"id": "37", "Name": "keypad"},
            {"id": "38", "Name": "TG 9"},
            {"id": "39", "Name": "Glass"},
            {"id": "40", "Name": "Temperature Sensor"},
            {"id": "41", "Name": "TCH"},
            {"id": "42", "Name": "TSL"},
            {"id": "43", "Name": "WTG GPS"},
            {"id": "44", "Name": "718"},
            {"id": "45", "Name": "Siren"},
            {"id": "46", "Name": "BX"},
            {"id": "47", "Name": "HRRS"},
            {"id": "48", "Name": "Power Switch Meter"},
            {"id": "49", "Name": "WTRV"},
            {"id": "50", "Name": "Power Meter"},
            {"id": "51", "Name": "Thermostat"},
            {"id": "52", "Name": "UPIC"},
            {"id": "53", "Name": "Dimmer Meter"},
            {"id": "54", "Name": "Room Sensor"},
            {"id": "55", "Name": "Radon"},
            {"id": "56", "Name": "Thermostat"},
            {"id": "57", "Name": "Door Lock"},
            {"id": "58", "Name": "Heat Detector"},
            {"id": "59", "Name": "Falla"},
            {"id": "60", "Name": "Fallc"},
            {"id": "61", "Name": "Switch"},
            {"id": "62", "Name": "Heat Meter"},
            {"id": "63", "Name": "Water Meter"},
            {"id": "64", "Name": "Gas Meter"},
            {"id": "65", "Name": "808rv"},
            {"id": "66", "Name": "Dimmer"},
            {"id": "67", "Name": "SDX"},
            {"id": "69", "Name": "IP Cam"},
            {"id": "70", "Name": "Door Lock"},
            {"id": "71", "Name": "Thermostat"},
            {"id": "72", "Name": "Door Lock"},
            {"id": "73", "Name": "thermostat"}
        ];

        this.arrTypes = [];
        this.notifiableEventsList = [];
        this.loaded = false;
        this.busy = false;

        this.load = function () {
            var self = this;
            if (self.loaded || self.busy)
                return;

            self.busy = true;

            $http.post($rootScope.urlBase + '/api/rest/timeline/events')
                .then(function (response) {
                    console.log("timeline events=", response);
                    self.loaded = true;
                    for (var i = 0; i < response.data.response.length; i++) {
                        if (response.data.response[i]['is_active'] == 1)
                            self.arrTypes.push({
                                "id": response.data.response[i].code,
                                "Name": response.data.response[i].label
                            });
                    }
                    console.log("Arrtypes=", self.arrTypes);
                })
                ['catch'](function (msg) {
                //toastr.error('Error in loading Event types.', null, $rootScope.toastrOpts);
            })
                ['finally'](function () {
                self.busy = false;
            });
        };


        this.getEventById = function (id) {
            for (var i = 0; i < this.arrTypes.length; i++)
                if (this.arrTypes[i].id == id)
                    return this.arrTypes[i];
            console.log('Event Object Not found, ID:', id);
            return null;
        };

        this.getEventByName = function (name) {
            for (var i = 0; i < this.arrTypes.length; i++)
                if (this.arrTypes[i].Name == name)
                    return this.arrTypes[i];
            console.log("Event Object Not Found, name=", name);
            return null;
        }
    })
    .service('EventsService', function ($rootScope, $http, Event, $q, EventsArray, ERRORS, $stateParams, $state, MessageService, FilterForm) {
        var list = [];
        this.list = list;
        this.showActualCount = true;
        this.alertsList = [];
        this.loadGroupBusy = false;
        this.groupList = [];
        this.groupTimelineList = [];
        this.groupTimelineResponse = false;
        this.alarmInProgress = [];
        this.page = 0;
        this.alertEventsPage = 0;
        this.busy = false;
        this.alertEventsBusy = false;
        this.timelineEventsBusy = false;
        this.markReadBusy = false;
        this.markAllReadBusy = false;
        this.busys;
        this.selectedGroupId = '';
        this.alertLastSeen = '';
        this.prevbusy = false;
        this.loaded = false;
        this.alertEventsloaded = false;
        this.allPagesLoaded = false;
        this.alertEventsAllPagesLoaded = false;
        this.allPrevPagesLoaded = false;
        this.eventid = '';
        this.callInProgress = false;
        this.direction = '';
        this.groupEventsPage = 0;
        this.is_param = false;

        // new
        this.selectedAlarmIndex = 0;
        this.selectedAlarm = '';

        // this.search = function () {


        //     if (this.busy || this.prevbusy) return; // if a call to server is already in progress

        //     MessageService.showLoading("Loading...");
        //     $http.get($rootScope.urlBackend + "/timeline").
        //         then(function (response) {
        //             // set loader false
        //             this.clear();
        //             this.busy = false;
        //             this.prevbusy = false;
        //             console.log(response);
        //             if (response.data) {
        //               for (var i = 0; i < response.data.length; i++) {
        //                 this.list.push(new Event(response.data[i]));
        //               }

        //               this.loaded = true;
        //               console.log("Device list=", this.list);
        //             }
        //             else
        //                 throw "Events not found in response";


        //         }.bind(this))
        //         ['catch'](function (data, status, headers, config) {
        //         console.log("Error in loading timeline events");
        //     }.bind(this))
        //         ['finally'](function () {
        //         this.busy = false;
        //         this.busys = false;
        //         this.prevbusy = false;
        //         MessageService.hideLoading();

        //     }.bind(this));
        // };

        this.get_camera_events = function (cam) {
            var deferred = $q.defer();
            var eventLabel = '';
            if (cam.stream == '0') {
                // eventLabel = EventsArray.getEventByName("Image Capture").id;
                eventLabel = "Image Capture";
            } else if (cam.stream == '1') {
                // eventLabel = EventsArray.getEventByName("Video Capture").id;
                eventLabel = "Video Capture";
            }

            $http.get($rootScope.urlBackend + "/timeline", {
                params: {event_label: eventLabel, device_id: cam.id}
            }).
                then(function (response) {
                    //console.log("timeline =", response, "  eventLabel=", eventLabel, " deviceid=", cam.id);
                    if (response.data.length > 0) {
                        var event_list = [];
                        for (var i = 0; i < response.data.length; i++) {
                            event_list.push(new Event(response.data[i]));
                        }
                        deferred.resolve(event_list);
                    }
                    else
                        deferred.resolve(null);
                },

                function () {
                    deferred.reject();
                });

            return deferred.promise;
        };

        this.get_last_event = function (cam, cam_index) {

            var deferred = $q.defer();
            var eventLabel = '';
            if (cam.stream == '0') {
                // eventLabel = EventsArray.getEventByName("Image Capture").id;
                eventLabel = "Image Capture";
            } else if (cam.stream == '1') {
                // eventLabel = EventsArray.getEventByName("Video Capture").id;
                eventLabel = "Video Capture";
            }

            $http.get($rootScope.urlBackend + "/timeline", {
                params: {event_label: eventLabel, device_id: cam.id}
            }).
                then(function (response) {
                    //console.log("timeline =", response, "  eventLabel=", eventLabel, " deviceid=", cam.id);
                    if (response.data.length > 0) {
                        var resp_data = {};
                        resp_data.data = new Event(response.data[0]);
                        resp_data.cam_index = cam_index;
                        deferred.resolve(resp_data);
                    }
                    else
                        deferred.resolve(null);
                },

                function () {
                    deferred.reject();
                });

            return deferred.promise;
        };

        this.search = function (scrollbar, scrolltype) {
            var deferred = $q.defer();
            var self = this;

            if (!EventsArray.isFilterApplied && !$stateParams.id && !FilterForm.singleEvent && scrollbar == 'no' && this.loaded) {
                deferred.reject();
            }


            FilterForm.singleEvent = !!$stateParams.id;

            this.eventlabel = '';
            // var bui = blockUI.instances.get('eventsearchsection');
            // bui.start();


            if (this.busy || this.prevbusy) deferred.reject(); // if a call to server is already in progress

            if (scrollbar != 'yes') this.clear(); // on reset clear the this
            if ((this.allPagesLoaded && scrolltype == "next") || (this.allPrevPagesLoaded && scrolltype == "prev")) return; // check that all records are loaded then return

            //filter form code
            this.deviceid = FilterForm.device == null ? '' : (FilterForm.device == '' ? 'gateway' : FilterForm.device.id); // filter device id
            if (FilterForm.event != '' && FilterForm.event != undefined) { // filter events froM form
                if (scrolltype == 'prev') return;   //
                else {
                    this.eventid = '';
                    this.eventlabel = FilterForm.event == null ? '' : FilterForm.event.Name; // set event label
                }
            }
            //  event id as url parameter.check that id not empty  and then set the eventid as url parameter id and plus 1
            else if ($stateParams.id != '' && $stateParams.id != undefined) {
                this.eventid = $stateParams.id == null ? '' : (parseInt($stateParams.id) + 1).toString();
            }
            // for default case if there is no stateparam and filter event then set direction to next
            else {

                this.busy = true;
                this.prevbusy = false;
                this.eventid = '';
                this.direction = 'next';
            }
            // Now check that if scroll is next or previous  and scrollbar is allow or not
            if (scrolltype == 'next' && scrollbar == 'yes') {
                this.busy = true;  // set busy variable true
                this.prevbusy = false;  // busy loader for next events is false for this case
                this.eventid = this.list[this.list.length - 1] ? this.list[this.list.length - 1].id : ''; // if event list is already populated then set event id as last index of list otherwise empty event id
                this.direction = "next";
            }
            else if (scrolltype == 'prev' && scrollbar == 'yes') {

                this.busy = false;  // set busy variable false if we load previous events
                this.prevbusy = true; // busy loader for previous evenst is true for this case
                this.eventid = this.list[0] ? this.list[0].id : '';  // if event list is already populated then set event id as last index of list otherwise empty event id
                this.direction = "prev";
            }
            else {
                this.direction = "next";
                this.busy = true;
                this.prevbusy = false;
            }

            this.is_param = !!(FilterForm.isApplied() == false && !(isNaN($stateParams.id)) && $stateParams.id != '');

            // this.onAssociatesUpdate = function (users) {
            //     for (var i = 0; i < list.length; i++) {
            //         var e = list[i];
            //         e.whoDelInfo = e.getDelBy();
            //     }
            // };

            // $rootScope.$on('com.goabode.my.associates:update', this.onAssociatesUpdate);
            // MessageService.showLoading("Loading...");
            $http.get($rootScope.urlBackend + "/timeline", {
                params: {
                    last_seen: this.eventid,
                    from: FilterForm.dp1,
                    to: FilterForm.dp2,
                    event_label: this.eventlabel,
                    device_id: this.deviceid,
                    dir: this.direction
                }
            })
                .then(function (response) {
                    $http.get($rootScope.urlBackend + "/associates").then(function (data) {
                        self.users = data;
                        if (response.data) {
                            if (self.direction == 'next') {
                                self.allPrevPagesLoaded = false;
                                self.allPagesLoaded = response.data.length < 20;
                                for (var i = 0; i < response.data.length; i++) {
                                    self.list.push(new Event(response.data[i]));
                                }
                            }
                            else if (self.direction == 'prev' && self.is_param == true) {
                                //this.allPagesLoaded = false;

                                self.allPrevPagesLoaded = response.data.length < 20;
                                if (self.allPrevPagesLoaded)  self.is_param = false;
                                self.allPagesLoaded = false;
                                for (var i = 0; i < response.data.length; i++) {
                                    self.list.unshift(new Event(response.data[i]));
                                }
                            }
                            self.loaded = true;
                            self.busy = false;
                            self.busys = false;
                            self.prevbusy = false;
                            deferred.resolve();
                        }
                        else {
                            self.busy = false;
                            self.busys = false;
                            self.prevbusy = false;
                            deferred.reject();
                        }
                    });
                    // set loader false
                    self.busy = false;
                    self.prevbusy = false;
                    //console.log(response);

                    // throw "Events not found in response";

                }, function (err) {
                    self.busy = false;
                    self.busys = false;
                    self.prevbusy = false;
                    deferred.reject();
                });

            return deferred.promise;
            //     }.bind(this))
            //     ['catch'](function (data, status, headers, config) {
            //     console.log("Error in loading timeline events");
            // }.bind(this))
            //     ['finally'](function () {

            //     // MessageService.hideLoading();
            //     // bui.stop();
            // }.bind(this));
        };

        this.alarmVerification = function () {
            var self = this;
            if (this.selectedAlarmIndex < 0) return;
            MessageService.showLoading("Verifying...");
            $http.post($rootScope.urlBackend + "/timeline/" + this.alertsList[this.selectedAlarmIndex].id + "/verify_alarm").
                then(function (response) {
                    MessageService.hideLoading();
                    if (response.data) {
                        $rootScope.$broadcast('alarmClosed');
                    }
                })
                ['catch'](function (data, status, headers, config) {
                console.log(data);
            })
                ['finally'](function () {
                MessageService.hideLoading();
            });


        };

        this.alarmIgnore = function () {
            var self = this;
            if (this.selectedAlarmIndex < 0) return;
            MessageService.showLoading("Updating...");
            $http.post($rootScope.urlBackend + "/timeline/" + this.alertsList[this.selectedAlarmIndex].id + "/ignore_alarm").
                then(function (response) {
                    MessageService.hideLoading();
                    if (response.data) {
                        $rootScope.$broadcast('alarmClosed');
                    }
                })
                ['catch'](function (data, status, headers, config) {
                console.log(data);
            })
                ['finally'](function () {
                MessageService.hideLoading();
            });
        };

        this.loadGroupAlarm = function (scrollbar) {
            var self = this;
            if (self.loadGroupBusy) return;
            if (scrollbar != 'yes')
                self.clearAlertGroups();
            self.loadGroupBusy = true;
            var deferred = $q.defer();

            $http.get($rootScope.urlBackend + "/timeline/group_alarm?page=" + self.groupEventsPage).
                then(function (response) {
                    if (response.data) {
                        for (var i = 0; i < response.data.length; i++) {
                            self.groupList.push(response.data[i]);
                        }
                        // self.groupEventsPage++;
                    }
                    else
                        throw "Alert events not found in response";
                    deferred.resolve();
                })
                ['catch'](function (data, status, headers, config) {
                console.log("Error in loading alert events");
                deferred.reject();
            })
                ['finally'](function () {
                self.loadGroupBusy = false;
                // bui.stop();

            });
            return deferred.promise;
        };
        this.loadGroupTimeline = function (event_id, verified_tid) {
            var self = this;
            // var bui = blockUI.instances.get('alertGroupBlock');
            // bui.start();

            //console.log("Timeline params=", event_id, ", ", verified_tid);
            if (self.timelineEventsBusy) return;
            this.selectedGroupId = verified_tid;
            while (this.groupTimelineList.length > 0) this.groupTimelineList.pop();
            var deferred = $q.defer();
            self.timelineEventsBusy = true;
            $http.get($rootScope.urlBackend + "/timeline/group_timeline?id=" + event_id + "&verified_tid=" + verified_tid).
                then(function (response) {
                    if (response.data) {
                        //console.log("GroupTimeline=", response.data);
                        if (response.data.length > 0) {
                            for (var i = 0; i < response.data.length; i++) {
                                self.groupTimelineList.push(new Event(response.data[i]));
                            }
                            self.groupTimelineResponse = true;
                        }
                        else (response.data.length == 0)
                        self.groupTimelineResponse = true;
                    }
                    deferred.resolve();
                })
                ['catch'](function (data, status, headers, config) {
                console.log("Error in loading alert events");
                deferred.reject();
            })
                ['finally'](function () {
                self.timelineEventsBusy = false;
                // bui.stop();
            });
            return deferred.promise;
        };

        this.loadAlertEvents = function (scrollbar) {
            var self = this;
            var event_exist = true;
            if (self.alertEventsBusy) return;

            if (scrollbar != 'yes')
                self.clearAlerts();

            if (self.alertEventsAllPagesLoaded) return;

            // var bui = blockUI.instances.get('alertEventsBlockUI');
            // bui.start();
            self.alertEventsBusy = true;
            self.alertLastSeen = self.alertsList[self.alertsList.length - 1] ? self.alertsList[self.alertsList.length - 1].id : '';


            // page: self.alertEventsPage, status: 0, severity: 0, size: 20
            MessageService.showLoading("Loading...");
            $http.get($rootScope.urlBackend + "/timeline/alarm", {
                params: {
                    is_viewed: 0,
                    dir: "next",
                    last_seen: self.alertLastSeen
                }
            }).
                then(function (response) {
                    MessageService.hideLoading();
                    if (response.data) {

                        for (var i = 0; i < response.data.length; i++) {
                            self.alertsList.push(new Event(response.data[i]));

                            if (self.alertsList[i].verifiedBy == '0') {
                                self.alarmInProgress.unshift(self.alertsList[i]);
                                $rootScope.$broadcast('alarmOccured', self.alertsList[i]);
                            }
                        }
                        self.showActualCount = (self.alertEventsPage == 0 && self.alertsList.length < 20);
                        self.alertEventsPage++;
                        if (response.data.length < 20) {
                            self.alertEventsAllPagesLoaded = true;
                            self.showActualCount = true;
                        }
                        self.alertEventsloaded = true;
                    }
                    else
                        throw "Alert events not found in response";

                })
                ['catch'](function (data, status, headers, config) {
                console.log("Error in loading alert events");
                MessageService.hideLoading();
            })
                ['finally'](function () {
                self.alertEventsBusy = false;
                MessageService.hideLoading();
                // bui.stop();
            });
        };


        this.markAlertEventAsRead = function (event) {
            var self = this;
            if (self.markReadBusy) return;

            // var bui = blockUI.instances.get('alertAllEventsBlockUI');
            // var icon_bui = blockUI.instances.get('alertEventsBlockUI');
            // bui.start();
            // icon_bui.start();

            self.markReadBusy = true;

            $http.put($rootScope.urlBackend + "/timeline/" + event.id + "/mark_read").
                then(function (response) {
                    if (response.data) {
                        for (var i = 0; i < self.alertsList.length; i++) {
                            if (self.alertsList[i] == event)
                                self.alertsList.splice(i, 1);
                        }
                        // toastr.success(response.data.message, null, $rootScope.toastrOpts);
                    }
                    else
                        throw "Alert events not found in response";
                })
                ['catch'](function (data, status, headers, config) {
                // toastr.error(ERRORS.ERROR_MARK_AS_READ, null, $rootScope.toastrOpts);
                console.log("Error in marking events as read");
            })
                ['finally'](function () {
                self.markReadBusy = false;
                // bui.stop();
                // icon_bui.stop();
            });
        };


        this.markAllAlertEventsAsRead = function () {
            var self = this;
            if (self.markAllReadBusy) return;

            // var bui = blockUI.instances.get('alertAllEventsBlockUI');
            // var icon_bui = blockUI.instances.get('alertEventsBlockUI');
            // bui.start();
            // icon_bui.start();

            self.markAllReadBusy = true;
            $http.put($rootScope.urlBackend + "/timeline/mark_all_read").
                then(function (response) {

                    if (response.data) {
                        while (0 < self.alertsList.length)
                            self.alertsList.pop();
                        self.showActualCount = true;
                    }
                    else
                        throw "Alert events not found in response";

                })
                ['catch'](function (data, status, headers, config) {
                // toastr.error(ERRORS.ERROR_MARK_ALL_AS_READ, null, $rootScope.toastrOpts);
                console.log("Error in marking events as read");
            })
                ['finally'](function () {
                self.markAllReadBusy = false;
                // bui.stop();
                // icon_bui.stop();
            });
        };


        this.clear = function () {
            while (this.list.length > 0) this.list.pop();
            this.page = 0;
            this.loaded = false;
            this.allPagesLoaded = false;
            this.showActualCount = true;
        };
        this.clearAlertGroups = function () {

            while (this.groupList.length > 0) this.groupList.pop();
            this.groupEventsPage = 0;
        };
        this.clearAlerts = function () {
            while (this.groupTimelineList.length > 0) this.groupTimelineList.pop();

            while (this.alertsList.length > 0) this.alertsList.pop();
            while (this.alarmInProgress.length > 0) this.alarmInProgress.pop();
            this.alertEventsPage = 0;
            this.alertEventsAllPagesLoaded = 0;
        };

        // $rootScope.$on('filterFormChange', function (event, scroll) {
        //     this.search(scroll);
        // }.bind(this));
    })
    .factory('Event', function ($http, $sce, User, EventsArray, $rootScope, ERRORS, DevicesService) {

        var Event = function (item) {
            this.id = '';
            this.deviceName = '';
            this.deviceId = '';
            this.deviceTypeId = '';
            this.device = '';
            this.cid = '';
            this.time = '';
            this.date = '';
            this.area = '';
            this.type = '';
            this.typeId = '';
            this.code = '';
            this.level = '';
            this.severity = '';
            this.color = '';
            this.position = '';
            this.files = [];
            this.thumbnail = [];
            this.isVideo = false;
            this.isDel = false;
            this.verifiedBy = '';
            this.viewedBy = '';

            this.whoDel = 0;
            this.whoDelInfo = '';
            //this.status = '';
            this.apptype = '';
            this.userid = '';

            // New
            this.date_time = '';
            this.image_path = '';
            this.deviceIcon = '';
            if (item)
                this.update(item);
        };


        Event.prototype.getDeviceName = function (type) {
            for (var i = 0; EventsArray.deviceNamesByType.length > i; i++) {
                if (type == EventsArray.deviceNamesByType[i].id)
                    return EventsArray.deviceNamesByType[i].Name;
            }

        };
        Event.prototype.getUserName = function (type) {
            // console.log(associates.list);
            // for (var i = 0; i < associates.list.length; i++) {
            //     if (type == associates.list[i].id)
            //         return  'by ' + associates.list[i].name;
            // }
            return 'by Username';
        };

        Event.prototype.getDelBy = function () {
            // Image Deleted by
            // if (!this.isDel) return '';
            var str = this.isVideo ? 'Video Deleted' : 'Image Deleted';
            // for (var i = 0; associates.list.length > i; i++) {
            //     var user = associates.list[i];
            //     if (this.whoDel == user.id) {
            //         str += ' by ' + user.name;
            //         break;
            //     }
            // }
            return str;
        };

        Event.prototype.init_icon = function () {
            for (var i = 0; i < DevicesService.list.length; i++) {
                if (DevicesService.list[i].id == this.deviceId) {
                    this.deviceIcon = $rootScope.urlBase + "/" + DevicesService.list[i].icon;
                    return;
                }
            }

            this.deviceIcon = $rootScope.urlBase + "/" + "assets/images/svg/unknown.svg";

            //console.log("DeviceIcon=", this.deviceIcon, "DeviceiD=", this.deviceId);
        };

        Event.prototype.update = function (item) {
            this.id = item.id;
            this.is_active = item.is_active;
            this.deviceId = item.device_id;
            this.deviceType = item.device_type_id;
            this.deviceName = item.device_name;
            this.cid = item.event_cid;
            this.code = item.event_code;
            this.time = item.time;
            this.date = item.date;
            this.type = item.event_type;
            this.typeId = item.event_code;
            this.level = item.level;
            this.severity = item.severity;
            this.color = item.color;
            this.icon = item.icon;
            this.file_count = item.file_count;
            this.device_type_id = item.device_type_id;
            this.file_path = item.file_path;
            this.device_event = item.device_name + ' ' + item.event_type;
            this.position = item.pos;
            this.isDel = item.file_is_del >= 1;
            this.whoDel = parseInt(item.file_is_del);
            this.verifiedBy = item.verified_by_tid;
            this.viewedBy = item.viewed_by_uid;
            this.apptype = item.app_type;
            this.userid = item.user_id;
            this.userName = '';
            this.files = [];
            //this.status = item.status;


            if (this.deviceName == '' && (this.code != '4001' || this.code != '4000')) {
                this.deviceName = this.getDeviceName(this.deviceType) + " (Removed)"
                this.device_event = this.getDeviceName(this.deviceType) + ' ' + this.type + " (Removed)";
            }
            if (this.code == '4001' || this.code == '4000') {
                //console.log(this.userid);
                this.deviceName = this.getUserName(this.userid);
                this.device_event = this.type;
            }

            if (item.file_path && item.file_path.length > 5) {
                var ext = item.file_path.substring(item.file_path.length - 4);
                this.isVideo = (ext == '.mp4');
                if (item.file_count > 1) {
                    var base = item.file_path.substring(0, item.file_path.length - 5);
                    for (var i = 0; i < item.file_count; i++)
                        if (this.isVideo)
                            this.files.push({
                                videoPath: $sce.trustAsResourceUrl($rootScope.urlBase + '/' + base + (i + 1)) + ext,
                                thumbnailPath: ''
                            });
                        else
                            this.files.push({
                                videoPath: $rootScope.urlBase + '/' + base + (i + 1) + ext,
                                thumbnailPath: ''
                            });
                }
                else if (this.isVideo) {
                    var str = String($sce.trustAsResourceUrl(item.file_path));
                    var ImagePath = str.replace(".mp4", ".jpg");
                    ImagePath = ImagePath.replace("videos", "images");
                    this.files.push({
                        videoPath: $sce.trustAsResourceUrl($rootScope.urlBase + '/' + item.file_path),
                        thumbnailPath: $rootScope.urlBase + '/' + ImagePath
                    });
                }
                else {
                    this.files.push({videoPath: $rootScope.urlBase + '/' + item.file_path, thumbnailPath: ''});
                }
                //console.log("id =", this.id, "files=", this.files);
            }

            // date and time revision
            var l_date, l_date_now;
            l_date = new Date(Date.parse(this.date));
            l_date_now = new Date();
            if (l_date.getYear() == l_date_now.getYear() &&
                l_date.getMonth() == l_date_now.getMonth()) {
                if (l_date.getDate() == l_date_now.getDate()) {
                    this.date_time = "Today @ " + this.time;
                } else if (l_date.getDate() == l_date_now.getDate() - 1) {
                    this.date_time = "Yesterday @ " + this.time;
                } else {
                    this.date_time = this.date + " @ " + this.time;
                }

            } else {
                this.date_time = this.date + " @ " + this.time;
            }

            // image file path revision
            if (item.file_path != '') {
                this.image_path = $rootScope.urlBase + '/' + item.file_path;
            }


            // init icon image
            this.init_icon();


            this.whoDelInfo = this.getDelBy();
        };

        Event.prototype.captureDelete = function () {
            $http['delete']($rootScope.urlBackend + "/timeline", {data: {"id": this.id}})
                .then(function (response) {
                    if (response.data.response) {
                        this.isDel = true;
                        var str = this.isVideo ? 'Video Deleted' : 'Image Deleted';
                        this.whoDelInfo = str + " by " + user.name.first + " " + user.name.last;
                        // toastr.success(ERRORS.CAPTURE_FILE_DELETED, null, $rootScope.toastrOpts);
                        console.log("Event Capture Delete Success= ", ERRORS.CAPTURE_FILE_DELETED);
                    }

                }.bind(this))
                ['catch'](function (data, status, headers, config) {
                // toastr.error(data.data.error, null, $rootScope.toastrOpts);
                console.log("Event Capture Delete Error= ", data.data.error);
                // TODO: Handle Error;
                this.busy = false;
            }
                .bind(this))['finally'](function () {

            });

        };

        Event.prototype.toString = function () {
            return "[Event '" + this.type + "']";
        };

        return Event;
    })
    .service('FilterForm', function ($rootScope, $http, Device, Event, EventsArray) {
        this.dp1 = '';
        this.dp2 = '';
        this.device = null;
        this.event = null;
        this.singleEvent = false;

        $rootScope.del = function (devi) {
            devi.is_del = 1;

            Event(devi);
        };

        this.submit = function () {
            var isApplied = this.isApplied();
            EventsArray.isFilterApplied = !!isApplied;
            $rootScope.$broadcast('filterFormChange', 'no');
        };

        this.scrolled = function () {
            $rootScope.$broadcast('filterFormChange', 'yes');
        };

        this.reset = function () {
            this.clear();
            $rootScope.$broadcast('filterFormChange', 'no');
        };


        this.isApplied = function () {
            return !(this.dp1 == '' && this.dp2 == '' && this.device == null && this.event == null);
        };

        this.clear = function () {
            this.dp1 = '';
            this.dp2 = '';
            this.device = null;
            this.event = null;
        };
    })
    .service('User', function ($q, $http, $rootScope, localStorageService) {
        this.id = '';
        this.email = '';
        this.name = {first: '', last: ''};
        this.pic = 'https://zigron.goabode.com/assets/avatar/default.jpg';
        this.phone = '';
        this.address = {line: '', city: '', state: '', zip: ''};
        this.loc = {lat: '', lon: ''};
        this.timezone = '';
        this.gateway = ''; // Gateway Code
        this.verified = ''; // email has been verified or not
        this.owner = '';
        this.plan = '';
        this.stripeaccount = '';
        this.emergency_contacts = [];
        this.busy = false;
        this.loaded = false;
        this.step = '';

        this.update = function (u) {
            //console.log(u);
            if (u.id != undefined) {
                //console.log('user not empty');
                this.id = u.id;
            }
            if (u.email != undefined) {
                this.email = u.email;
            }
            if (u.owner != undefined) {
                this.owner = u.owner;
            }
            if (u['first_name'] != undefined) {
                this.name.first = u['first_name'];
            }
            if (u['last_name'] != undefined) {
                this.name.last = u['last_name'];
            }
            if (u.phone != undefined) {
                this.phone = u.phone;
            }
            if (u['profile_pic'] != undefined) {
                this.pic = u['profile_pic'];
            }
            if (u.address != undefined) {
                this.address.line = u.address;
            }
            if (u.city != undefined) {
                this.address.city = u.city;
            }
            if (u.state != undefined) {
                this.address.state = u.state;
            }
            if (u.zip != undefined) {
                this.address.zip = u.zip;
            }
            if (u.latitude != undefined) {
                this.loc.lat = u.latitude;
            }
            if (u.longitude != undefined) {
                this.loc.lon = u.longitude;
            }
            if (u.timezone != undefined) {
                this.timezone = u.timezone;
            }
            if (u['gateway_code'] != undefined) {
                this.gateway = u['gateway_code'];
            }
            if (u.verified != undefined) {
                this.verified = u.verified;
            }
            if (u.plan != undefined) {
                this.plan = u.plan;
            }
            if (u.step != undefined) {
                this.step = u.step;
            }

            if (u['stripe_account'] != undefined) {
                this.stripeaccount = u['stripe_account'];
            }

            this.loaded = true;
            // $rootScope.$broadcast('com.goabode.my.user:update', this);

            localStorageService.set("CurrentUser", u);
        };

        this.load = function () {
            if (this.loaded) return;
            this.busy = true;
            var self = this;
            var deferred = $q.defer();
            $http.get($rootScope.urlBackend + "/user").success(function (response) {
                //console.log("response", response);
                if (response.user) {

                    self.update(response.user);
                    deferred.resolve('Status is successfully changed.');
                }
                self.busy = false;
            }, function (err) {
                self.busy = false;
                deferred.reject();
            });

            return deferred.promise;
        };

        this.contacts = function () {
            //if(this.loaded) return;
            this.busy = true;
            this.emergency_contacts = [];
            var deferred = $q.defer();
            $http.get($rootScope.urlBackend + "/contacts").success(function (response) {
                //console.log(response.contacts);
                if (response.contacts) {
                    for (var i = 0; i < response.contacts.length; i++) {
                        this.emergency_contacts.push(response.contacts[i]);
                    }
                    //console.log(this.emergency_contacts);
                    deferred.resolve('Emergency Contacts loaded');

                }
                //  this.loaded = true;
                this.busy = false;
            }.bind(this)).
                error(function (data, status, headers, config) {
                    // TODO: Handle Error;
                    this.busy = false;
                }.bind(this));

            return deferred.promise;
        };

        this.clear = function () {
            this.id = '';
            this.email = '';
            this.name = {first: '', last: ''};
            this.pic = 'images/a0.png';
            this.phone = '';
            this.address = {line: '', city: '', state: '', zip: ''};
            this.loc.lat = '';
            this.loc.lon = '';
            this.timezone = '';
            this.gateway = ''; // Gateway Code
            this.verified = ''; // email has been verified or not
            this.owner = '';
            this.plan = '';
            this.stripeaccount = '';
            this.loaded = false;
        };


        this.cancel = function () {
            //   $modalInstance.dismiss('cancel');
        };
    })
    .factory('$localstorage', ['$window', function ($window) {
        return {
            set: function (key, value) {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function (key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key) {
                return JSON.parse($window.localStorage[key] || '{}');
            }
        }
    }]);
