'use strict';

angular.module('app.application')
    .directive('quickActionBarLeft', quickActionBarLeft)
    .directive('quickActionBarRight', quickActionBarRight);

quickActionBarLeft.$inject = ['SetupService', 'PanelService', 'localStorageService', '$ionicPopup', '$interval', '$timeout', '$rootScope'];
function quickActionBarLeft(SetupService, PanelService, localStorageService, $ionicPopup, $interval, $timeout, $rootScope) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: 'components/tabs/tabs.headerLeft.template.html',
        controller: [
            '$scope',
            '$ionicPopover',
            function ($scope, $ionicPopover) {
                $rootScope.vm = {
                    timer: 0,
                    interval: null
                };
                $scope.vm = {
                    newMode: null
                };
                $scope.functions = {};

                $scope.changeModePopup = function ($event) {
                    if ($rootScope.gateway.online === '1') {
                        $scope.popover.show($event);
                        var result = document.getElementsByClassName('view-container');
                        var wrappedResult = angular.element(result);
                        wrappedResult.addClass('blur-filter');
                    }
                };

                $scope.popover = $ionicPopover.fromTemplateUrl('components/_global/templates/popups/popover-standby.html', {
                    scope: $scope
                }).then(function (popover) {
                    $scope.popover = popover;
                });

                $scope.$on('popover.hidden', function () {
                    var result = document.getElementsByClassName('view-container');
                    angular.element(result).removeClass('blur-filter');
                });
                $rootScope.$watch('vm.timer', function () {
                    if ($rootScope.vm.timer && !$rootScope.vm.interval) {
                        $rootScope.vm.interval = $interval(function () {
                            if ($rootScope.vm.timer !== 0) {
                                $rootScope.vm.timer--;
                            } else {
                                $interval.cancel($rootScope.vm.interval);
                                $rootScope.vm.interval = null;
                            }
                        }, 1000);
                    }
                });

                $scope.functions.setMode = function (mode) {
                    if ($rootScope.gateway.mode.area_1 !== mode) { //jshint ignore:line
                        $scope.vm.newMode = mode;
                        PanelService.panelAreasGET().then(function (areas) {
                            PanelService.panelModePUT(mode).then(function (data) {
                                console.log('setmode', data.error);
                                if (data.error) {
                                    $scope.vm.alertView = $ionicPopup.show({
                                        templateUrl: 'components/_global/templates/popups/popup_gateway_error.html',
                                        scope: $scope,
                                        title: data.error,
                                        buttons: [{
                                            text: 'Cancel',
                                            type: 'abode-button',
                                            onTap: function () {
                                                $scope.vm.newMode = null;
                                                $scope.popover.hide();
                                            }
                                        }, {
                                            text: 'Arm Anyways',
                                            type: 'abode-button',
                                            onTap: function () {
                                                PanelService.panelModePUT(mode).then(function () {
                                                    PanelService.panelModePUT(mode).then(function (data) {
                                                        $rootScope.gateway.mode['area_' + data.area] = data.mode;
                                                        localStorageService.set('gateway', $rootScope.gateway);
                                                        $scope.vm.newMode = null;
                                                        if (data.mode && areas['area_' + data.area][data.mode + '_entry_delay']) {
                                                            $rootScope.vm.timer = areas['area_' + data.area][data.mode + '_entry_delay'];
                                                        } else {
                                                            $rootScope.vm.timer = 0;
                                                        }
                                                        $scope.popover.hide();
                                                    });
                                                });
                                            }
                                        }]
                                    });
                                } else {
                                    $rootScope.gateway.mode['area_' + data.area] = data.mode;
                                    localStorageService.set('gateway', $rootScope.gateway);
                                    $scope.vm.newMode = null;
                                    if (data.mode && areas['area_' + data.area][data.mode + '_entry_delay']) {
                                        $rootScope.vm.timer = areas['area_' + data.area][data.mode + '_entry_delay'];
                                    } else {
                                        $rootScope.vm.timer = 0;
                                    }
                                    $scope.popover.hide();
                                }
                            });
                        });
                    }
                };
            }
        ],
        link: function () {
            $rootScope.gateway = localStorageService.get('gateway') || {};
            SetupService.gatewayGET().then(function (data) {
                $rootScope.gateway = data;
                localStorageService.set('gateway', data);
            });
        }
    };
}

quickActionBarRight.$inject = [];
function quickActionBarRight() {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: 'components/tabs/tabs.headerRight.template.html'
    };
}