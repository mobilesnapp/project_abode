'use strict';

angular.module('app.signin', [])
    .config(signInConfig);

signInConfig.$inject = ['$stateProvider'];
function signInConfig($stateProvider) {
    $stateProvider
        .state('signin', {
            url: '/signin',
            templateUrl: 'components/signin/signin.template.html',
            controller: 'SigninController as vm'
        })
        .state('signinVerify', {
            url: '/signin/:id',
            templateUrl: 'components/signin/signin.template.html',
            controller: 'SigninController as vm'
        })
        .state('verify', {
            url: '/verify/:code',
            controller: 'VerifyController as vm'
        });
}