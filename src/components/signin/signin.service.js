'use strict';

angular.module('app.signin')
    .factory('SigninService', SigninService);

SigninService.$inject = ['Restangular', 'MessageService', 'PanelService', 'md5', 'localStorageService', '$state', '$http', 'apiVersion', 'Sockets'];
function SigninService(Restangular, MessageService, PanelService, md5, localStorageService, $state, $http, apiVersion, Sockets) {
    return {
        login: function (user, scope) {
            var me = this;
            var form = {
                id: user.email,
                password: user.password//md5.createHash(user.password)
            };

            //jshint ignore:start
            if (window.cordova) {
                //device.platform
                if (cordova.platformId === 'android') {
                    form.gcm_token = localStorageService.get('pushToken') || 'null';
                }
                else if (cordova.platformId === 'ios') {
                    form.apns_token = localStorageService.get('pushToken') || 'cb7235d5f91cb09f99e0328ee952fe76806af9f6d334a1a65f16d329c56785ce';
                }
                form.device_model = device.model;
                form.device_name = device.name || device.model;
            } else {
                form.apns_token = 'cb7235d5f91cb09f99e0328ee952fe76806af9f6d334a1a65f16d329c56785ce';
                form.device_model = 'Virtual';
                form.device_name = 'Virtual';
            }
            //jshint ignore:end

            MessageService.showLoading('Logging In…');
            return Restangular.all('auth2/login').post(form).then(function (data) {
                Sockets.connect();
                MessageService.hideLoading();
                $http.defaults.headers.common['ABODE-API-KEY'] = data.token;
                me.location();
                localStorageService.set('token', {
                    value: data.token,
                    expired_at: data.expired_at //jshint ignore:line
                });
                localStorageService.set('user', data.user);

                PanelService.panelGET().then(function (panel) {
                    localStorageService.set('panel', panel);

                    switch (data.user.step) {
                        case '1':
                            $state.go('setupProfile');
                            break;
                        case '2':
                            $state.go('setupChoose');
                            break;
                        default:
                            if (data.initiate_screen === 'quick_action') { //jshint ignore:line
                                $state.go('tab.quickaction');
                            } else if (data.initiate_screen === 'setup_zone') { //jshint ignore:line
                                if (data.panel.setup_zone_1 === '1' || data.panel.setup_zone_2 === '1' || data.panel.setup_zone_3 === '1') {  //jshint ignore:line
                                    $state.go('setupDevices');
                                } else {
                                    $state.go('setupGateway5');
                                }

                            } else {
                                $state.go('tab.status.timeline');
                            }
                            break;
                    }
                });
            }, function (error) {
                MessageService.hideLoading();
                if (error.data.message === 'You need to verify your email address before you can continue.') {
                    scope.vm.verify();
                } else {
                    MessageService.showErrorMessage('Login Failed', error.data.message);
                }
                return false;
            });
        },
        remind: function (email) {
            MessageService.showLoading('Sending...');
            return Restangular.all('reg/forget_password').post({username: email}).then(function (data) {
                MessageService.hideLoading();
                if (!data.data.message) {
                    MessageService.showErrorMessage('Forget Password', data.data.error);
                } else {
                    MessageService.showSuccessMessage('Reset Password', data.data.message);
                }
                //return data;
            }, function (error) {
                MessageService.hideLoading();
                MessageService.showErrorMessage('Login', error.data.message);
                //return error;
            });
        },
        location: function () {
            Restangular.all(apiVersion + 'automation/locations').getList(null, {
                //'ABODE-API-KEY': token
            }).then(function (data) {
                if (data) {
                    if (window.geofence && localStorageService.get('settings').location) {
                        window.geofence.removeAll().then(function () {
                            var tags = [];
                            angular.forEach(data, function (val) {
                                var id = (val.rule === 'in') ? 1 + '' + val.loc_id : 2 + '' + val.loc_id; //jshint ignore:line
                                tags.push({
                                    id: id,
                                    latitude: Math.round(val.lat * 1000000) / 1000000, //Geo latitude of geofence
                                    longitude: Math.round(val.lng * 1000000) / 1000000, //Geo longitude of geofence
                                    radius: parseInt(val.radius), //Radius of geofence in meters
                                    transitionType: (val.rule === 'in') ? 1 : 2, //Type of transition 1 - Enter, 2 - Exit, 3 - Both
                                    notification: {
                                        id: id,
                                        text: val.loc_id //jshint ignore:line
                                    }
                                });
                            });
                            console.log('GEOFENCE', data);
                            if (tags.length) {
                                window.geofence.addOrUpdate(tags).then(function () {
                                    console.log('Geofence successfully added');
                                }, function (reason) {
                                    console.log('Adding geofence failed', reason);
                                });
                            }
                        });
                    }
                }
            });
        },
        verify: function (code) {
            var form = {
                authcode: code
            };

            //jshint ignore:start
            if (window.cordova) {
                //device.platform
                if (cordova.platformId === 'android') {
                    form.gcm_token = localStorageService.get('pushToken');
                }
                else if (cordova.platformId === 'ios') {
                    form.apns_token = localStorageService.get('pushToken');
                }
                form.device_model = device.model;
                form.device_name = device.name || device.model;
            } else {
                form.apns_token = '78374359cd0dc313cc353108e0aeb404f132fe1701f108b6eda1058f047b78ad';
                form.device_model = 'Virtual';
                form.device_name = 'Virtual';
            }
            //jshint ignore:end

            console.log(form);

            MessageService.showLoading('Logging In…');
            return Restangular.all('reg/verify').post(form).then(function (data) {
                Sockets.connect();
                MessageService.hideLoading();
                $http.defaults.headers.common['ABODE-API-KEY'] = data.token;
                localStorageService.set('token', {
                    value: data.token,
                    expired_at: data.expired_at //jshint ignore:line
                });
                localStorageService.set('user', data.user);

                switch (data.user.step) {
                    case '1':
                        $state.go('setupProfile');
                        break;
                    case '2':
                        $state.go('setupChoose');
                        break;
                    default:
                        $state.go('tab.status.timeline');
                        break;
                }
            }, function (error) {
                MessageService.hideLoading();
                if (error.data.message === 'You need to verify your email address before you can continue.') {
                    $state.go('signin');
                } else {
                    $state.go('signin');
                    MessageService.showErrorMessage('Login Failed', error.data.message);
                }
                return false;
            });
        }
    };
}