'use strict';

angular.module('app.signin')
    .controller('SigninController', SigninController)
    .controller('VerifyController', VerifyController);

SigninController.$inject = ['MessageService', 'SigninService', 'SetupService', '$stateParams', '$scope', '$ionicPopup', 'RegisterService', '$rootScope'];
function SigninController(MessageService, SigninService, SetupService, $stateParams, $scope, $ionicPopup, RegisterService, $rootScope) {
    var vm = this;
    vm.user = {
        //email: 'sehord@pascalium.com',
        //email: 'fousey72@gmail.com',
        //password: 'Abodetest123'
        //password: '6N9jjcjg'
    };

    vm.signIn = signIn;
    vm.remindPassword = remindPassword;
    vm.verify = verify;
    vm.login = login;

    if ($stateParams.id) {
        MessageService.showErrorMessage('Verification Successful', 'Thanks for verifying you account.');
    }

    $rootScope.$on('$stateChangeSuccess', function (event, toState) { //jshint ignore:line
        vm.login();
    });

    function signIn(user) {
        if (!user.email || !user.password) {
            if (!user.email) {
                MessageService.showErrorMessage('Input Error', 'Please input email address.');
            }
            else if (!user.password) {
                MessageService.showErrorMessage('Input Error', 'Please input password.');
            }
        } else {
            SigninService.login(user, $scope).then(function () {
                SetupService.learnStopPUT();
            });
        }
    }

    function remindPassword(user) {
        if (!user.email) {
            MessageService.showErrorMessage('Input Error', 'Please input email address.');
        } else {
            SigninService.remind(user.email);
        }
    }

    function verify() {
        $scope.view = $ionicPopup.show({
            templateUrl: 'components/_global/templates/popups/popup-register.html',
            scope: $scope,
            buttons: [{
                text: 'Resend Link',
                type: 'abode-button register-popup',
                onTap: function (e) {
                    RegisterService.resendLink(vm.user.email);
                    $scope.view.close();
                    e.preventDefault();
                }
            }]
        });
    }

    function login() {
        if ($scope.view) {
            $scope.view.close();
        }
    }
}

VerifyController.$inject = ['SigninService', '$stateParams'];
function VerifyController(SigninService, $stateParams) {
    SigninService.verify($stateParams.code);
}