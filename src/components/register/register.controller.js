'use strict';

angular.module('app.register')
    .controller('RegisterController', RegisterController);

RegisterController.$inject = ['MessageService', 'RegisterService', '$ionicPopup', '$scope', '$state', '$rootScope'];
function RegisterController(MessageService, RegisterService, $ionicPopup, $scope, $state, $rootScope) {
    var vm = this;
    vm.user = {
        //email: 'sehord@pascalium.com',
        //password: 'Abodetest123',
        //password2: 'Abodetest123'
    };
    vm.register = register;
    vm.login = login;

    $rootScope.$on('$stateChangeSuccess', function (event, toState) { //jshint ignore:line
        if ($scope.view) {
            $scope.view.close();
        }
    });

    function register(user) {
        if (!user.email || !user.password || user.password !== user.password2) {
            if (!user.email) {
                MessageService.showErrorMessage('Input Error', 'Please input email address.');
            }
            else if (!user.password) {
                MessageService.showErrorMessage('Input Error', 'Please input password.');
            }
            else if (!user.password2) {
                MessageService.showErrorMessage('Input Error', 'Please input confirm password.');
            }
            else if (user.password !== user.password2) {
                MessageService.showErrorMessage('Input Error', 'Confirm Password is not matched for the password. Please check again.');
            }
        } else {
            RegisterService.register(user).then(function (data) {
                if (data) {
                    $scope.email = vm.user.email;
                    $scope.view = $ionicPopup.show({
                        templateUrl: 'components/_global/templates/popups/popup-register.html',
                        scope: $scope,
                        buttons: [{
                            text: 'Resend Link',
                            type: 'abode-button register-popup',
                            onTap: function (e) {
                                RegisterService.resendLink(vm.user.email);
                                $scope.view.close();
                                $state.go('signin');
                                e.preventDefault();
                            }
                        }]
                    });
                }
            });
        }
    }

    function login() {
        $scope.view.close();
        $state.go('signin');
    }
}