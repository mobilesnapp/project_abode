'use strict';

angular.module('app.register')
    .factory('RegisterService', RegisterService);

RegisterService.$inject = ['Restangular', 'MessageService'];
function RegisterService(Restangular, MessageService) {
    return {
        register: function (user) {
            MessageService.showLoading('Registering...');
            return Restangular.all('reg/new').post(user).then(function (data) {
                MessageService.hideLoading();
                return data;
            }, function (error) {
                MessageService.hideLoading();
                MessageService.showErrorMessage(error.data.message, error.data.errors[0].message);
                return false;
            });
        },
        resendLink: function (email) {
            MessageService.showLoading('Sending...');
            return Restangular.all('reg/resend').post({id: email}).then(function () {
                MessageService.hideLoading();
            }, function (error) {
                MessageService.hideLoading();
                MessageService.showErrorMessage(error.data.message, error.data.errors[0].message);
                return false;
            });
        }
    };
}