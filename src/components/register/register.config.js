'use strict';

angular.module('app.register', [])
    .config(registerConfig);

registerConfig.$inject = ['$stateProvider'];
function registerConfig($stateProvider) {
    $stateProvider
        .state('register', {
            url: '/register',
            templateUrl: 'components/register/register.template.html',
            controller: 'RegisterController as vm'
        });
}