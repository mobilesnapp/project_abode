'use strict';

angular.module('app.setup', [])
    .config(setupConfig);

setupConfig.$inject = ['$stateProvider'];
function setupConfig($stateProvider) {
    $stateProvider
        .state('setupProfile', {
            url: '/setup/profile',
            templateUrl: 'components/setup/setup-profile/setup-profile.template.html',
            controller: 'SetupProfileController as vm'
        })
        .state('setupChoose', {
            url: '/setup/choose',
            templateUrl: 'components/setup/setup-choose/setup-choose.template.html'
        })
        .state('setupGateway', {
            url: '/setup/gateway',
            templateUrl: 'components/setup/setup-gateway/setup-gateway.template.html',
            controller: 'SetupGatewayController as vm'
        })
        .state('setupGateway2', {
            url: '/setup/gateway2',
            templateUrl: 'components/setup/setup-gateway/setup-gateway2.template.html'
        })
        .state('setupGateway2-1', {
            url: '/setup/gateway2-1',
            templateUrl: 'components/setup/setup-gateway/setup-gateway2-1.template.html'
        })
        .state('setupGateway3', {
            url: '/setup/gateway3',
            templateUrl: 'components/setup/setup-gateway/setup-gateway3.template.html',
            controller: 'SetupGatewayController as vm'
        })
        .state('setupGateway4', {
            url: '/setup/gateway4',
            templateUrl: 'components/setup/setup-gateway/setup-gateway4.template.html',
            controller: 'SetupGatewayController as vm'
        })
        .state('setupGateway5', {
            url: '/setup/gateway5',
            templateUrl: 'components/setup/setup-gateway/setup-gateway5.template.html'
        })
        .state('setupDevices', {
            url: '/setup/devices',
            templateUrl: 'components/setup/setup-devices/setup-devices.template.html',
            controller: 'SetupDevicesController as vm'
        })
        .state('setupDeviceAddContact', {
            url: '/setup/device/device_type.door_contact',
            params: {
                new: true
            },
            templateUrl: 'components/setup/templates/devices-edit/contact.template.html',
            controller: 'SetupDevicesController as vm'
        })
        .state('setupDeviceEditContact', {
            url: '/setup/device/device_type.door_contact/:id',
            templateUrl: 'components/setup/templates/devices-edit/contact.template.html',
            controller: 'SetupDevicesController as vm'
        })
        .state('setupDeviceAddMotion', {
            url: '/setup/device/device_type.ir_camera',
            params: {
                new: true
            },
            templateUrl: 'components/setup/templates/devices-edit/motion.template.html',
            controller: 'SetupDevicesController as vm'
        })
        .state('setupDeviceEditMotion', {
            url: '/setup/device/device_type.ir_camera/:id',
            templateUrl: 'components/setup/templates/devices-edit/motion.template.html',
            controller: 'SetupDevicesController as vm'
        })
        .state('setupDeviceAddCamera', {
            url: '/setup/device/stream.camera',
            templateUrl: 'components/setup/templates/devices-edit/streamcamera.template.html',
            controller: 'SetupDevicesController as vm'
        })
        .state('setupDeviceEditCamera', {
            url: '/setup/device/stream.camera/:id',
            params: {
                currentStep: 5
            },
            templateUrl: 'components/setup/templates/devices-edit/streamcamera.template.html',
            controller: 'SetupDevicesController as vm'
        });
}