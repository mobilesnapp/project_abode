'use strict';

angular.module('app.setup')
    .controller('SetupProfileController', SetupProfileController);

SetupProfileController.$inject = ['SetupService', 'MessageService', '$state', 'localStorageService'];
function SetupProfileController(SetupService, MessageService, $state, localStorageService) {
    var vm = this;
    vm.user = {};
    vm.states = [];
    vm.editProfile = editProfile;

    SetupService.statesGET().then(function (data) {
        vm.states = data;
    });

    function editProfile(user) {
        if (!user.fullName || !user.address || !user.city || !user.state || !user.zip) {
            if (!user.fullName) {
                MessageService.showErrorMessage('Input Error', 'Please input your name.');
            }
            else if (!user.phone) {
                MessageService.showErrorMessage('Input Error', 'Please input your phone number.');
            }
            else if (!user.address) {
                MessageService.showErrorMessage('Input Error', 'Please input your address.');
            }
            else if (!user.city) {
                MessageService.showErrorMessage('Input Error', 'Please input your city.');
            }
            else if (!user.state) {
                MessageService.showErrorMessage('Input Error', 'Please input your state.');
            }
            else if (!user.zip) {
                MessageService.showErrorMessage('Input Error', 'Please input your zip.');
            }
        } else {
            SetupService.profilePOST(user).then(function (data) {
                if (data) {
                    var user = localStorageService.get('user');
                    user.first_name = data.first_name; //jshint ignore:line
                    user.last_name = data.last_name; //jshint ignore:line
                    user.address = data.address;
                    user.city = data.city;
                    user.state = data.state;
                    user.zip = data.zip;
                    user.phone = data.phone;
                    localStorageService.set('user', user);
                    $state.go('setupChoose');
                }
            });
        }
    }
}