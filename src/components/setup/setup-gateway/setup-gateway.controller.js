'use strict';

angular.module('app.setup')
    .controller('SetupGatewayController', SetupGatewayController);

SetupGatewayController.$inject = ['SetupService', '$timeout', '$state', '$rootScope', 'MessageService', 'localStorageService'];
function SetupGatewayController(SetupService, $timeout, $state, $rootScope, MessageService, localStorageService) {
    var vm = this;
    vm.user = {
        //gatewayCode: 'GMFY2C'
    };

    vm.enterCode = enterCode;
    vm.setupGateway = setupGateway;

    $rootScope.$on('$stateChangeSuccess', function (event, toState) {
        if (toState.name === 'setupGateway4') {
            SetupService.gatewayPUT().then(function (data) {
                if (data) {
                    $state.go('setupGateway5');
                }
            });
        }
    });

    function enterCode(user) {
        SetupService.profileGatewayPOST(user).then(function (data) {
            if (data && !data.errors) {
                vm.errType = 0;
                console.log(data);
                $timeout(function () {
                    vm.setupGateway();
                }, 1500);
            } else {
                console.log(data);
                vm.errType = 1;
                vm.errMessage = data.errors[0].message;
                //if (data.errors) {
                //    vm.errType = 2;
                //} else {
                //    vm.errType = 1;
                //}
            }
        });
    }

    function setupGateway() {
        SetupService.gatewaySetupZonePUT({
            setup_zone_1: 0, //jshint ignore:line
            setup_zone_2: 0, //jshint ignore:line
            setup_zone_3: 0, //jshint ignore:line,
            setup_gateway: 1 //jshint ignore:line
        }).then(function () {
            SetupService.gatewayGET().then(function (data) {
                localStorageService.set('panel', data);
                if (data) {
                    if (data.online === '1') {
                        if (data.initialized === '1') {
                            $state.go('setupGateway5');
                        } else {
                            $state.go('setupGateway4');
                        }
                    } else {
                        $state.go('setupGateway3');
                        MessageService.showErrorMessage('Error', 'Gateway is offline');
                    }
                }
            });
        });
    }
}