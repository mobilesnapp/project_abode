'use strict';

angular.module('app.setup')
    .factory('SetupService', SetupService);

SetupService.$inject = ['Restangular', 'apiVersion', 'MessageService'];
function SetupService(Restangular, apiVersion, MessageService) {
    return {
        statesGET: function () {
            return Restangular.all(apiVersion + 'states').getList().then(function (data) {
                return data;
            });
        },
        profilePOST: function (user) {
            MessageService.showLoading('Saving...');
            var form = {
                first_name: user.fullName.split(' ')[0], //jshint ignore:line
                last_name: user.fullName.split(' ')[1], //jshint ignore:line
                address: user.address,
                city: user.city,
                state: user.state.originalObject.code,
                zip: user.zip,
                phone: user.phone,
                step: '2'
            };
            return Restangular.all(apiVersion + 'user/edit').post(form).then(function (data) {
                MessageService.hideLoading();
                return data;
            }, function (error) {
                console.log(error);
                MessageService.hideLoading();
                return false;
            });
        },
        profileGatewayPOST: function (user) {
            MessageService.showLoading('Saving...');
            var form = {
                gateway_code: user.gatewayCode //jshint ignore:line
            };
            return Restangular.all(apiVersion + 'user/edit').post(form).then(function (data) {
                MessageService.hideLoading();
                return data;
            }, function (error) {
                MessageService.hideLoading();
                //MessageService.showErrorMessage('Error', error.data.errors[0].message);
                //if (error.data.errors[0].field === 'gateway_code') {
                //    return false;
                //} else {
                //    return {
                //        error: true
                //    };
                //}
                return error.data;
            });
        },
        gatewayGET: function () {
            return Restangular.one(apiVersion + 'panel').get().then(function (data) {
                return data;
            }, function (error) {
                console.log(error);
                return false;
            });
        },
        gatewaySetupZonePUT: function (options) {
            return Restangular.one(apiVersion + 'panel/setup_zone').customPUT(options).then(function (data) {
                return data;
            }, function (error) {
                console.log(error);
                return false;
            });
        },
        gatewayPUT: function () {
            return Restangular.one(apiVersion + 'panel/init').put().then(function (data) {
                return data;
            }, function (error) {
                console.log(error);
                return false;
            });
        },
        gatewayDevicesGET: function () {
            return Restangular.all(apiVersion + 'devices').getList().then(function (data) {
                return data;
            }, function (error) {
                console.log(error);
                return false;
            });
        },
        gatewayDevicesPUT: function (device, id) {
            MessageService.showLoading('Saving...');
            var form = {
                name: device.name,
                type: device.type
            };
            form.is_window = (device.type == 'Window') ? 1 : 0; //jshint ignore:line
            return Restangular.one(apiVersion + 'devices/' + id).customPUT(form).then(function (data) {
                MessageService.hideLoading();
                return data;
            }, function (error) {
                console.log(error);
                MessageService.hideLoading();
                return false;
            });
        },
        gatewayDevicesCameraGET: function (code) {
            MessageService.showLoading('Activating...');
            return Restangular.one(apiVersion + 'cams/activate?code=' + code).get().then(function (data) {
                MessageService.hideLoading();
                return data;
            }, function (error) {
                MessageService.hideLoading();
                MessageService.showErrorMessage('Error', error.data.message);
                return false;
            });
        },
        gatewayDevicesCameraDetailsGET: function (id) {
            return Restangular.one(apiVersion + 'cams/' + id).get().then(function (data) {
                return data;
            }, function (error) {
                MessageService.showErrorMessage('Error', error.data.message);
                return false;
            });
        },
        gatewayDevicesCameraWifiGET: function (mac) {
            return Restangular.one(apiVersion + 'cams/wifi?mac=' + mac).get().then(function (data) {
                return data;
            }, function (error) {
                if (error.data) {
                    MessageService.showErrorMessage('Error', error.data.message);
                }
                return false;
            });
        },
        gatewayDevicesCameraWifiPUT: function (data, mac) {
            MessageService.showLoading('Joining Network...');
            var form = {
                mac: mac,
                enable: 1,
                ssid: data.ssid,
                auth: data.auth,
                encrypt: data.encrypt,
                password: data.password
            };
            //console.log(form);
            //data.enable = 1;
            return Restangular.one(apiVersion + 'cams/wifi').customPUT(form).then(function (data) {
                MessageService.hideLoading();
                return data;
            }, function (error) {
                console.log(error);
                MessageService.hideLoading();
                if (error.data) {
                    MessageService.showErrorMessage('Error', error.data.message);
                } else {
                    MessageService.showErrorMessage('Gateway Timeout', 'Please try again');
                }
                return false;
            });

        },
        gatewayDevicesNewGET: function () {
            return Restangular.all(apiVersion + 'devices/new').getList().then(function (data) {
                return data;
            }, function (error) {
                MessageService.showErrorMessage('Error', error.data.message);
                return false;
            });
        },
        gatewayDevicesAddPOST: function (id) {
            return Restangular.one(apiVersion + 'devices/' + id).post().then(function (data) {
                return data;
            }, function (error) {
                MessageService.showErrorMessage('Error', error.data.message);
                return false;
            });
        },
        capturePhoto: function (id) {
            return Restangular.one(apiVersion + 'cams/' + id + '/capture').put().then(function (data) {
                return data;
            }, function (error) {
                console.log(error);
                MessageService.showErrorMessage('Error', error.data.message);
                return false;
            });
        },
        getPhotoFromTimeline: function (id) {
            //&from='+moment().format('YYYY-MM-DD')+'&last_seen=&to='+moment().format('YYYY-MM-DD')
            return Restangular.one(apiVersion + 'timeline?device_id=' + id + '&dir=next&event_label=Image+Capture').get().then(function (data) { //jshint ignore:line
                return data;
            }, function (error) {
                console.log(error);
                MessageService.showErrorMessage('Error', error.data.message);
                return false;
            });
        },
        learnStartPUT: function () {
            return Restangular.one(apiVersion + 'panel/learn/start').put().then(function (data) {
                return data;
            }, function (error) {
                MessageService.showErrorMessage('Error', error.data.message);
                return false;
            });
        },
        learnStopPUT: function () {
            return Restangular.one(apiVersion + 'panel/learn/stop').put().then(function (data) {
                return data;
            });
        }
    };
}