'use strict';

angular.module('app.setup')
    .controller('SetupDevicesController', SetupDevicesController);

SetupDevicesController.$inject = ['SetupService', '$rootScope', '$timeout', 'localStorageService', '$stateParams', 'MessageService', '$state', 'apiUrl', '$ionicModal', '$scope', '$ionicHistory'];
function SetupDevicesController(SetupService, $rootScope, $timeout, localStorageService, $stateParams, MessageService, $state, apiUrl, $ionicModal, $scope, $ionicHistory) {
    var vm = this;
    vm.steps = [];
    var searching = false, timeout = {}, captureTime, isSnapCancel;

    //vm.code = 'CMZH6C';

    vm.deviceId = ($stateParams.id) ? $stateParams.id : null;
    vm.devices = localStorageService.get('devices') || [];
    var panel = localStorageService.get('panel') || [];
    if (panel['setup_zone_4'] === '0') { //jshint ignore:line
        SetupService.gatewaySetupZonePUT({
            setup_zone_4: '1' //jshint ignore:line
        });
    }

    vm.edited = $rootScope.devicesEdited || {};
    vm.editedTotal = 0;
    checkedDevices(vm.devices);
    vm.skipVisible = false;

    if ($stateParams.new) {
        vm.newDevice = true;
        vm.deviceId = 'new';
        vm.edited[vm.deviceId] = {};
    }
    if (vm.deviceId) {
        vm.edited[vm.deviceId] = (vm.edited[vm.deviceId]) ? vm.edited[vm.deviceId] : {};
    }

    vm.steps.push(vm.currentStep = $stateParams.currentStep || 1);

    vm.goToEditUrl = goToEditUrl;
    vm.cameraSetupPopup = cameraSetupPopup;
    vm.openApp = openApp;
    vm.nextWindow = nextWindow;
    vm.previousWindow = previousWindow;
    vm.updateDeviceContact = updateDeviceContact;
    vm.setAsType = setAsType;
    vm.setAsConnection = setAsConnection;
    vm.snapPhoto = snapPhoto;
    vm.snapCancel = snapCancel;
    vm.cameraActivate = cameraActivate;
    vm.cameraChooseConnection = cameraChooseConnection;
    vm.cameraChooseNetwork = cameraChooseNetwork;
    vm.cameraChoosePassword = cameraChoosePassword;

    SetupService.gatewayDevicesGET().then(function (data) {
        var devices = [];
        vm.editedTotal = 0;
        angular.forEach(data, function (val) {
            if (val.type_tag !== 'device_type.remote_controller') { //jshint ignore:line
                devices.push(val);
            }
        });
        checkedDevices(data);
        vm.devices = angular.copy(devices);
        localStorageService.set('devices', devices);
    });

    $ionicModal.fromTemplateUrl('components/_global/templates/popups/popup-setup-camera.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });

    $scope.$watch('vm.currentStep', function () {
        if (searching) {
            searching = false;
            SetupService.learnStopPUT();
        }
        if (vm.currentStep === -1) {
            learningStartMotion();
        }
    });
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });
    $rootScope.$on('$stateChangeStart', function () {
        if (searching) {
            searching = false;
            SetupService.learnStopPUT();
        }
    });

    function checkedDevices(devices) {
        angular.forEach(devices, function (val) {
            if (val.type_tag !== 'device_type.remote_controller') { //jshint ignore:line
                vm.edited[val.id] = angular.copy(val);
                if (panel['setup_zone_' + val.zone] === '1' || val.zone > 3 || !panel['setup_zone_' + val.zone]) {
                    vm.editedTotal++;
                    vm.edited[val.id].done = true;
                }
            }
        });
    }

    function goToEditUrl(device) {
        switch (device.type_tag) { //jshint ignore:line
            case 'device_type.door_contact':
                return 'setupDeviceEditContact({"id": "' + device.id + '"})';
            case 'device_type.ir_camera':
                return 'setupDeviceEditMotion({"id": "' + device.id + '"})';
            case 'device_type.ipcam':
                return 'setupDeviceEditCamera({"id": "' + device.id + '"})';
            default:
                return 'login';
        }
    }

    function cameraSetupPopup() {
        $scope.modal.show();
    }

    function openApp() {
        if (vm.editedTotal >= vm.devices.length) {
            //$state.go('tab.status.timeline');
            $state.go('settingsSubscription');
        }
    }

    function nextWindow(step) {
        vm.steps.push(step);
        vm.currentStep = step;
    }

    function previousWindow() {
        vm.steps.pop(-1);
        if (!!vm.steps.length) {
            vm.currentStep = vm.steps[vm.steps.length - 1];
        } else {
            $ionicHistory.goBack();
        }
    }

    function updateDeviceContact(device) {
        if (!device.name) {
            MessageService.showErrorMessage('Add Door/Window Sensor', 'Please input sensor name.');
        } else {
            SetupService.gatewayDevicesPUT(device, vm.deviceId).then(function (data) {
                if (data) {
                    //$state.go('setupDevices');
                    if (vm.edited[vm.deviceId].zone < 4) {
                        vm.edited[vm.deviceId].done = true;
                        var options = {};
                        options['setup_zone_' + vm.edited[vm.deviceId].zone] = 1;

                        SetupService.gatewaySetupZonePUT(options).then(function () {
                            var p = localStorageService.get('panel');
                            p['setup_zone_' + vm.edited[vm.deviceId].zone] = '1';
                            localStorageService.set('panel', p);
                            $ionicHistory.goBack();
                            vm.steps = [];
                        });
                    } else {
                        $ionicHistory.goBack();
                        vm.steps = [];
                    }
                }
            });
        }
    }

    // door/window contact settings
    function setAsType(type) {
        vm.edited[vm.deviceId].type = type;
        $rootScope.devicesEdited = vm.edited;
        vm.nextWindow(2);
    }

    function setAsConnection(type) {
        vm.edited[vm.deviceId].type = type;
        $rootScope.devicesEdited = vm.edited;
        vm.cameraChooseConnection(vm.edited[vm.deviceId].type);
    }

    // motion camera settings
    function snapPhoto() {
        imageid = null;
        isSnapCancel = false;
        vm.steps.push(vm.currentStep = 3);
        $timeout(function () {
            vm.skipVisible = true;
        }, 10000);
        SetupService.capturePhoto(vm.deviceId).then(function (data) {
            if (data) {
                captureTime = moment().format('x'); //jshint ignore:line
                checkPhoto();
            } else {
                vm.skipVisible = false;
                vm.steps.push(vm.currentStep = 2);
            }
        });
    }

    function snapCancel() {
        isSnapCancel = true;
        nextWindow(2);
    }

    function cameraActivate(code) {
        SetupService.gatewayDevicesCameraGET(code).then(function (device) {
            if (device && !device.code) {
                if (device.online === '1') {
                    vm.steps.push(vm.currentStep = 4);
                    learningStart();
                } else {
                    vm.steps.push(vm.currentStep = 3);
                }
            } else {
                vm.code = '';
            }
        });
    }

    function cameraChooseConnection(type) {
        if (type) {
            if (type === 'Ethernet') {
                vm.steps.push(vm.currentStep = 10);
            } else {
                var token = vm.deviceId.split(':')[1].split(''),
                    mac = '';
                angular.forEach(token, function (val, i) {
                    if (i === 2 || i === 4 || i === 6 || i === 8 || i === 10) {
                        mac += ':';
                    }
                    mac += val;
                });
                SetupService.gatewayDevicesCameraWifiGET(mac.toUpperCase()).then(function (wifi) {
                    console.log(wifi);
                    if (wifi) {
                        vm.wifis = wifi;
                    } else {
                        vm.steps[vm.steps.length - 1] = vm.currentStep = 5;
                    }
                });
                vm.steps.push(vm.currentStep = 6);
            }
        } else {
            MessageService.showErrorMessage('Warning', 'Please choose one of connection types');
        }
    }

    function cameraChooseNetwork(network) {
        vm.wifi = network;
        vm.steps.push(vm.currentStep = 7);
    }

    function cameraChoosePassword() {
        var token = vm.deviceId.split(':')[1].split(''),
            mac = '';
        angular.forEach(token, function (val, i) {
            if (i === 2 || i === 4 || i === 6 || i === 8 || i === 10) {
                mac += ':';
            }
            mac += val;
        });
        SetupService.gatewayDevicesCameraWifiPUT(vm.wifi, mac.toUpperCase()).then(function (data) {
            if (data) {
                vm.steps.push(vm.currentStep = 8);
            } else {
                //MessageService.showErrorMessage('Warning', 'Please check your WIFI or choose another connection types');
                vm.steps.push(vm.currentStep = 5);
            }
        });
    }

    function learningStart() {
        SetupService.learnStartPUT().then(function (data) {
            if (data) {
                timeout = $timeout(function () {
                    learningStart();
                }, 175000);
                if (!searching) {
                    searching = true;
                    checkNewDevices();
                }
            } else {
                vm.steps[vm.steps.length - 1] = vm.currentStep = 2;
                SetupService.learnStopPUT();
            }
        });
    }

    function learningStartMotion() {
        SetupService.learnStartPUT().then(function (data) {
            if (data) {
                timeout = $timeout(function () {
                    learningStartMotion();
                }, 175000);
                if (!searching) {
                    searching = true;
                    checkNewMotionDevices();
                }
            } else {
                SetupService.learnStopPUT();
                $ionicHistory.goBack();
            }
        });
    }

    function checkNewDevices() {
        if (searching) {
            SetupService.gatewayDevicesNewGET().then(function (newDevices) {
                if (newDevices.length) {
                    vm.deviceId = newDevices[0].id;
                    vm.edited[vm.deviceId] = {};
                    SetupService.gatewayDevicesAddPOST(newDevices[0].id).then(function () {
                        SetupService.learnStopPUT();
                        $timeout.cancel(timeout);
                        vm.steps[vm.steps.length - 1] = vm.currentStep = 5;
                    });
                } else {
                    checkNewDevices();
                }
            });
        }
    }

    function checkNewMotionDevices() {
        if (searching) {
            SetupService.gatewayDevicesNewGET().then(function (newDevices) {
                if (newDevices.length) {
                    var data = vm.edited[vm.deviceId];
                    vm.deviceId = newDevices[0].id;
                    vm.edited[vm.deviceId] = data;
                    vm.edited[vm.deviceId].type_id = newDevices[0].type_id; //jshint ignore:line
                    if (($state.current.name === 'setupDeviceAddContact' && newDevices[0].type_id === '4') || ($state.current.name === 'setupDeviceAddMotion' && newDevices[0].type_id === '27')) { //jshint ignore:line
                        SetupService.gatewayDevicesAddPOST(newDevices[0].id).then(function () {
                            if (($state.current.name === 'setupDeviceAddContact' && newDevices[0].type_id === '4')) { //jshint ignore:line
                                vm.steps[vm.steps.length - 1] = vm.currentStep = 3;
                            } else {
                                vm.steps[vm.steps.length - 1] = vm.currentStep = 2;
                            }
                            SetupService.learnStopPUT();
                            $timeout.cancel(timeout);
                        });

                    } else {
                        $timeout(function () {
                            checkNewMotionDevices();
                        }, 3000);
                    }
                } else {
                    $timeout(function () {
                        checkNewMotionDevices();
                    }, 3000);
                }
            });
        }
    }

    var imageid;

    function checkPhoto() {
        SetupService.getPhotoFromTimeline(vm.deviceId).then(function (data) {
            if (data.length) {
                if (data[0].file_path && !imageid) { //jshint ignore:line
                    imageid = data[0].file_path; //jshint ignore:line
                } else if (!imageid) {
                    imageid = true;
                }
                if (data[0].file_path && imageid != data[0].file_path) { //jshint ignore:line
                    vm.capturedPicture = apiUrl + '/' + data[0].file_path; //jshint ignore:line
                    $timeout(function () {
                        vm.steps[vm.steps.length - 1] = vm.currentStep = 4;
                        vm.skipVisible = false;
                    }, 2000);
                } else {
                    $timeout(function () {
                        if (!isSnapCancel) {
                            checkPhoto();
                        }
                    }, 2000);
                }
            } else {
                imageid = true;
                $timeout(function () {
                    if (!isSnapCancel) {
                        checkPhoto();
                    }
                }, 2000);
            }
        });
    }
}