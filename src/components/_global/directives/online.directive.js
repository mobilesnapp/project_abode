'use strict';

angular.module('app.global')
    .directive('onlineStatus', onlineStatus);

onlineStatus.$inject = ['Restangular', 'apiVersion', '$state', '$rootScope', 'AlertService'];
function onlineStatus(Restangular, apiVersion, $state, $rootScope, AlertService) {
    return {
        template: '<div class="online-status" ng-class="{\'active\': active}" ng-click="goToDetails()">Touch to return to alert</div>',
        controller: function ($scope) {
            Restangular.all(apiVersion + 'timeline/group_alarm?page=0').getList().then(function (data) {
                if (data) {
                    angular.forEach(data, function (val) {
                        if (val.verified_by_tid === '0') { //jshint ignore:line
                            $scope.active = true;
                            $scope.id = val.id;
                        }
                    });
                }
            });
            $scope.goToDetails = function () {
                $state.go('alertDetails', {id: $scope.id, by: 0});
            };
            $rootScope.$on('updateAlert', function () {
                console.log('updateAlert');
                initAlert();
            });
            function initAlert() {
                AlertService.alarmGroupListGET(0).then(function (data) {
                    if (data) {
                        var active = false,
                            counter = 0;
                        angular.forEach(data, function (val) {
                            if (val.verified_by_tid === '0') { //jshint ignore:line
                                active = true;
                                $scope.id = val.id;
                                counter++;
                            }
                        });
                        $rootScope.counter = counter;
                        $scope.active = active;
                    }
                });
            }
        }
    };
}