'use strict';

angular.module('app.global')
    .factory('StreamService', StreamService);

StreamService.$inject = ['Restangular', 'apiVersion'];
function StreamService(Restangular, apiVersion) {
    return {
        streamSessionPOST: function (mac) {
            return Restangular.one(apiVersion + 'stream/session').customPOST({
                mac: mac
            }).then(function (data) {
                return data;
            }, function (error) {
                console.error(error);
                return false;
            });
        },
        startStreamPOST: function (mac) {
            return Restangular.one(apiVersion + 'stream/start').customPOST({
                mac: mac
            }).then(function (data) {
                return data;
            }, function (error) {
                console.error(error);
                return false;
            });
        }
    };
}