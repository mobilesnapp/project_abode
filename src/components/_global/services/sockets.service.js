'use strict';

angular.module('app.global')
    .factory('Sockets', SocketsService);

SocketsService.$inject = ['$rootScope', '$timeout', '$state', 'socketUrl', 'localStorageService', 'DevicesService', 'GatewayService'];
function SocketsService($rootScope, $timeout, $state, socketUrl, localStorageService, DevicesService, GatewayService) {
    var socket = window.io(socketUrl, {
        transports: ['websocket'],
        autoConnect: false,
        forceNew: true,
        reconnection: true,
        reconnectionAttempts: Infinity
    });
    socket.on('connect', function () {
        console.log('connect');
        socket.on('com.goabode.gateway.mode', function (newMode) {
            console.log('change mode');
            $timeout(function () {
                localStorageService.set('gateway', $rootScope.gateway);
                $rootScope.gateway.mode.area_1 = newMode; //jshint ignore:line
            });
            if ($state.current.name.indexOf('alertDetails') >= 0) {
                $state.go('alert', {state: 1});
            }
        });

        socket.on('com.goabode.gateway.online', function (currentStatus) {
            if (currentStatus === '0') {
                GatewayService.online = $rootScope.gateway = '0';
            } else {
                GatewayService.online = $rootScope.gateway = '1';
            }
        });

        socket.on('com.goabode.device.update', function (deviceID) {
            DevicesService.rtDeviceUpdate(deviceID);
            console.log('Device Update::  ' + deviceID);
        });

        socket.on('com.goabode.gateway.getrecord', function (obj) {
            console.log('com.goabode.gateway.getrecord', obj);
        });

        socket.on('com.goabode.gateway.timeline', function (timelineObject) {
            if ($state.current.name === 'tab.status.devices') {
                $rootScope.status_tab = 'timeline'; //jshint ignore:line
                $state.go('tab.status.timeline');
            }
            $rootScope.$broadcast('updateTimeLine', timelineObject);
            $rootScope.$broadcast('updateAlert', timelineObject);
        });
    });

    //socket.on('event', function (data) {
    //    console.log(data);
    //});

    socket.on('disconnect', function () {
        console.log('disconnect');
        //socket.disconnect();
    });

    return {
        disconnect: function () {
            socket.close();
            socket.connect();
        },
        connect: function () {
            socket.connect();
        }
    };
}