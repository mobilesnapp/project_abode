'use strict';

angular.module('app.global.translation', [])
    .constant('language', (window.navigator.userLanguage || window.navigator.language || 'en').split('-')[0])//jshint ignore:line
    .config(configTranslationService);

configTranslationService.$inject = ['$translateProvider', 'language'];
function configTranslationService($translateProvider) {
    var language = window.navigator.userLanguage || window.navigator.language; //jshint ignore:line

    $translateProvider.translations('en', {});
    $translateProvider.translations('ru', {});

    $translateProvider.useSanitizeValueStrategy('sanitize');
    $translateProvider.preferredLanguage(language);
}