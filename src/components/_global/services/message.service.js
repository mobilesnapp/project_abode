'use strict';

angular.module('app.global')
    .factory('MessageService', MessageService);

MessageService.$inject = ['$rootScope', '$ionicPopup', '$ionicLoading'];
function MessageService($rootScope, $ionicPopup, $ionicLoading) {
    return {
        showSuccessMessage: function (title, message) {
            $rootScope.popup = {
                title: title,
                message: message
            };
            $ionicPopup.show({
                templateUrl: 'components/_global/templates/popups/popup-alert.html',
                // title: 'Select',
                scope: $rootScope,
                buttons: [{
                    text: 'OK',
                    type: 'abode-button register-popup',
                    onTap: function () {
                        return true;
                    }
                }]
            });
        },
        showErrorMessage: function (title, message) {
            $rootScope.popup = {
                title: title,
                message: message
            };
            $ionicPopup.show({
                templateUrl: 'components/_global/templates/popups/popup-alert.html',
                // title: 'Select',
                scope: $rootScope,
                buttons: [{
                    text: 'OK',
                    type: 'abode-button register-popup',
                    onTap: function () {
                        return true;
                    }
                }]
            });
        },
        showRegisteredErrorMessage: function (title, message) {
            $rootScope.popup = {
                title: title,
                message: message
            };
            $ionicPopup.show({
                templateUrl: 'components/_global/templates/popups/popup-alert.html',
                // title: 'Select',
                scope: $rootScope,
                buttons: [{
                    text: 'OK',
                    type: 'abode-button register-popup',
                    onTap: function () {
                        return true;
                    }
                }]
            });
        },
        showLoading: function (text) {
            $ionicLoading.show({
                template: text
            });
        },
        hideLoading: function () {
            $ionicLoading.hide();
        }
    };
}