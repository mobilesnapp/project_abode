'use strict';

angular.module('app.global.time', [])
    .constant('moment', moment)//jshint ignore:line
    .config(configTimeService);

configTimeService.$inject = ['moment', 'language'];
function configTimeService(moment, language) {
    moment.locale(language);
}