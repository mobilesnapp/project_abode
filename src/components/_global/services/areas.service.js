'use strict';
//jshint ignore:start
angular.module('app.global')
    .factory('AreasService', AreasService);

AreasService.$inject = ['Restangular', 'apiVersion'];
function AreasService(Restangular, apiVersion) {
    return {
        AreasPUT: function (req) {
            return Restangular.one(apiVersion + 'areas').customPUT({
                area: 1,
                away_entry_beep: req,
                away_exit_beep: req,
                home_entry_beep: req,
                home_exit_beep: req
            }).then(function (data) {
            }, function (error) {
                console.log(error);
            });
        },
        AreasGET: function () {
            return Restangular.one(apiVersion + 'areas').customGET().then(function (data) {
                return data.area_1;
            }, function (error) {
                console.log(error);
            });
        }
    };
}