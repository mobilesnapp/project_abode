'use strict';

angular.module('app.global')
    .factory('PanelService', PanelService);

PanelService.$inject = ['Restangular', 'apiVersion'];
function PanelService(Restangular, apiVersion) {
    return {
        panelGET: function () {
            return Restangular.one(apiVersion + 'panel').get().then(function (data) {
                return data;
            }, function (error) {
                console.error(error);
                return false;
            });
        },
        panelModePUT: function (status) {
            return Restangular.one(apiVersion + 'panel/mode/1/' + status).put().then(function (data) {
                return data;
            }, function (error) {
                console.error(error);
                return error.data;
            });
        },
        panelAreasGET: function () {
            return Restangular.one(apiVersion + 'areas').get().then(function (data) {
                return data;
            }, function (error) {
                console.log(error);
                return false;
            });
        }
    };
}