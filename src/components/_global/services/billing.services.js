'use strict';

angular.module('app.global')
    .factory('BillingService', BillingService);

BillingService.$inject = ['Restangular', 'apiVersion', 'MessageService', 'localStorageService'];
function BillingService(Restangular, apiVersion, MessageService, localStorageService) {
    return {
        billingPlansGET: function () {
            return Restangular.one(apiVersion + 'plans').get().then(function (data) {
                return data;
            }, function (error) {
                console.log(error);
                return false;
            });
        },
        billingPlansPUT: function (req) {
            MessageService.showLoading('Processing Payment...');
            return Restangular.one(apiVersion + 'billing').customPUT(req).then(function (data) {
                console.log('billing', data);
                var user = localStorageService.get('user');
                user.plan = data.plan;
                user.plan_id = data.id; //jshint ignore:line
                localStorageService.set('user', user);
                MessageService.hideLoading();
                return data;
            }, function (error) {
                console.log(error);
                MessageService.hideLoading();
                if (error.data.message === 'You have already registered to this plan.') {
                    return error;
                } else {
                    MessageService.showErrorMessage('Failed', error.data.message);
                    return false;
                }
            });
        },
        billingPlansCardPUT: function (req) {
            return Restangular.one(apiVersion + 'billing/card').customPUT(req).then(function (data) {
                return data;
            }, function (error) {
                console.log(error);
                MessageService.showErrorMessage('Failed', error.data.message);
                return false;
            });
        }
    };
}