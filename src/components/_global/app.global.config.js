'use strict';

angular.module('app.global', [
    'app.global.translation',
    'app.global.time'
])
    .constant('apiUrl', 'https://my.goabode.com')
    .constant('socketUrl', 'https://io.goabode.com')
    .constant('pushWooshKey', '69DE1-30E71')
    .constant('branchKey', 'key_live_hlpOv2orbZwY5ZD2VMt80nmmByp3ne9L')
    .constant('androidKey', '337736643747')
    .constant('apiVersion', 'v1/')
    .run([
        function () {

        }
    ]);
