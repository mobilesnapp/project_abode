'use strict';

angular.module('app.welcome')
    .controller('WelcomeController', welcomeController);

welcomeController.$inject = [];
function welcomeController() {
    var vm = this;
    vm.slides = [
        {
            title: 'Welcome to abode <br> The Future of Home <br>Security',
            imageSrc: 'assets/img/login/abode-Logo.png'
        },
        {
            title: 'Verify all events to prevent false alarms.',
            imageSrc: 'assets/img/onboarding/verify-events.png'
        },
        {
            title: 'Get Instant Notifications',
            imageSrc: 'assets/img/onboarding/Instant-Notifications.png'
        }
    ];
}