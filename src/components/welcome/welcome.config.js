'use strict';

angular.module('app.welcome', [])
    .config(welcomeConfig);

welcomeConfig.$inject = ['$stateProvider'];
function welcomeConfig($stateProvider) {
    $stateProvider
        .state('welcome', {
            url: '/welcome',
            templateUrl: 'components/welcome/welcome.template.html',
            controller: 'WelcomeController as vm'
        });
}