'use strict';

angular.module('app.alert')
    .config(alertDetailsConfig);

alertDetailsConfig.$inject = ['$stateProvider'];
function alertDetailsConfig($stateProvider) {
    $stateProvider
        .state('alertDetails', {
            url: '/adetails/:id/:by',
            templateUrl: 'components/alert/details/details.template.html',
            controller: 'AlertDetailsController as vm'
        });
}