'use strict';

angular.module('app.alert')
    .controller('AlertDetailsController', AlertDetailsController);

AlertDetailsController.$inject = [
    '$sce', '$state', '$ionicPopup', '$interval', '$timeout', '$stateParams', '$ionicHistory', 'AssociatesService', 'AlertDetailsService', 'localStorageService', 'CameraService', 'apiUrl',
    'StreamService', 'DevicesService', '$rootScope'
];
function AlertDetailsController($sce, $state, $ionicPopup, $interval, $timeout, $stateParams, $ionicHistory, AssociatesService, AlertDetailsService, localStorageService, CameraService, apiUrl, StreamService, DevicesService, $rootScope) {
    var vm = this;

    var timeoutStop;

    vm.cameraCounter = 0;
    vm.userDetails = localStorageService.get('user');
    vm.alertResult = $stateParams.by;
    vm.alertsList = localStorageService.get('alerts')[$stateParams.id] || [];

    vm.videoCams = {};
    vm.counter = 0;
    vm.streamUrl = '';
    vm.refresh = true;

    vm.url = url;
    vm.imgUrl = imgUrl;
    vm.callPolice = callPolice;
    vm.verifyAlarm = verifyAlarm;
    vm.disableAlarm = disableAlarm;
    vm.startStream = startStream;
    vm.getImageUrl = getImageUrl;
    //vm.getUserInfo = getUserInfo;

    DevicesService.load().then(function () {
        console.log('devices load');
        CameraService.load().then(function () { //jshint ignore:line
            console.log('camera list', CameraService.list);
            angular.forEach(CameraService.list, function (val) {
                if (val.stream === '1') { //jshint ignore:line
                    vm.streamPreparing = true;
                    vm.cameraCounter++;
                }
            });
            loadList();
        }, function () {
            angular.forEach(CameraService.list, function (val) {
                if (val.stream === '1') { //jshint ignore:line
                    vm.streamPreparing = true;
                    vm.cameraCounter++;
                }
            });
            loadList();
        });
    });

    vm.user = {};
    AssociatesService.associatesGET().then(function (data) {
        angular.forEach(data, function (val) {
            vm.user[val.id] = angular.copy(val);
        });
    });

    function loadList() {
        AlertDetailsService.alarmGroupGET($stateParams.id, $stateParams.by).then(function (data) {
            //vm.refresh = false;
            vm.alertsList = data;
            var storage = localStorageService.get('alerts');
            storage[$stateParams.id] = data;
            localStorageService.set('alerts', storage);

            angular.forEach(data, function (val) {
                if (val.device_type_id === '69') { //jshint ignore:line
                    vm.streamPreparing = false;
                    vm.cameraCounter++;
                    vm.device = val;
                    if (!vm.videoCams[vm.device.device_id]) { //jshint ignore:line
                        vm.videoCams[vm.device.device_id] = angular.copy(vm.device); //jshint ignore:line
                        vm.startStream(vm.device.device_id.split(':')[1]); //jshint ignore:line
                    }
                }
                if (val.file_count > 1) { //jshint ignore:line
                    var ext = val.file_path.substring(val.file_path.length - 4); //jshint ignore:line
                    var file = val.file_path.substring(0, val.file_path.length - 5); //jshint ignore:line
                    val.files = [];
                    for (var i = 0; i < val.file_count; i++) { //jshint ignore:line
                        val.files.push({
                            file_path: apiUrl + '/' + file + (i + 1) + ext  //jshint ignore:line
                        });
                    }
                }
            });

            if (vm.alertResult === '0') {
                vm.alertsList = vm.alertsList.reverse();
                timeoutStop = $timeout(function () {
                    loadList();
                }, 10000);
            }
        });
    }

    $rootScope.$on('$stateChangeStart', function () {
        $timeout.cancel(timeoutStop);
    });

    function url(path) {
        return $sce.trustAsResourceUrl(apiUrl + '/' + path);
    }

    function imgUrl(path) {
        var res = path.replace('mp4', 'jpg');
        return $sce.trustAsResourceUrl(apiUrl + '/' + res);
    }

    function startStream(id) {
        var streamId = id;
        StreamService.streamSessionPOST(streamId)
            .then(function (data) {
                if (data) {
                    vm.videoCams[vm.device.device_id].session = data.session; //jshint ignore:line
                    StreamService.startStreamPOST(streamId).then(function () {
                        vm.videoCams[vm.device.device_id].url = apiUrl + '/get_image?mac=' + streamId + "&session=" + vm.videoCams[vm.device.device_id].session; //jshint ignore:line
                        vm.videoCams[vm.device.device_id].state = 'STREAMING'; //jshint ignore:line
                        vm.videoCams[vm.device.device_id].timer = $interval(keepAlive, 45000); //jshint ignore:line
                        vm.getImageUrl();
                    }); //jshint ignore:line
                }
            });

        function keepAlive() {
            //StreamService.startStreamPOST(streamId)
            //    .then(function (response) {
            //        console.log(response.data);
            //        if (response.data['server'] != '') {
            //            //console.log('Streaming Keep Alive timer response received.');
            //        }
            //        else {
            //            throw 'Invalid response received for keep alive timer.'
            //        }
            //    });
            //    }, function (error) {
            //
            //    })
            //    ['catch'](function (error) {
            //    console.log('Error in streaming keep alive, stoping stream.', error);
            //    self.stopStream();
            //});
        }
    }

    function getImageUrl() {
        $interval(function () {
            vm.counter++;
            vm.streamUrl = vm.videoCams[vm.device.device_id].url + '&id=' + vm.counter; //jshint ignore:line
        }, 1000);
    }

    function callPolice() {
        $ionicPopup.show({
            templateUrl: 'components/_global/templates/popups/popup_police_call.html',
            buttons: [
                {
                    text: 'Go Back',
                    type: 'abode-button notif-popup alarm_gray_button'
                },
                {
                    text: 'Call 911',
                    type: 'abode-button notif-popup alarm_red_button',
                    onTap: function () {
                        window.location.href = 'tel://911';
                    }
                }
            ]
        });
    }

    function verifyAlarm() {
        $ionicPopup.show({
            templateUrl: 'components/_global/templates/popups/popup_disable_alarm.html',
            title: 'Are you sure you want to Verify this Alarm?',
            subTitle: [
                {text: 'Veryfing will:'},
                {text: 'Notify the alarm center and other users'},
                {text: 'Keep the siren active'}
            ],
            buttons: [
                {
                    text: 'Go Back',
                    type: 'abode-button notif-popup alarm_gray_button'
                }, {
                    text: 'Verify Alarm',
                    type: 'abode-button notif-popup alarm_red_button',
                    onTap: function () {
                        AlertDetailsService.alarmVerifyPOST($stateParams.id).then(function (data) {
                            if (data) {
                                $ionicPopup.show({
                                    templateUrl: 'components/_global/templates/popups/popup_disable_alarm.html',
                                    title: 'Alarm verified',
                                    subTitle: [
                                        {text: 'The alarm center was notified and the siren is still active. To turn off siren change your system to Standby when all clear'}
                                    ],
                                    buttons: [
                                        {
                                            text: 'Go Back',
                                            type: 'abode-button notif-popup alarm_gray_button'
                                        }
                                    ]
                                });
                            }
                        });
                    }
                }
            ]
        });
    }

    function disableAlarm() {
        var subtitle = (vm.userDetails.plan === 'free') ? [
            {text: 'Disabling will:'},
            {text: 'Silence the siren'},
            {text: 'System will enter standby mode'},
            {text: 'Other users will be notified you silenced the alarm'}
        ] : [
            {text: 'Disabling will:'},
            {text: 'Silence the alarm'},
            {text: 'System will enter standby mode'},
            {text: 'Other users will be notified you silenced the alarm'},
            {text: 'Notify of monitoring center of false alarm status (if they have not already contacted authorities)'}
        ];
        $ionicPopup.show({
            templateUrl: 'components/_global/templates/popups/popup_disable_alarm.html',
            title: 'Are you sure you want to disable your alarm?',
            subTitle: subtitle,
            buttons: [
                {
                    text: 'Go Back',
                    type: 'abode-button notif-popup alarm_gray_button'
                }, {
                    text: 'Disable Alarm',
                    type: 'abode-button notif-popup alarm_green_button',
                    onTap: function () {
                        AlertDetailsService.alarmDisablePOST($stateParams.id).then(function () {
                            $state.go('tab.status.timeline');
                        });
                    }
                }
            ]
        });
    }

    //function getUserInfo(id) {
    //    angular.forEach(vm.users, function (val) {
    //        if (id === val.id) {
    //            this.userName = ' by ' + val.name;
    //            this.profilePic = val.profile_pic; //jshint ignore:line
    //        }
    //    });
    //}
}