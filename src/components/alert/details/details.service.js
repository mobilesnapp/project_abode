'use strict';

angular.module('app.alert')
    .factory('AlertDetailsService', AlertDetailsService);

AlertDetailsService.$inject = ['Restangular', 'apiVersion', 'MessageService'];
function AlertDetailsService(Restangular, apiVersion, MessageService) {
    return {
        alarmGroupListGET: function () {
            return Restangular.all(apiVersion + 'timeline/group_alarm?page=0').getList().then(function (data) {
                return data;
            }, function (error) {
                MessageService.hideLoading();
                MessageService.showErrorMessage('Error', error.data.message);
            });
        },
        alarmGroupGET: function (id, tid) {
            return Restangular.all(apiVersion + 'timeline/group_timeline?id=' + id + '&verified_tid=' + tid || 0).getList().then(function (data) {
                return data;
            }, function (error) {
                MessageService.hideLoading();
                MessageService.showErrorMessage('Login', error.data.message);
            });
        },
        alarmVerifyPOST: function (id) {
            return Restangular.one(apiVersion + 'timeline/' + id + '/verify_alarm').post().then(function (data) {
                return data;
            }, function (error) {
                MessageService.hideLoading();
                MessageService.showErrorMessage('Login', error.data.message);
            });
        },
        alarmDisablePOST: function (id) {
            var me = this;
            return Restangular.one(apiVersion + 'timeline/' + id + '/ignore_alarm').post().then(function (data) {
                me.panelStandByPUT();
                return data;
            }, function (error) {
                MessageService.hideLoading();
                MessageService.showErrorMessage('Login', error.data.message);
            });
        },
        panelStandByPUT: function () {
            return Restangular.one(apiVersion + 'panel/mode/1/standby').put();
        }
    };
}