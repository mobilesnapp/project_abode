'use strict';

//jshint ignore:start
angular.module('app.alert')
    .filter('abode_date_format', function () {
        return function (content) {
            // date and time revision
            var r_string_date;
            var l_date, l_date_now;
            var time = (content.verified_time) ? content.verified_time : content.time;
            l_date = new Date(Date.parse(content.date));
            l_date_now = new Date();
            if (l_date.getYear() == l_date_now.getYear() &&
                l_date.getMonth() == l_date_now.getMonth()) {
                if (l_date.getDate() == l_date_now.getDate()) {
                    r_string_date = "Today @ " + time;
                } else if (l_date.getDate() == l_date_now.getDate() - 1) {
                    r_string_date = "Yesterday @ " + time;
                } else {
                    r_string_date = content.date + " @ " + time;
                }
            } else {
                r_string_date = content.date + " @ " + time;
            }
            return r_string_date;
        }
    })
    .controller('AlertController', AlertController);

AlertController.$inject = ['AlertService', '$scope', '$sce', 'apiUrl', 'CameraService', '$stateParams'];
function AlertController(AlertService, $scope, $sce, apiUrl, CameraService, $stateParams) {
    var vm = this;
    var page = 0;

    vm.tab = $stateParams.state || 0;
    vm.alarmGroupList = [];
    vm.newAlerts = 0;
    vm.loadStop = false;

    vm.init = init;
    vm.selectTab = selectTab;
    vm.url = url;

    CameraService.load();

    function init() {
        AlertService.alarmGroupListGET(page).then(function (data) {
            if (!data.length) {
                vm.loadStop = true;
            } else {
                vm.alarmGroupList = vm.alarmGroupList.concat(data);
                vm.newAlerts = 0;
                angular.forEach(vm.alarmGroupList, function (val) {
                    if (val.verified_by_tid === '0') {
                        vm.newAlerts++;
                    }
                });
                page++;
            }
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    }

    function selectTab(index) {
        vm.tab = index;
    }

    function url(path) {
        return $sce.trustAsResourceUrl(apiUrl + '/' + path);
    }
}