'use strict';

angular.module('app.application')
    .service('AlertService', AlertService);

AlertService.inject = ['Restangular', 'apiVersion', 'MessageService'];
function AlertService(Restangular, apiVersion, MessageService) {
    return {
        alarmGroupListGET: function (page) {
            return Restangular.all(apiVersion + 'timeline/group_alarm?page=' + page).getList().then(function (data) {
                return data;
            }, function (error) {
                MessageService.hideLoading();
                MessageService.showErrorMessage('Error', error.data.message);
            });
        }
    };
}