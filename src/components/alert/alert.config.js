'use strict';

angular.module('app.alert', ['app.application'])
    .config(alertConfig);

alertConfig.$inject = ['$stateProvider'];
function alertConfig($stateProvider) {
    $stateProvider
        .state('alert', {
            url: '/alert',
            params: {
                state: null
            },
            templateUrl: 'components/alert/alert.template.html',
            controller: 'AlertController as vm'
        });
}