'use strict';

//jshint ignore:start
angular.module('app.alert')
    .controller('AlarmCtrl', AlarmCtrl);

AlarmCtrl.inject = ['$scope', '$ionicPopup', 'AlarmService'];
function AlarmCtrl($scope, $ionicPopup, AlarmService) {
    $scope.click_index = 0;
    $scope.alarms = AlarmService.getAlarms();

    $scope.onCallPoliceClick = onCallPoliceClick;
    $scope.onDisableClick = onDisableClick;

    function call_police() {
    }

    function disable_alarm() {
    }

    function onCallPoliceClick() {
        // An alert dialog
        $ionicPopup.show({
            templateUrl: 'templates/popup/popup_police_call.html',
            scope: $scope,
            buttons: [
                {
                    text: 'Go Back',
                    type: 'abode-button notif-popup alarm_gray_button',
                    onTap: function () {

                        return true;
                    }
                }, {
                    text: 'Call 911',
                    type: 'abode-button notif-popup alarm_red_button',
                    onTap: function () {
                        call_police();
                        return true;
                    }
                }]
        });
    }

    function onDisableClick() {
        // An alert dialog
        $ionicPopup.show({
            templateUrl: 'templates/popup/popup_disable_alarm.html',
            // title: 'Select',
            scope: $scope,
            buttons: [
                {
                    text: 'Go Back',
                    type: 'abode-button notif-popup alarm_gray_button',
                    onTap: function () {
                        return true;
                    }
                },
                {
                    text: 'Disable Alarm',
                    type: 'abode-button notif-popup alarm_green_button',
                    onTap: function () {
                        disable_alarm();
                        return true;
                    }
                }]
        });
    }
}