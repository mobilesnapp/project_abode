'use strict';

//jshint ignore:start
angular.module('app.application')
    .service('AlarmService', AlarmService);

AlarmService.inject = [];
function AlarmService() {
    var alarmFactory = {};
    var alarms = [
        {
            title: 'Alarm Activated',
            date: '11:10AM - 1 minute ago',
            icon: './img/alert/alarm_activated.png',
            image_path: './img/camera_images/screen-capture1.png',
            imgs: -1,
            type: '1'
        },
        {
            title: 'Photo Captured',
            date: '11:10AM  -  1 minute ago',
            icon: './img/alert/photo_captured.png',
            imgs: [
                {
                    url: './img/alert/captured1.png'
                },
                {
                    url: './img/alert/captured1.png'
                },
                {
                    url: './img/alert/captured1.png'
                }
            ],
            type: '2'
        },
        {
            title: 'Motion Detected - Hallway',
            date: '11:10AM  - 1 minute ago',
            icon: './img/alert/motion_detected.png',
            imgs: -1,
            type: '1'
        },
        {
            title: 'Front Door Opened',
            date: '11:10AM  - 1 minute ago',
            icon: './img/alert/door_opened.png',
            imgs: -1,
            type: '1'
        }
    ];

    alarmFactory.getAlarms = function () {
        return alarms;
    };

    return alarmFactory;
}