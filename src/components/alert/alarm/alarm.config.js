'use strict';

angular.module('app.alert')
    .config(alarmConfig);

alarmConfig.$inject = ['$stateProvider'];
function alarmConfig($stateProvider) {
    $stateProvider
        .state('alarmoff', {
            url: '/alarmoff',
            templateUrl: 'components/alert/alarm/alarm.template.html',
            controller: 'AlarmCtrl'
        });
}