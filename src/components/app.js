'use strict';

angular.module('app', [
    'ionic',
    'ui.router',
    'restangular',
    'angular-md5',
    'LocalStorageModule',
    'angucomplete',
    'pascalprecht.translate',
    'angularMoment',
    'ngCordova',
    'ngSanitize',

    'app.global',
    'app.welcome',
    'app.signin',
    'app.register',
    'app.setup',
    'app.application',
    'app.alert'
])
    .config([
        '$ionicConfigProvider',
        'RestangularProvider',
        'apiUrl',
        'localStorageServiceProvider',
        '$httpProvider',
        function ($ionicConfigProvider, RestangularProvider, apiUrl, localStorageServiceProvider, $httpProvider) {
            $ionicConfigProvider.views.maxCache(0);
            $ionicConfigProvider.backButton.text('').icon('ion-ios-arrow-back');
            $ionicConfigProvider.views.transition('platform');
            $ionicConfigProvider.navBar.alignTitle('center');
            $ionicConfigProvider.tabs.position('bottom');

            RestangularProvider.setBaseUrl(apiUrl + '/api/');

            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
            $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
            $httpProvider.defaults.transformRequest.unshift(function (data) {
                if (data) {
                    var key, result = [];
                    for (key in data) {
                        if (data.hasOwnProperty(key)) {
                            result.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
                        }
                    }
                    return result.join('&');
                }
            });
            $httpProvider.interceptors.push(function ($q, $rootScope) {
                return {
                    'responseError': function (rejection) {
                        if (rejection.status === 403) {
                            $rootScope.$emit('logout');
                        }
                        return $q.reject(rejection);
                    }
                };
            });

            localStorageServiceProvider.setPrefix(apiUrl);
        }
    ])
    .run([
        'Restangular',
        'localStorageService',
        'apiVersion',
        'SigninService',
        'SetupService',
        'PanelService',
        '$state',
        '$rootScope',
        'apiUrl',
        'pushWooshKey',
        'branchKey',
        'androidKey',
        '$http',
        'Sockets',
        '$ionicPlatform',
        function (Restangular, localStorageService, apiVersion, SigninService, SetupService, PanelService, $state, $rootScope, apiUrl, pushWooshKey, branchKey, androidKey, $http, Sockets, $ionicPlatform) {

            Restangular.setErrorInterceptor(function (response) {
                if (response.status === 403) {
                    $rootScope.$emit('logout');
                    return false; // error handled
                }
                return true; // error not handled
            });

            // TODO: REMOVE THIS
            $rootScope.urlBase = apiUrl;
            $rootScope.urlBackend = apiUrl + '/api/v1';
            $rootScope.urlBackendReg = apiUrl + '/api';
            // ENDTODO: REMOVE THIS

            if (!localStorageService.get('settings')) {
                localStorageService.set('settings', {
                    location: true
                });
            }
            if (!localStorageService.get('alerts')) {
                localStorageService.set('alerts', {});
            }
            if (localStorageService.get('token')) {
                $http.defaults.headers.common['ABODE-API-KEY'] = localStorageService.get('token').value;
                SigninService.location();
                Restangular.one(apiVersion + 'user').get().then(function (data) {
                    if (window.cordova) {
                        if (cordova.platformId === 'ios' && localStorageService.get('settings').touchId) {
                            window.touchid.authenticate(function () {
                                switchPoint();
                            }, null, null);
                        } else {
                            switchPoint();
                        }
                    } else {
                        switchPoint();
                    }
                    function switchPoint() {
                        localStorageService.set('user', data);
                        Sockets.connect();
                        switch (data.step) {
                            case '1':
                                $state.go('setupProfile');
                                break;
                            case '2':
                                $state.go('setupChoose');
                                break;
                            default:
                                SetupService.learnStopPUT();
                                PanelService.panelGET().then(function (data) {
                                    localStorageService.set('panel', data);
                                    if (data.setup_zone_1 === '1' && data.setup_zone_2 === '1' && data.setup_zone_3 === '1') { //jshint ignore:line
                                        Restangular.all(apiVersion + 'timeline/group_alarm?page=0').getList().then(function (data) {
                                            if (data.length) {
                                                if (data[0].verified_by_tid === '0') { //jshint ignore:line
                                                    $state.go('alertDetails', {id: data[0].id, by: 0});
                                                } else {
                                                    $state.go('tab.status.timeline');
                                                }
                                            } else {
                                                $state.go('tab.status.timeline');
                                            }
                                        });
                                    } else {
                                        $state.go('setupDevices');
                                    }
                                });
                                break;
                        }
                    }
                }, function () {
                    $rootScope.$emit('logout');
                });
            } else {
                if (localStorageService.get('welcome')) {
                    $state.go('signin');
                } else {
                    $state.go('welcome');
                    localStorageService.set('welcome', true);
                }
            }

            $ionicPlatform.ready(function () {
                if (window.cordova) {
                    window.open = cordova.InAppBrowser.open;
                    var push = window.PushNotification.init({
                        android: {
                            senderID: androidKey
                        },
                        ios: {
                            alert: true,
                            badge: true,
                            sound: true
                        }
                    });
                    push.on('registration', function (data) {
                        console.log(data);
                        localStorageService.set('pushToken', data.registrationId);
                        push.setApplicationIconBadgeNumber(null, null, 0);
                    });
                    push.on('notification', function (data) {
                        console.log('PUSH', data);
                        Restangular.all(apiVersion + 'timeline/group_alarm?page=0').getList().then(function (data) {
                            if (data.length) {
                                if (data[0].verified_by_tid === '0') { //jshint ignore:line
                                    $state.go('alertDetails', {id: data[0].id, by: 0});
                                }
                            }
                        });
                    });
                    push.on('error', function (e) {
                        console.log(e.message);
                    });
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
                    cordova.plugins.Keyboard.disableScroll(true);
                }
                if (window.geofence && localStorageService.get('settings').location) {
                    window.geofence.initialize();
                    window.geofence.getWatched().then(function (geofencesJson) {
                        var geofences = JSON.parse(geofencesJson);
                        console.log(geofences);
                    });
                    window.geofence.onTransitionReceived = function (geofences) {
                        geofences.forEach(function (geo) {
                            console.log('Geofence transition detected', geo);
                            Restangular.one(apiVersion + 'automation/apply_location_rules').customPUT({
                                id: geo.id.substring(1, 10),
                                rule: (geo.transitionType === 1) ? 'in' : 'out'
                            });
                        });
                    };
                }
                if (window.branch) {
                    branch.init(branchKey, function (err, data) { //jshint ignore:line
                        if (!err && data.data_parsed['+clicked_branch_link']) { //jshint ignore:line
                            branchCallback(data.data_parsed); //jshint ignore:line
                        }
                    });
                    $ionicPlatform.on('resume', function () {
                        branch.init(branchKey, function (err, data) { //jshint ignore:line
                            if (!err && data.data_parsed['+clicked_branch_link']) { //jshint ignore:line
                                branchCallback(data.data_parsed); //jshint ignore:line
                            }
                        });
                    });
                }
                function branchCallback(data) {
                    console.log('branch', data);
                    switch (data.url) {
                        case 'register':
                            $state.go('signinVerify', {id: 1});
                            break;
                        case 'timeline':
                            $state.go('alertDetails', {id: data.value, by: 0});
                            break;
                        case 'verify':
                            $state.go('verify', {code: data.value});
                    }
                }
            });

            $rootScope.$on('logout', function () {
                localStorageService.clearAll();
                localStorageService.set('welcome', true);
                localStorageService.set('tutorial_show', '1');
                localStorageService.get('alerts', {});
                localStorageService.set('settings', {
                    location: true
                });
                $state.go('signin');
            });
        }]);
