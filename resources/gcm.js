var gcm = require('node-gcm');

var message = new gcm.Message({
    priority: 'high',
    timeToLive: 3,
    data: {
        key1: 'message1',
        key2: 'message2'
    },
    notification: {
        title: "Abode",
        body: "This is a notification that will be displayed ASAP."
    }
});

var regIds = [
    "APA91bGbdOmFgwcT2-qA14lHlfYv7-zfuqpY3ybxx8FDPY1OSfjTVEQFYiCXHp7vhS1aOqeucE_jzQGr7hJ_GBNy0U5wzvn_GwLPBPU8VRpOKmaOcO2uZeH8il802ohwBhzOp2WIByzC",
    "APA91bE_WnneV9eTV8HLiWj8fKF2sjOdirQOfqm_tcbQFDEQaiKSMXTEzUWIatzGPG_0PRPQGWJTtS2qKsXc48P7vmoeW520h-AwDGsLE-pJuprw9_hDFcFEkLfV4fbDLMtHBx-KI29E"
];

// Set up the sender with you API key
var sender = new gcm.Sender('AIzaSyCjT5SvvHWkgwlMthR_64AlVTlStgJCw1c');

// Now the sender can be used to send messages
sender.send(message, {registrationIds: regIds}, function (err, result) {
    if (err) console.error(err);
    else    console.log(result);
});

// Send to a topic, with no retry this time
//sender.sendNoRetry(message, {topic: '/topics/global'}, function (err, result) {
//    if (err) console.error(err);
//    else    console.log(result);
//});